# 폰기프트 API

- 해당 API들은 IP등록이 필요함. (사용 전 API를 호출할 서버IP 전달 받아야함)
- API 호출 실패/에러 시 에러코드 참고 [에러코드](/markdown/errorCode.md)


## 1. 토큰 발급

### POST /apis/pongift/token

#### - 요청 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :---------- | 
| accessKey | String | true | accessKey | 
| secretKey | String | true | secretKey | 
| userKey | String | true | 웰컴 유저 키 (unique) | 

#### - 응답
- status 201
  ```javascript
  {
      "token": "gml0tzhtDya6aNdtVoLj+A=="
  }
  ```



## 2. 토큰 확인

### POST /apis/pongift/token-check

#### - 요청 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :---------- | 
| userKey | String | true | accessKey | 
| token | String | true | token | 
| accessKey | String | false | secretKey | 
| serviceId | Int | false | 서비스id | 

#### - 응답
- status 200



## 3. 토큰 삭제

### POST /apis/pongift/token-delete

#### - 요청 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :---------- | 
| accessKey | String | true | accessKey | 
| token | String | true | 토큰 | 
| userKey | String | true | 웰컴 유저 키 (unique) | 

#### - 응답
- status 204



## 4. 결제성공

### POST /apis/pongift/purchase-success

#### - 요청 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :---------- | 
| purchaseCode | String | true | 폰기프트 주문번호 | 
| tno | String | true | 웰컴 주문번호 (unique) | 
| price | Number | false | 결제 금액 (폰기프트 금액 검증용) |

#### - 응답
- status 200




## 5. 상품(문자) 발송 요청

### POST /apis/pongift/send-product

#### - 요청 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :---------- | 
| purchaseCode | String | true | 폰기프트 주문번호 | 

#### - 응답
- status 200
