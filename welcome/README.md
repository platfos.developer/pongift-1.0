### [폰기프트 API 상세](/markdown/welcome/pongift.md)

## 프로세스 흐름

### 토큰 인증 절차
![tokenValidationProcess](/markdown/welcome/token.jpg)

### 구매절차
![purchaseProcess](/markdown/welcome/purchase.jpg)

### 취소절차
![refundProcess](/markdown/welcome/refund.jpg)

<br><br>

## 폰기프트 SDK 연동 시 필요 작업

### 구매
1. SDK 구매버튼 클릭
1. 웰컴 결제 페이지 이동 (제공할 데이터는 아래 표 참고)
1. 웰컴에서는 전달받은 userKey와 token으로 유효성 검사 필요
1. 웰컴 결제 성공 시 SDK successUrl로 리다이렉트 / 실패 시 failUrl로 리다이렉트
1. 폰기프트에 결제 성공 알림 (POST /apis/pongift/purchase-success)
1. 폰기프트에 상품 발송 요청 (POST /apis/pongift/send-product)

#### 결제 시 제공할 데이터

| name | description | 
| ---------- | :---------- | 
| purchaseCode | 폰기프트 주문번호 |
| price | 결제금액 |
| successUrl | 결제성공 시 리다이렉트 url |
| failUrl | 결제실패 시 리다이렉트 url |
| userKey | 웰컴 유저키 |
| token | 토큰 |


### 환불
1. 폰기프트 -> 웰컴 환불API 요청 (요청 파라미터는 아래 표 참고)
1. 환불 성공 시 status 200과 성공 정보(json) 리턴
1. 환불 실패 시 status xxx와 실패 정보(json) 리턴

#### 환불API 요청 파라미터
| name | description | 
| ---------- | :---------- | 
| purchaseCode | 폰기프트 주문번호 |
| tno | 웰컴 주문번호 |
| price | 환불금액 |
| userKey | 유저키 |
| token | 토큰 |
