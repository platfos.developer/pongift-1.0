# dates

## /api/dashboard/dates

### 1. GET
#### admin 대시보드 화면의 "매출 현황 / 일일 매출 현황" API

> * 권한 (roleAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getDateDashboard() - 날짜별 주문 통계 쿼리  
> * roleUltraAdmin 최고관리자일 경우 모든 법인에 대한 수치를 보여주며 법인관리자일 경우 해당 법인에 속한 상품들의 통계만 보여준다.
> * 요청시 salesType을 date로 하면 해당 날짜의 데이터를 리턴하며 / week로 할시 일주일 통계 데이터를 리턴한다. 

<br/><br/>
#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| salesType | enum |  false | 검색타입(date / week) | 
| purchaseServiceId | int |  false | 서비스ID | 
| productServiceId | int |  false | 상품서비스id | 
| corporationId | int |  false | 법인id | 
| startAt | bigint |  false | 시작일 (timestamp) | 
| size | int |  false | 리턴갯수 | 

<br/><br/>
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
| parameter | desc |
| --------- | :---- |
| createdAt | 날짜 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

<br/><br/>
#### 응답 예제 (JSON) 

```
{
	"list": [
		{
			"createdAt": 17487,
			"totalMoney": 35000,
			"totalCount": 2
		},
		{
			"createdAt": 17484,
			"totalMoney": 1000,
			"totalCount": 1
		}
	]
}
```


