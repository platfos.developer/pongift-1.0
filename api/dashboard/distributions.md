# distributions

## /api/dashboard/distributions

### 1. GET
#### admin 대시보드 화면의 "BEST 유통대행 5" API

> * 권한 (roleUltraAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getDistributionDashboard() - 유통대행사별 통계  

#### 요청 파라미터 
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
| parameter | desc |
| --------- | :---- |
| serviceId | 서비스 id |
| name | 서비스명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

#### 응답 예제 (JSON) 

```
{
	"list": [
		{
			"serviceId": 52,
			"name": "스토어팜",
			"totalMoney": 6617500,
			"totalCount": 695
		},
		{
			"serviceId": 36,
			"name": "스마트dmb",
			"totalMoney": 20800,
			"totalCount": 6
		}
	]
}
```
