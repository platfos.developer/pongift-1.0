# products

## /api/dashboard/products

### 1. GET
#### admin 대시보드 화면의 "BEST 상품 10" API 

> * 권한 (roleAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getProductDashboard() - 상품별 주문 통계 쿼리  
> * roleUltraAdmin 최고관리자일 경우 모든 법인에 대한 수치를 보여주며 법인관리자일 경우 해당 법인에 속한 상품들의 통계만 보여준다.

<br/><br/>
#### 요청 파라미터 

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int |  false | 법인Id | 
| purchaseServiceId | int |  false | 서비스 ID | 
| productServiceId | int |  false | 상품 서비스 ID | 

 <br/><br/>
 
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
| parameter | desc |
| --------- | :---- |
| productId | 상품 id |
| name | 상품명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

<br/><br/>
 #### 응답 예제 (JSON) 

```
{
	"list": [
		{
			"productId": 1211,
			"name": "포레스트에코랜드1(교환처정보)",
			"totalMoney": 1600000,
			"totalCount": 18
		},
		{
			"productId": 1251,
			"name": "하트를듬뿍담은 쇼콜라스퀘어(뚜레쥬르)",
			"totalMoney": 1386000,
			"totalCount": 66
		},
		{
			"productId": 42,
			"name": "불고기와퍼스토어팜(전국 버거킹 매장에서 사용 가능합니다 \n※사용불가 매장\n- 오션월드점, 대명비발디점, 잠실야구장점, 문학야구장점, 여주휴게소점, 지산리조트점 , 경남대점, 삼성라이온즈파크점)",
			"totalMoney": 966600,
			"totalCount": 179
		},
		{
			"productId": 1160,
			"name": "벌크 상품 테스트(정보없음)",
			"totalMoney": 959000,
			"totalCount": 137
		},
		{
			"productId": 1225,
			"name": "카트라이더(교환처)",
			"totalMoney": 840000,
			"totalCount": 33
		},
		{
			"productId": 1236,
			"name": "네이버테디베어(교환처)",
			"totalMoney": 800000,
			"totalCount": 43
		},
		{
			"productId": 1195,
			"name": "스마트인피니상품2(교환처)",
			"totalMoney": 648000,
			"totalCount": 71
		},
		{
			"productId": 4,
			"name": "메리초코(정보없음)",
			"totalMoney": 629800,
			"totalCount": 94
		},
		{
			"productId": 38,
			"name": "X-TRA크런치치킨(전국 버거킹 매장에서 사용 가능합니다 \n※사용불가 매장\n- 오션월드점, 대명비발디점, 잠실야구장점, 문학야구장점, 여주휴게소점, 지산리조트점 , 경남대점, 삼성라이온즈파크점)",
			"totalMoney": 573400,
			"totalCount": 122
		},
		{
			"productId": 1250,
			"name": "후라이드반+양념반+콜라1.25L(또래오래)",
			"totalMoney": 542500,
			"totalCount": 31
		}
	]
}
```

