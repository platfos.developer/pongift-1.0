# users

## /api/dashboard/users

### 1. GET
#### admin 대시보드 화면의 "Best 우수고객 10" API

> * 권한 (roleUltraAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getUserDashboard() - 유저별 주문 통계 쿼리  

#### 요청 파라미터 
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
| parameter | desc |
| --------- | :---- |
| userId | 유저 id |
| phoneNum | 핸드폰번호 |
| nick | 유저명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

#### 응답 예제 (JSON) 

```
{
    "list": [
        {
            "nick": "1231231",
            "phoneNum": "+821029590224",
            "totalCount": 423,
            "totalMoney": 3425100,
            "userId": 10
        },
        {
            "nick": null,
            "phoneNum": null,
            "totalCount": 91,
            "totalMoney": 2398000,
            "userId": 26
        },
        {
            "nick": "현승재",
            "phoneNum": "+821029590224",
            "totalCount": 118,
            "totalMoney": 2115100,
            "userId": 1
        },
        {
            "nick": null,
            "phoneNum": null,
            "totalCount": 68,
            "totalMoney": 1469200,
            "userId": 35
        },
        {
            "nick": null,
            "phoneNum": null,
            "totalCount": 37,
            "totalMoney": 397400,
            "userId": 36
        },
        {
            "nick": "sngjoong",
            "phoneNum": null,
            "totalCount": 14,
            "totalMoney": 350000,
            "userId": 3
        },
        {
            "nick": "pongift_admin",
            "phoneNum": null,
            "totalCount": 8,
            "totalMoney": 329000,
            "userId": 20
        },
        {
            "nick": "1231232",
            "phoneNum": "+821029590224",
            "totalCount": 41,
            "totalMoney": 287000,
            "userId": 11
        },
        {
            "nick": "123123",
            "phoneNum": "+821029590224",
            "totalCount": 37,
            "totalMoney": 259000,
            "userId": 5
        },
        {
            "nick": "sngjoong",
            "phoneNum": null,
            "totalCount": 26,
            "totalMoney": 126000,
            "userId": 2
        }
    ]
}
```
