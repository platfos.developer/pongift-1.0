# categories

## /api/dashboard/categories

### 1. GET
#### admin 대시보드 화면의 "BEST 카테고리 5" API 

> * 권한 (roleAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getProductDashboard() - 상품별 주문 통계 쿼리  
> * roleUltraAdmin 최고관리자일 경우 모든 법인에 대한 수치를 보여주며 법인관리자일 경우 해당 법인에 속한 상품들의 통계만 보여준다.

<br/><br/>
#### 요청 파라미터 

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int |  false | 법인Id | 
| purchaseServiceId | int |  false | 서비스 ID | 
| productServiceId | int |  false | 상품 서비스 ID | 

 <br/><br/>
 
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
| parameter | desc |
| --------- | :---- |
| categoryId | 카테고리 id |
| name | 카테고리명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

<br/><br/>
 #### 응답 예제 (JSON) 

```
{
	"list": [
		{
			"categoryId": 46,
			"name": "과자",
			"totalMoney": 4286000,
			"totalCount": 148
		},
		{
			"categoryId": 38,
			"name": "버거/피자/치킨",
			"totalMoney": 2408200,
			"totalCount": 273
		},
		{
			"categoryId": 24,
			"name": "베이커리/떡",
			"totalMoney": 1456000,
			"totalCount": 63
		},
		{
			"categoryId": 23,
			"name": "커피/음료",
			"totalMoney": 1286000,
			"totalCount": 181
		},
		{
			"categoryId": 39,
			"name": "화장품",
			"totalMoney": 150000,
			"totalCount": 6
		}
	]
}
```
