
# dashboard

## 1. /api/dashboard/categories
### 1.1 GET
#### admin 대시보드 화면의 "BEST 카테고리 5" API

> * 권한 (roleAdmin)
> * 로그인 유저의 권한에 따라 데이터 수치 변경됨
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getCategoryDashboard() 사용 

#### 파라미터  

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId    | Int       | false       | 선택 법인 id |
| purchaseServiceId    | Int       | false       | 선택  서비스 id |
| productServiceId    | Int       | false       |  선택 상품의 서비스 id |

#### Return

| parameter | desc |
| --------- | :---- |
| categoryId | 카테고리 아이디 |
| name | 카테고리명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

<br />
<br />

## 2. /api/dashboard/dates
### 2.1 GET
#### admin 대시보드 화면의 "전일 대비 금일", "전주 대비 금주", "일일 매출 현황" API

> * 권한 (roleAdmin)
> * 로그인 유저의 권한에 따라 데이터 수치 변경됨
> * salesType에 따라 일별/주별 데이터 리턴
> * 리턴 데이터 7개 중 당일과 전일 데이터는 "전일 대비 금일"에 사용
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getDateDashboard() 사용 

#### 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| salesType | ENUM | true | "date", "week" |
| purchaseServiceId | Int | false | 선택 서비스 id |
| productServiceId | Int | false | 선택 상품의 서비스 id |
| corporationId | Int | false | 선택 법인 id |
| startAt | timestamp | true | 시작일 |
| size | Int | false | |

#### Return

| parameter | desc |
| --------- | :---- |
| createdAt | 생성일 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

<br />
<br />
    
## 3. /api/dashboard/distributions
### 3.1 GET
#### admin 대시보드 화면의 "BEST 유통대행 5" API

> * 권한 (roleUltraAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getDistributionDashboard() 사용 

#### 파라미터
없음

#### Return
| parameter | desc |
| --------- | :---- |
| serviceId | 서비스 id |
| name | 서비스 명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

<br />
<br />

## 4. /api/dashboard/products
### 4.1 GET
#### admin 대시보드 화면의 "BEST 상품 10" API

> * 권한 (roleAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getProductDashboard() 사용

#### 파라미터  

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId    | Int       | false       | 선택 법인 id |
| purchaseServiceId    | Int       | false       | 선택  서비스 id |
| productServiceId    | Int       | false       |  선택 상품의 서비스 id |

#### Return
| parameter | desc |
| --------- | :---- |
| productId | 상품 id |
| name | 상품명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

<br />
<br />

## 5. /api/dashboard/users
### 5.1 GET
#### admin 대시보드 화면의 "Best 우수고객 10" API

> * 권한 (roleUltraAdmin)
> * [AppProductTransfers](/markdown/api/DB/AppProductTransfers.md)의 getUserDashboard() 사용

#### 요청 파라미터 
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
| parameter | desc |
| --------- | :---- |
| userId | 유저 id |
| phoneNum | 핸드폰번호 |
| nick | 유저명 |
| totalMoney | 총 금액 |
| totalCount | 총 합 |

#### 응답 예제 (JSON) 

```
{
    "list": [
        {
            "nick": "1231231",
            "phoneNum": "+821029590224",
            "totalCount": 423,
            "totalMoney": 3425100,
            "userId": 10
        },
        {
            "nick": null,
            "phoneNum": null,
            "totalCount": 91,
            "totalMoney": 2398000,
            "userId": 26
        },
        {
            "nick": "현승재",
            "phoneNum": "+821029590224",
            "totalCount": 118,
            "totalMoney": 2115100,
            "userId": 1
        },
        {
            "nick": null,
            "phoneNum": null,
            "totalCount": 68,
            "totalMoney": 1469200,
            "userId": 35
        },
        {
            "nick": null,
            "phoneNum": null,
            "totalCount": 37,
            "totalMoney": 397400,
            "userId": 36
        },
        {
            "nick": "sngjoong",
            "phoneNum": null,
            "totalCount": 14,
            "totalMoney": 350000,
            "userId": 3
        },
        {
            "nick": "pongift_admin",
            "phoneNum": null,
            "totalCount": 8,
            "totalMoney": 329000,
            "userId": 20
        },
        {
            "nick": "1231232",
            "phoneNum": "+821029590224",
            "totalCount": 41,
            "totalMoney": 287000,
            "userId": 11
        },
        {
            "nick": "123123",
            "phoneNum": "+821029590224",
            "totalCount": 37,
            "totalMoney": 259000,
            "userId": 5
        },
        {
            "nick": "sngjoong",
            "phoneNum": null,
            "totalCount": 26,
            "totalMoney": 126000,
            "userId": 2
        }
    ]
}
```
