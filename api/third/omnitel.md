# omnitel

## /api/third/omnitel

### 1. get
#### 옴니텔 쿠폰 강제 사용 완료 API

> *  관리자가 해당 쿠폰 사용 완료 처리시 사용 

> *  사용일 기준 정산 상품일 경우 완료처리와 함께 정산처리 진행

> *  BRANCH_NAME은 필히 null값으로 해야 한다.

> *  findProductTransfer(): barcode 값이 같은 주문건 검색

> *  initSupplyAdjustment(): 상품공급사 정산 객체 생성

> *  initDistributionAdjustment(): 유통대행사 정산 객체 생성

> *  updateProductTransfer(): 정산 진행

> *  checkStoreFarmPurchase(): 스토어팜 주문 변경 (사용으로 변경)

> *  setPurchaseStatus(): 스토어팜에서 취소 요청인 상황에서 쿠폰을 사용했다면 취소 거절 전송

> *  fileWrite(): 스토어팜 return xml 작성 및 전송 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| BARCODE_NUM | String |  false | 쿠폰번호 |
| EXCHANGE_NUM | String |  false | 사용, 사용취소에 대한 인증 번호 |
| STATUS | Enum |  false | 쿠폰의 상태 001,002,010 |
| EXCHANGE_DATE | String |  false | 사용, 사용취소에 대한 시간 (YYYYMMDDHH24MISS) |
| BRANCH_NAME | String |  false | 사용, 사용취소 된 지점명 |
| BRANCH_CODE | String |  false | 사용, 사용취소 된 지점코드 |
| BARCODE_TYPE | String |  false | 쿠폰의 종류 BARCODE,AMOUNT |
| PRICE | Int |  false | 쿠폰의 금액, BARCODE 인 경우 공급가 전달, AMOUNT 인 경우 사용된 금액 전달 |
 

<br/><br/>
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

<br/><br/>
#### 응답 예제 (XML) 
```
<?xml version="1.0" encoding="EUC-KR"?>
<COUPONEXCHANGE>
	<RESULTCODE>00</RESULTCODE>
	<RESULTMSG>Success</RESULTMSG>
	<EXCHANGE_ID>1511751400419053</EXCHANGE_ID>
</COUPONEXCHANGE>
```

### 2. post
#### 옴니텔 쿠폰 완료 API

> *  사용자가 쿠폰을 사용했을 경우 옴니텔에서 사용 완료 호출 

> *  사용일 기준 정산 상품일 경우 완료처리와 함께 정산처리 진행

> *  rawBodyParsing() : 데이터가 body가 아닌 rawBody로 담겨오는것을 body로 치환

> *  findProductTransfer(): barcode 값이 같은 주문건 검색

> *  initSupplyAdjustment(): 상품공급사 정산 객체 생성

> *  initDistributionAdjustment(): 유통대행사 정산 객체 생성

> *  updateProductTransfer(): 정산 진행

> *  checkStoreFarmPurchase(): 스토어팜 주문 변경 (사용으로 변경)

> *  setPurchaseStatus(): 스토어팜에서 취소 요청인 상황에서 쿠폰을 사용했다면 취소 거절 전송

> *  fileWrite(): 스토어팜 return xml 작성 및 전송 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| BARCODE_NUM | String |  false | 쿠폰번호 |
| EXCHANGE_NUM | String |  false | 사용, 사용취소에 대한 인증 번호 |
| STATUS | Enum |  false | 쿠폰의 상태 001,002,010 |
| EXCHANGE_DATE | String |  false | 사용, 사용취소에 대한 시간 (YYYYMMDDHH24MISS) |
| BRANCH_NAME | String |  false | 사용, 사용취소 된 지점명 |
| BRANCH_CODE | String |  false | 사용, 사용취소 된 지점코드 |
| BARCODE_TYPE | String |  false | 쿠폰의 종류 BARCODE,AMOUNT |
| PRICE | Int |  false | 쿠폰의 금액, BARCODE 인 경우 공급가 전달, AMOUNT 인 경우 사용된 금액 전달 |
 
<br/><br/>
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

<br/><br/>
#### 응답 예제 (XML) 
```
<?xml version="1.0" encoding="EUC-KR"?>
<COUPONEXCHANGE>
	<RESULTCODE>00</RESULTCODE>
	<RESULTMSG>Success</RESULTMSG>
	<EXCHANGE_ID>1511751400419053</EXCHANGE_ID>
</COUPONEXCHANGE>
```

