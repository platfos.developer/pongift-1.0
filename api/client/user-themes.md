# user-themes

## /api/client/user-themes

### 1. gets
 #### 테마 조회

> * gets.validate() : 유효성 검사

> * gets.setParam() : 조건에 맞는 테마조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스 id |
| themeId | int |  false | 테마 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppProductTheme | AppProductThemes객체 |
 | AppProduct | AppProducts객체 |
 | AppService | AppServices객체 |
 | AppCorporation | AppCorporations객체 |
 | AppTheme | AppThemes객체 |
 | Image | Images객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
[
   {
      "id": 19,
      "themeName": "기프트",
      "imageName": "upload_1d6b951d2edbeadee8ef431b1ed85686.jpg",
      "imageFolder": "theme",
      "imageDateFolder": "2017-05-25",
      "imageAuthorized": 1
   }
]
```
