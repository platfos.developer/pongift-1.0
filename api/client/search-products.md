# search-products

## /api/client/search-products

### 1. get
 #### 상품 단일 얻기
    - 조건에 해당하는 단일상품을 조회한다.

> * get.validate() : 유효성 검사

> * get.setParam() : 가져올 쿼리 조건 설정

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int |  false | productId |

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | product | AppProduct객체 |
 | service | AppService객체 |
 | category | AppCategory객체 |
 | productImages | AppProductImage객체 |
 | productInfos | AppProductInfo객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON) 
```json
   {
     "product" : {
                 "service" : {
                             "corporation" : {}
                             },
                 "category" : {},
                 "productImages" : {
                                   "productImages" : {}
                                   },
                 "productInfos" : {}
                 },
   }
```


### 2. gets
 #### 상품 조회
    - 조건에 해당하는 상품을 조회한다.
    
> * gets.validate() : 유효성 검사

> * gets.setParam() : 조건에 해당하는 상품조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색 내용 |
| last |  |  false | 마지막 데이터 정보 |
| size | |  false | 가져올 데이터 크기 |
| serviceId | |  false | 서비스 id |
| weight | |  false | 상품 노출 가중치 (true / false) |
| offset | |  false | offset |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppProducts | AppProduct객체 |
 | AppProductInfo | AppProductInfo객체 |
 | AppProductImage | AppProductImage객체 |
 | AppCategory | AppCategory객체 |
 | AppServices | AppServices객체 |
 | AppCorporations | AppCorporations객체 |
 
 

 <br /><br /><br /><br />



#### 응답 예제 (JSON) 


```json
  {
    "AppProducts" : {
                    "AppProductInfo" : {},
                    "AppProductImage" : {},
                    "AppCategory": {},
                    "AppServices" : {},
                    "AppCorporations" : {}
                    }
  }
```
