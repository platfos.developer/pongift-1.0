# purchase-cancel

## /api/client/purchase-cancel

### 1. post
 #### 구매 취소
 
> * post.validate() : 구매 취소를 위한 유효성 검사 : 결제승인번호 체크  

> * post.checkCanCancel() : 구매 취소를 위한 유효성 검사 : '결제취소기간' / 클라이언트

> * post.smartInfiniCancelRequest() : 스마트인피니 상품인 경우 쿠폰제거

> * post.omnitelAllCancelRequest() : 옴니텔 상품인 경우 쿠폰제거

> * post.purchaseCancel() : 정상적으로 '쿠폰취소'된 경우 

> * post.purchaseCancel() : KCP 결제 취소

> * post.findPurchase() : 구매취소한 AppPurchase테이블 ROW 데이터 가져옴

> * post.initSupplyAdjustment() : 상품공급사인 경우 정산 초기화

> * post.initDistributionAdjustment() : 유통대행사인 경우 정산 초기화

> * post.cancelPurchase() : 정상적으로 '구매취소' 시 AppPurchase 업데이트


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| tno | int |  false | 주문 승인 번호 |
| nonmemberId | int |  false | 비회원 Id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | purchase | AppPurchase객체 |
 | service | AppService객체 |
 | product | AppProduct객체 |
 | productImages | AppProductImage객체 |
 | promotion | AppPromotion객체 |
 | productPromotion | AppProductPromotion객체 |
 | productTransfers | AppProductTransfer객체 |
 

 <br /><br /><br /><br />
 
 
 #### 응답 예제 (JSON) 
 ```json
 {
   "purchase" : {
                service : {} ,
                product : {
                          service : {},
                          productInfos : {},
                          productImages : {}
                          } ,
                promotion : {} ,
                productPromotion : {} ,
                productTransfers : {} ,
                }
 }
 
 ```

