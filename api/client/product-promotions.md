# product-promotions

## /api/client/product-promotions

### 1. gets
 #### 프로모션 상품 조회

> * gets.validate() : 유효성 검사

> * gets.setParam() : 조건에 해당하는 프로모션 상품 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| promotionId | int |  false | 프로모션 id |
| last | timestamp |  false | 마지막 데이터 정보 |
| size | int |  false | 가져올 데이터 크기 |
| serviceId | int |  false | 서비스 id |
| weight | boolean |  false | 상품 노출 가중치 (true / false) |
| offset | int |  false | offset |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppProduct | AppProducts객체 |
 | AppProductInfo | AppProductInfos객체 |
 | AppProductImage | AppProductImages객체 |
 | Image | Images객체 |
 | AppService | AppServices객체 |
 | AppCorporation | AppCorporations객체 |
 | AppProductPromotion  | AppProductPromotions객체 |
 

 <br /><br /><br /><br />

#### 응답 예제 (JSON) 

```json
  {
    "AppProduct" : {
                   "AppProductInfo" : {},
                   "AppProductImage" : {},
                   "Image" : {},
                   "AppService" : {},
                   "AppCorporation" : {},
                   "AppProductPromotion" : {}
                   }
  }
```
