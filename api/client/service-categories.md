# service-categories

## /api/client/service-categories

### 1. gets
 #### 테마 조회

> * gets.validate() : 유효성 검사

> * gets.setParam() : 카테고리 조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | false | categoryId |
| name | string | false | categoryName |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppCategory.id : 카테고리Id |
 | AppCategory.name | 카테고리이름 |

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
{
   "list": [
      {
         "id": 20,
         "name": "체험/전시"
      },
      {
         "id": 19,
         "name": "테마파크"
      },
      {
         "id": 18,
         "name": "편의점/마트"
      },
      {
         "id": 17,
         "name": "상품권/생활편의/기타"
      },
      {
         "id": 16,
         "name": "헤어/뷰티"
      },
      {
         "id": 15,
         "name": "화장품"
      },
      {
         "id": 12,
         "name": "영화/공연"
      },
      {
         "id": 9,
         "name": "베이커리/도넛/떡"
      },
      {
         "id": 7,
         "name": "분식/죽/간편식"
      },
      {
         "id": 6,
         "name": "버거/피자/치킨"
      },
      {
         "id": 4,
         "name": "패밀리레스토랑/뷔페"
      },
      {
         "id": 3,
         "name": "커피"
      }
   ]
}
```
