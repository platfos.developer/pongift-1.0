# product-themes

## /api/client/product-themes

### 1. gets
 #### 테마 상품 조회

> * gets.validate() : 유효성 검사 

> * gets.setParam() : 조건에 해당하는 상품테마 검색

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| themeId | int |  false | 테마 id |
| last | int |  false | 마지막 데이터 정보 |
| size | int |  false | 가져올 데이터 크기 |
| serviceId | int |  false | 서비스 id |
| weight | boolean |  false | 상품 노출 가중치 (true / false) |
| offset | int |  false | offset |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppProducts | AppProduct객체|
 | AppProductInfo | AppProductInfo객체 |
 | AppProductImages | AppProductImages객체 |
 | AppService | AppService객체 |
 | AppCorporation | AppCorporation객체 |
 | AppProductTheme | AppProductTheme객체 |
 
 

 <br /><br /><br /><br />

#### 응답 예제 (JSON) 

```json
   {
     "AppProducts" : {
                      "AppProductInfo" : {},
                      "AppProductImages" : {},
                      "AppService" : {},
                      "AppCorporation" : {},
                      "AppProductTheme" : {}
                     }
   
```

