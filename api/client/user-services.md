# user-services

## /api/client/user-services

### 1. get
 #### 단일 서비스조회

> * get.validate() : 유효성 검사

> * get.setParam() : 조건에 맞는 서비스 조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | serviceId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | service | AppService객체 |
 | logoImg | Image객체 |
 | category | AppCategory객체 |
 | calculations | AppCalculation객체 |

 <br /><br /><br /><br />
 
#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

#### 응답 예제 (JSON)  

```json
  {
     "service" : {
                 "logoImg" : {},
                 "category" : {},
                 "calculations" : {}
                 }
  }
```

### 2. gets
 #### 서비스 조회

> * gets.validate() : 유효성 검사

> * gets.setParam() : 조건에 맞는 서비스 조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| categoryId | int |  false | 카테고리 id |
| state | enum |  false | 현재 상태 standby, use, close |
| type | enum |  false | 서비스 유형 supply, distribution, etc |
| noSize | boolean |  false | limit 적용 유무 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | service | AppService객체 |
 | affiliation | AppAffiliation객체 |
 | corporation | AppCorporation객체 |
 | category | AppCategory객체 |
 | pongiftContact | User객체 |
 | logoImg | Image객체 |
 | calculations | AppCalculation객체 |
 | contacts | AppContact객체 |
 

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
   
   {
      "service" : {
                 "affiliation" : {},
                 "corporation" : {
                                 "logoImg" : {}
                                 },
                 "category" : {},
                 "pongiftContact" : {},
                 "logoImg" : {},
                 "calculations" : {},
                 "contacts' : {}
                  }
   }
```
