# client-users

## /api/client/client-users

### 1. post
 #### 클라이언트 회원 가입

> * post.validate() : 유효성검사

> * post.setParam() : user 생성 (requestAPI 로 users.post.js(회원가입)호출)

                     1) post.validate() : 유효성검사 <br>
                     2) post.checkCi() : 발급받은 고유번호가 있는지 조회 <br>
                     3) post.checkSocialProvider() : 모바일 접속 시 토큰 값 업데이트
                     4) post.createUser() : 유저 생성
                     5) post.createOptionalTerms() : 옵션 조건이 있다면 생성
                     6) post.sendEmailAuth() : 이메일 회원가입
                     

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| type | |  false | 가입 형태(제공자타입) email, phone, social, phoneId, normalId, phoneEmail, authCi |
| uid | |  false | 아이디, 그냥 아이디로만 가입할땐 일반 아이디, 이메일 가입이면 이메일, 전화번호가입이면 전화번호 |
| secret | |  false | 비밀번호 혹은 엑세스토큰, 전화번호가입이면 인증번호 |
| nick | |  false | 닉네임 |
| aid | |  false | 전화번호 가입을 할때 아이디 / 비밀번호를 이용할 경우 아이디 |
| apass | |  false | 전화번호 가입을 할때 아이디 / 비밀번호를 이용할 경우 비밀번호 |
| agreedPhoneNum | |  false | 휴대폰 수신 동의 |
| agreedEmail | |  false | 이메일 수신 동의 |
| platform | |  false | OS 및 버전 |
| device | |  false | 휴대폰 기종 |
| version | |  false | 앱버전 |
| token | |  false | 푸시를 위한 디바이스토큰 |
 

 #### Return

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

