# user-promotions

## /api/client/user-promotions

### 1. gets
 #### 프로모션 조회

> * gets.validate() : 유효성 검사

> * gets.findService() : 서비스 조회

> * gets.findServicePromotion() : 서비스에 해당하는 프로모션 조회 

> * gets.setParam() : user 프로모션 조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | |  false | 서비스 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | promotion | AppPromotion객체 |
 | service | AppService객체 |
 | corporation | AppCorporation객체 |
 | event | AppEvent객체 |
 | sdkBannerImg1 | Image객체 |
 | sdkBannerImg2 | Image객체 |
 | webBannerImg1 | Image객체 |
 | webBannerImg2 | Image객체 |
 

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```ㅜjson
  {
     "promotion" : {
                      "service" : {},
                      "corporation" : {},
                      "corporation" ; {},
                      "event" : {},
                      "sdkBannerImg1" : {},
                      "sdkBannerImg2" : {},
                      "webBannerImg1" : {},
                      "webBannerImg2" : {}
                      }
  }

```
