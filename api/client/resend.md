# resend

## /api/client/resend

### 1. post
 #### 옴니텔 상품재발송 - 클라이언트 페이지에서 '재전송'버튼을 위한 API 
> * post.validate() : '재발송'을 위한 유효성 검사 (상품발송ID / nomemberId)

> * post.getCurrentMeta() : AppMeta테이블의 Row 가져옴 : 재발송 횟수제한 

> * post.findProductTransfer() : '재발송'을 하기위해 해당하는 상품발송정보 가져옴

> * post.omnitelResendRequest() : 옴니텔 상품 '재발송' 요청

> * post.updateProductTransfer() : 정상적으로 '재발송'된 경우 '재발송 횟수'증가를 위해 AppProductTransfer테이블 업데이트


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productTransferId | |  false | 재전송할 대상 ID |
| nonmemberId | |  false | 비회원 Id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | productTransfer | AppProductTransfer객체 |
 

 <br /><br /><br /><br />

#### 응답 예제 (JSON) 

```json
{
 "productTransfer" : {
                     purchase : {
                                 product : { 
                                           service: {}
                                           }
                                }
                     }
}

```
