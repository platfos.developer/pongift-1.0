# purchases

## /api/client/purchases

### 1. get
 #### 구매상품 단일 조회

> * get.validate() : 유효성 검사

> * get.setParam() : 가져올 데이터 설정 및 조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| nonmemberId | int |  false |  | 
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | purchases | AppPurchase객체 |
 | product | AppProduct객체 |
 | service | AppService객체 |
 | theme | AppTheme객체 |
 | promotion | AppPromotion객체 |
 | productPromotion | AppProductPromotion객체 |
 | productTransfers | AppProductPromotion객체 |
 | nonmember | AppNonmember객체 | 

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON) 
 
 ```json
{ 
  "purchases" : {
                "product" : {
                            "productImages" : {
                                              "image" : {}
                                              },
                            "category" : {},
                            "service" : {}
                            }
                },
                "service" : {
                            "corporation" : {}
                            },
                "theme" : {},
                "promotion" : {},
                "productPromotion" : {},
                "productTransfers" : {},
                "nonmember" : {}
}
 ```

### 2. gets
 #### 비회원 구매 조회

> * gets.validate() : 유효성 검사

> * gets.setParam() : 쿼리조건 설정

> * gets.findPurchases() : 조건에 해당하는 쿼리조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | timestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| nonmemberId | int |  false | 비회원 구매자 ID |
| serviceId | int |  false | 서비스 ID |
| notState | enum |  false | 구매 상태 제외 standby, purchase, cancel |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | purchase | AppPurchase객체 |
 | service | AppService객체 |
 | product | AppProduct객체 |
 | promotion | AppPromotion객체 |
 | productPromotion | AppProductPromotion객체 |
 | theme | AppTheme객체 |
 | user | User객체 |
 | productTransfers | AppProductTransfer객체 |

 <br /><br /><br /><br />


#### 응답 예제 (JSON) 

```json
  {
    "purchase" : {
                 "service" : {
                             "corporation" : {}
                             },
                 "product" : {
                             "service" : {
                                         "corporation" : {} 
                                         },
                             "productImages" : {
                                               "image" : {}
                                               }
                             },
                 "promotion" : {},
                 "productPromotion" : {},
                 "theme" : {},
                 "user" : {},
                 "productTransfers" : {}
                 }
  }
```

### 3. post
 #### 비회원 구매 생성

> * gets.validate() : 유효성 검사

> * post.getCurrentMeta() : AppMeta설정값 조회

> * post.getNonmember() : 비회원 조회

> * post.createPurchase() : 구매데이터 생성 (옴니텔 상품인 경우)

> * post.createSingleOptionPurchase() : 구매데이터 생성 (스마트 인피니 상품인 경우)


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| nonmemberId | int |  false | 비회원아이디 |
| serviceId | int |  false | 서비스 id |
| productId | int |  false | 상품 id |
| promotionId | int |  false | 프로모션 id |
| themeId | int |  false | 테마 id |
| type | enum |  false | 구매 유형 |
| destinations | |  false | 수신인 |
| amounts | varchar |  false | 구매 수량 |
| charge | float |  false | 수수료 |
| customerPrice | int |  false | 소비자가격 |
| supplyPrice | int |  false | 공급가격 |
| realPrice | int |  false | 실판매가격 |
| vatType | enum |  false | vat 포함 유무 |
| message | varchar |  false | 메세지 첨부 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | purchase | AppPurchase객체 |
 | user | User객체 (비회원이 아닌 회원인경우 조회) |
 | service | AppService객체 |
 | product | AppProduct객체 |
 | theme | AppTheme객체 |
 | promotion | AppPromotion객체 |
 | productTransfers | AppProductTransfer객체 |
 

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
  {
    "purchase" : {
                 "user" : {},
                 "service" : {
                             "calculations" : {}
                             },
                 "product" : {
                             "service" : {},
                             "productInfos" : {}
                             },
                 "theme" : {},
                 "promotion" : {},
                 "productTransfers": {}
                 }
  }
```
