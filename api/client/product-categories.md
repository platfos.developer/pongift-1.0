# product-categories

## /api/client/product-categories

### 1. gets
 #### 상품 카테고리 조회
    - 서비스에 해당하는 카테고리 조회
> * gets.setParam() : 조건에 해당하는 서비스 카테고리 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200


 | parameter | desc |
 | --------- | :---- |
 | AppProduct | AppProducts객체 |
 | AppService | AppServices객체 |
 | AppCorporation | AppCorporations객체 |
 | AppCategorie | AppCategories객체 |
 

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
{
   "list": [
      {
         "id": 21,
         "name": "커피"
      }
   ]
}
```
