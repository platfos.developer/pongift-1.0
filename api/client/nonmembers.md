# nonmembers

## /api/client/nonmembers

### 1. get
 #### 단일 비회원 정보얻기

> * AppNonmember.upsertNonmember() : 조건에 해당하는 비회원 정보조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | |  false | 서비스 ID |
| serviceUid | |  false | 서비스 유저 id (계정) |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppNonmember | AppNonmember객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
   "type": null,
   "serviceId": 21,
   "phoneNum": null,
   "serviceUid": "1",
   "name": null,
   "createdAt": 1512358079031998,
   "updatedAt": 1512358079032010,
   "deletedAt": null,
   "id": 17243
}
```
