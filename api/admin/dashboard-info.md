# dashboard-info

## /api/admin/dashboard-info

### 1. get
 #### 게시글 조회

> * get.validate() : 유효성검사

> * get.parseTimeZoneOffset() : 시간 설정

> * get.getUsersStatus() : 유저 일별 정보조회

> * get.getUsersStatusByMonth() : 유저 월별조회

> * get.getUserAgeGroup() : 유저 나이별조회

> * get.getReportsStatus() : 답변 일별 조회

> * get.getReportsStatusByMonth() : 답변 월별 조회

> * get.getImagesStatus() : 이미지조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| timeZoneOffset | |  false | ex) +09:00 |
| year | int |  false | ex) 1992 |
| month | int |  false | ex) 03 |
| day | | int  false | ex) 12 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | usersStatus | usersStatus객체 |
 | usersStatusByMonth | Users객체 |
 | deletedUsers | Users객체 |
 | userAgeGroup | Users객체 |
 | reportsStatus | Report객체 |
 | reportsStatusByMonth | Report객체 |
 | solvedReports | Report객체 |
 | imagesStatus | imagesStatus객체 |
 

 <br /><br /><br /><br />


```json
{
   "usersStatus": {
      "usersTotal": 156,
      "usersDeletedTotal": 0,
      "singUpToday": 0,
      "loginTotal": 47,
      "loginToday": 0,
      "deletedUserToday": 0
   },
   "usersStatusByMonth": {
      "createdUsers": {
         "11_month": {
            "month": 11,
            "count": 0
         },
         "10_month": {
            "month": 10,
            "count": 0
         },
         "9_month": {
            "month": 9,
            "count": 0
         },
         "8_month": {
            "month": 8,
            "count": 0
         },
         "7_month": {
            "month": 7,
            "count": 0
         }
      },
      "deletedUsers": {
         "11_month": {
            "month": 11,
            "count": 0
         },
         "10_month": {
            "month": 10,
            "count": 0
         },
         "9_month": {
            "month": 9,
            "count": 0
         },
         "8_month": {
            "month": 8,
            "count": 0
         },
         "7_month": {
            "month": 7,
            "count": 0
         }
      }
   },
   "userAgeGroup": [
      {
         "ageGroup": null,
         "count": 156
      }
   ],
   "reportsStatus": {
      "total": 6,
      "solved": 6,
      "reportsToday": 0,
      "solvedToday": 0
   },
   "reportsStatusByMonth": {
      "createdReports": {
         "11_month": {
            "month": 11,
            "count": 0
         },
         "10_month": {
            "month": 10,
            "count": 0
         },
         "9_month": {
            "month": 9,
            "count": 0
         },
         "8_month": {
            "month": 8,
            "count": 0
         },
         "7_month": {
            "month": 7,
            "count": 0
         }
      },
      "solvedReports": {
         "11_month": {
            "month": 11,
            "count": 0
         },
         "10_month": {
            "month": 10,
            "count": 0
         },
         "9_month": {
            "month": 9,
            "count": 0
         },
         "8_month": {
            "month": 8,
            "count": 0
         },
         "7_month": {
            "month": 7,
            "count": 0
         }
      }
   },
   "imagesStatus": {
      "total": 1283,
      "unauthorized": 0
   }
}
```
