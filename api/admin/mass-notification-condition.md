# mass-notification-condition

## /api/admin/mass-notification-condition

### 1. post
 #### 조건 선택 메세지 전송

> * post.validate() : 유효성검사

> * post.validateMessageBody() : (보낼)메세지 유효성검사

> * post.findUsers() : 조건에 맞는 유저조회

> * post.createMassNotification() : 전송정보 생성

> * post.setImage() : 보낼 메세지에 파일이 있는경우의 설정값

> * post.sendMassNotification() : 순차적으로 메세지전송 (async.series)

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| sendType | enum |  false | 전송타입 email, push, message |
| gender | enum |  false | 성별 m / f |
| minBirthYear | int |  false | 최소 생년 |
| maxBirthYear | int |  false | 최대 생년 |
| platform | enum |  false | 핸드폰기종 all, android, ios |
| notificationName | string |  false | 알림 이름 |
| sendMethod | enum |  false | sms, lms, mms |
| messageTitle | string |  false | 제목 |
| messageBody | string |  false | 내용 |
| messageImg | string |  false | 이미지 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | MassNotification | 객체 |
 

 <br /><br /><br /><br />

```json
  {
     MassNotification : {}
  }
```
