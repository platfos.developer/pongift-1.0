# company-info

## /api/admin/company-info

### 1. get
 #### 단일 얻기
     - 회사정보 조회
> *  CompanyInfo.findDataById() : 회사정보조회

#### 요청 파라미터 
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
 
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | CompanyInfo | CompanyInfo객체 |

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
{
   "companyName": "(주)플랫포스",
   "representative": "신영준",
   "regNum": "248-86-00265",
   "privateInfoManager": "정원용",
   "address": "서울특별시 중구 퇴계로 36길 201호 (필동2가,동국대학교충무로영상센터)",
   "communicationsRetailReport": "2016-서울중구-1060",
   "contact": "02-597-0660",
   "contact2": null,
   "fax": null,
   "email": "contact@platfos.com",
   "hostUrl": null,
   "id": 1,
   "createdAt": "2017-02-09T04:46:30.000Z",
   "updatedAt": "2017-08-02T05:13:06.000Z"
}
```

### 2. put
 #### 회사정보 수정

> * put.validate() : 유효성검사

> * put.setParam() : 회사정보 수정을 위한 설정

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| companyName | string |  false | 회사이름 |
| representative | string |  false | 대표자 |
| regNum | string |  false | 사업자등록번호 |
| privateInfoManager | string |  false | 개인정보보호관리자 |
| communicationsRetailReport | |  false |  | 
| address | string |  false | 회사주소 |
| contact | string |  false | 연락처 |
| contact2 | string |  false |  | 
| fax | string |  false |  | 
| email | string |  false |  | 
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | CompanyInfo | CompanyInfo객체 |

 <br /><br /><br /><br />

```json
{
   "companyName": "(주)플랫포스",
   "representative": "신영준",
   "regNum": "248-86-00265",
   "privateInfoManager": "정원용",
   "address": "서울특별시 중구 퇴계로 36길 201호 (필동2가,동국대학교충무로영상센터)",
   "communicationsRetailReport": "2016-서울중구-1060",
   "contact": "02-597-0660",
   "contact2": null,
   "fax": null,
   "email": "contact@platfos.com",
   "hostUrl": null,
   "id": 1,
   "createdAt": "2017-02-09T04:46:30.000Z",
   "updatedAt": "2017-08-02T05:13:06.000Z"
}
```
