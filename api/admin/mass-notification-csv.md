# mass-notification-csv

## /api/admin/mass-notification-csv

### 1. post
 #### csv 메세지 전송

> * post.validate() : 유효성검사

> * post.checkNCreatePart() : MassNotification 로우 생성

> * post.series() : csv 메세지 전송<br>
          - post.seriesSplitFile : json데이터를 csv로 변환 <br>
          - post.seriesSendImportFile : (저장경로를 s3로 설정했다면) import한 파일를 EC2 S3로 이동<br>
          - post.seriesMoveImportFile : (저장경로는 로컬로 설정했다면) import한 파일을 로컬 파일이동<br>
          - post.seriesReadSplitFileCreateMassNotification : 파일을 읽은 후 MassNotificationDest테이블의 로우 생성<br>
          - post.seriesRemoveSplitFiles : 파일제거<br>
          - post.seriesCountMassNotification : MassNotification테이블 로우 수 조회<br>
          - post.seriesFindMassNotificationPhoneNumNSendMessage : 조건에 맞는 MassNotification테이블 로우조회<br>
          - post.seriesSendMessageFile : 파일을 S3로 이동

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| folder | string |  false | mms 이미지 저장될 폴더notification |
| sendType | enum |  false | email, push, message |
| sendMethod | enum |  false | sms, lms, mms |
| notificationName | string |  false | 전송 제목 |
| messageTitle | string |  false | 메세지 제목 |
| messageBody | string |  false | 메세지 내용 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | true | 성공 |
 | error{} | 실패코드 |

 <br /><br /><br /><br />
 

#### 응답 예제 (JSON)  

```
{
 *성공 시 
 true
 
 *실패 시
 {errCode : '002'}
}
