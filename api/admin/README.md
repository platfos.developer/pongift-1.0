## admin

### 1. [회사정보조회](/markdown/api/admin/company-info.md)

### 2. [게시글 조회](/markdown/api/admin/dashboard-info.md)

### 3. [메세지 전송](/markdown/api/admin/mass-notification-condition.md)

### 4. [csv 메세지 전송](/markdown/api/admin/mass-notification-csv.md)

### 5. [유저 선택 메세지 전송](/markdown/api/admin/mass-notification-select.md)

### 6. [전송관리](/markdown/api/admin/mass-notifications.md)

### 7. [유저](/markdown/api/admin/users.md)
