# mass-notifications

## /api/admin/mass-notifications

### 1. get
 #### 메세지전송 단일 얻기

> * get.validate() : 유효성검사

> * get.setParam () : MassNotification테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | MassNotificationId |

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | MassNotification | MassNotifications객체 |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
  {
    MassNotification : {}  
  }
```


### 2. gets
 #### 전송관리 조회

> * 

> * 

> * 

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchField | enum |  false | 검색 필드 notificationName, messageTitle, messageBody |
| searchItem | string |  false | 검색 내용 |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | timeStamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| key | enum |  false | 전송 유형 notice, event, emergency |
| sendType | enum |  false | 전송 방식 email, push, message |
| isStored  enum| |  false | Box 저장 여부 true, false |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | MassNotification | MassNotifications객체 |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
 MassNotification : {}
}
```

### 3. delete
 #### 전송관리 제거

> * del.validate() : 유효성검사

> * del.destroy() : MassNotification테이블 로우 삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | MassNotificationId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | MassNotification | MassNotifications객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
  {
    MassNotification : {}
  }
