# users

## /api/admin/users

### 1. gets
 #### 유저 리스트 얻기

> * gets.validate() : 유효성검사

> * gets.getUsers() : 조건에 맞는 User테이블 조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id, nick, email, phoneNum |
| last | microTimeStamp |  false | 마지막 데이터 |
| size | int |  false | 몇개 로드할지에 대한 사이즈 |
| orderBy | enum |  false | 정렬 기준 필드 orderCreate, orderUpdate |
| sort | enum |  true  | 정렬 순서 DESC, ASC |
| role | enum |  false | 유저 권한 roleA, roleB, roleC, roleD, roleE, roleF, roleG, roleS |
| gender | enum |  false | 성별 m, f |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | count | 찾은 개수 |
 | rows | 데이터 (배열) |
 

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
{
   "count": 1,
   "rows": [
      {
         "aid": "admin@pongift.com",
         "email": "admin@pongift.com",
         "secret": "/==",
         "salt": "=",
         "phoneNum": null,
         "name": null,
         "nick": "pongift",
         "role": "roleF",
         "gender": null,
         "birth": null,
         "isVerifiedEmail": false,
         "country": "KR",
         "language": "ko",
         "isReviewed": false,
         "agreedEmail": true,
         "agreedPhoneNum": true,
         "agreedTermsAt": 1484730153894174,
         "profileId": 2,
         "di": null,
         "ci": null,
         "createdAt": 1484730154064418,
         "updatedAt": 1484730154064466,
         "deletedAt": null,
         "passUpdatedAt": 1484729989732499,
         "id": 2,
         "profile": {
            "state": "normal",
            "wedding": null,
            "createdAt": 1484730154044620,
            "updatedAt": 1484730154044782,
            "deletedAt": null,
            "id": 2
         },
         "providers": [],
         "loginHistories": [
            {
               "userId": 2,
               "type": "email",
               "platform": "Mac OS",
               "device": null,
               "browser": "Chrome",
               "version": null,
               "token": null,
               "ip": "::1",
               "session": "EtYB_jVo4",
               "createdAt": 1512012847160482,
               "updatedAt": 1512012847160509,
               "id": 233
            },
         ],
         "userImages": []
      }
   ]
}
```
