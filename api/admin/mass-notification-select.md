# mass-notification-select

## /api/admin/mass-notification-select

### 1. post
 #### 유저 선택 메세지 전송

> * post.validate() : 유효성검사

> * post.checkNCreatePart() : MassNotification테이블 로우생성

> * post.series() : 유저선택 메세지전송 <br>
       - post.seriesSplitFile : S3 / 로컬에 따른 path경로 설정 후 파일생성<br>
       - seriesSendImportFile : (파일경로가 S3인경우) S3에 파일생성<br>
       - seriesMoveImportFile : (파일경로가 로컬인경우) 로컬에 파일이동<br>
       - post.seriesReadSplitFileCreateMassNotificationPhoneNum : MassNotificationDest테이블 로우생성<br>
       - post.seriesRemoveSplitFiles : 파일제거<br>
       - seriesCountMassNotificationPhoneNum : 조건에 맞는 MassNotificationDest테이블 로우개수 조회<br>
       - seriesFindMassNotificationPhoneNumNSendMessage : 조건에 맞는 MassNotificationDest테이블 로우조회<br>
       - seriesSendMessageFile : S3에 csv파일생성<br>

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userIds | int |  true | 유저 아이디 ,로 구분 |
| sendType | enum | true | 전송타입 email, push ,message |
| sendMethod | enum | true | 전송방법 sms, lms, mms |
| massNotificationTitle | string | true | 제목 |
| messageBody | string | true | 메세지 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | true | 성공 |
 | false | 실패 |
 | errCode | 실패정보 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```
*성공*
true

*실패*
1) errCode {}
2) false     
```
