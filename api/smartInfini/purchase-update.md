# purchase-update

## /api/smartInfini/purchase-update

### 1. get
 #### 거래내역 업데이트

> * get.validate : 올바른 파라미터(날짜형식)이 왔는지 체크
> * get.setParam : 날짜형식 타입변환
> * get.updateOrderList : 스마트인피니 상태조회 후 업데이트
    <br> - 스마트인피니 상품들을 배열로 받고, async.mapSeries를 이용하여 배열에 담은 날짜별 데이터를 순차적으로 메서드실행
    <br> - 배열안의 각각 데이터를 async.series통해 동시에 상품업데이트 처리
    <br> - getRequestOrderList() : 스마트인피니 상태조회
    <br> - initAdjustment() : 정산 초기화
    <br> - searchUpdate() : 상태 업데이트 

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseStartAt | DATE |  false | 거래내역업데이트 시작날짜 |
| purchaseEndAt | DATE |  false | 거래내역업데이트 끝날짜 |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

