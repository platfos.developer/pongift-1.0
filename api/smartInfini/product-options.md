# file-to-db

## /api/smartInfini/file-to-db
 - 기존 스마트인피니 주문정보 파일들을 DB로 적용하는 API (파일 -> DB화)

### 1. get
> * get.findPurchase() : 상품구매ID에 해당하는 상품정보를 가져옴

> * get.updateProductTransfers() : 해당 상품ID의 상품정보를 가지고 AppPurchaseBarcodeInfo테이블의 ROW 생성  
                                   ( 스마트인피니 파일정보 = ROW)
> * get.updatePurchase() : 변화된 데이터가 있다면 update

#### 요청 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseId | int | true | 상품구매ID | 
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

