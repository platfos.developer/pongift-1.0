# cencel

## /api/smartInfini/cencel

### 1. patch
#### 스마트인피니 주문취소 API

> * 스마트인피니 resellerCd와 token을 사용

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| orderNum | int |  false | 주문번호 | 
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | code | 실패/성공 결과코드 <br>E001 : 인증토큰값 에러 <br> E002 : 유효성검사 실패 <br> E003 : 처리 실패 <br> E004 : 주문이 존재하지 않습니다. <br> E100 : 스마트인피니 서버 에러| 
 message | 결과코드에 따른 실패 메시지 |

 <br /><br /><br /><br />


#### 응답 예제 (JSON) 

```json
*HTTP Status Code 200
{
  "code": "0000",
  "message": ""
}

*HTTP Status Code 400
{
  "code": "E001",
  "message": "유효하지 않은 토큰입니다."
}

```
