## 스마트인피니

### 1. [취소](/markdown/api/smartInfini/cencel.md)
### 2. [파일업로드 (DB)](/markdown/api/smartInfini/file-to-db.md)
### 3. [구매취소](/markdown/api/smartInfini/purchase-admin-cancel.md)
### 4. [애플리케이션 구매 취소](/markdown/api/smartInfini/purchase-app-cancel.md)
### 5. [스마트인피니 상태값 업데이트](/markdown/api/smartInfini/purchase-update.md)
### 6. [재전송](/markdown/api/smartInfini/resend.md)