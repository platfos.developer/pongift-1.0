# resend

## /api/smartInfini/resend

### 1. get
   -상품 재발송(문자 재전송)
   
> * get.validate() : 유효성검사

> * get.request() : 문자재전송 (신규발송X)

> * get.updateOptionProductTransfer() : updateOptionProductTransfer테이블의 로우 업데이트 (재전송 카운트)

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| orderNum | int | false | 주문번호 |
| productTransfersId | int | false | productTransfersId 고유번호 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
  {
     
  }
```

### 2. post
   -상품 신규발급(쿠폰생성 에러발생시 사용)
   
> * post.validate(): 유효성검사

> * post.findPurchaseByPurchaseCode() : AppPurchase테이블 조회

> * post.smartInfiniSendRequest() : 상품발송

> * post.updatePurchase() : AppPurchase/AppProductTransfer테이블 업데이트

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| orderNum | |  false | 상품주문번호 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  


```json
  {
     
  }
```
