# purchase-app-cancel

## /api/smartInfini/purchase-app-cancel

### 1. post
 #### 구매 취소

> * post.checkCanCancelNonMember : 취소 요청한 상품이 DB에 존재하는 상품인지 조회 
    <br>- 에러코드 : 403_0002 / 400_0062 / 400_0068 / 404_0028 (하단 에러상세 참조)  
> * post.smartInfiniAllCancelRequest : 스마트인피니 상품취소API 요청 후 DB업데이트
    <br>- 에러발생 시 파일생성 : fileWriteSmartInfiniSendFail()
> * post.checkStoreFarmPurchase : 네이버스토어팜에서 구매한 상품인지 체크
    <br>- 내부 KCP결제를 했는지 체크하기위해서 체크함
> * post.purchaseCancel : KCP 결제취소 
    <br>- 네이버스토어팜에서 구매하지 않은 상품인 경우 KCP결제취소를 해야함
> * post.findPurchase : 구매취소한 상품정보를 조회
> * post.initSupplyAdjustment : 유통대행사인 경우의 정산초기화작업
> * post.initDistributionAdjustment : 상품공급사인 경우의 정산초기화작업
> * post.cancelPurchase : 상품취소 및 결제취소가 정상적으로 됬을 경우 DB업데이트


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseId | int |  false | 상품고유번호 |
| orderNum | | int false | 상품주문번호 |
| nonmemberId | int |  false | 비회원ID |
 
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :-------------- |
 | purchase | AppPurchase객체 |
 | service | AppService객체 | 
 | product | AppProduct객체 |
 | promotion | AppPromotion객체|
 | productPromotion | AppProductPromotion객체 |
 | productTransfers | AppProductTransfer 객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON) 

```json

{
   "purchase": {}
   "service": {
      "calculations": [
         {
         }
      ]
   },
   "product": {
      "service": {
         "calculations": [
            {

            }
         ]
      },
      "productInfos": [
         {

         }
      ],
      "productImages": [
         {

            }
         }
      ]
   },
   "promotion": {},
   "productPromotion": {},
   "productTransfers": [
      {

      }
   ]
}

```

#### 에러 코드 
  |code | message|
  |:----:|:------:|
  | 400_0062 | 구매취소 가능한 대상이 아닙니다.|
  | 400_0068 | 결제 취소 가능 기간이 아닙니다. |
  | 404_0028 | 존재하지 않는 구매정보입니다. |
  | 403_0002 | 결제 취소할 권한이 없습니다. |

