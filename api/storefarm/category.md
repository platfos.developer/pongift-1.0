# category

## /api/storefarm/category

### 1. post
#### 스토어팜 상품 카테고리 검색 

> * 권한 (roleUltraAdmin)

> * 스토어팜의 카테고리 파일 저장 (www/category.xml)


#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음


#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
