# reject-return

## /api/storefarm/reject-return

### 1. post
#### 환불 요청 거절 api

> * 스토어팜에서 사용자가 상품 환불 요청시 거절 API

> * setRejectReturn(): 상품 환불 거절 XML 작성 - RejectReturnRequest

> * sendRejectReturn(): 스토어팜에 환불 거절 요청 

> * updateStoreFarmOrderInfo(): 상품주문내역에 환불 거절 내용 업데이트 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | Int |  false | 스토어팜에서 제공한 productOrderId |
| rejectDetailContent | String |  false | 반품 거부 사유 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
 <br /><br /><br /><br />

