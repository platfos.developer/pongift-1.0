# third-party-category

## /api/storefarm/third-party-category

### 1. get
#### 스토어팜 상품 카테고리 조회 - 기존 AppCategory 사용으로 해당 API 사용하지 않음


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
 

 #### Return

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

### 2. gets - 기존 AppCategory 사용으로 해당 API 사용하지 않음


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | |  false |  | 
 

 #### Return

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

