# product-gift-order-detail

## /api/storefarm/product-gift-order-detail

### 1. get
 #### 일반 주문 확인 상세 api

> * productOrderId의 상세 내역 조회 

> * setParam() 요청 XML 작성

> * requestProductOrderList() 상세 내역 요청

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | Int |  false |  | 
 

 #### Return
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음 <br /><br /><br /><br />

### 2. post
#### 선물하기 주문 상세 확인 api

> * productOrderPayedToGift() 선물하기이며 발송되지 않은 주문건을 가져옴
> * setParam() 요청 XML 작성
> * requestProductOrderList() 상세 내역 요청
> * updateStoreFarmOrderList() 상세내역 업데이트
> * findThirdPartyProduct() 스토어팜 상품 확인
> * reqPurchase() /api/storefarm/purchase 요청
> * setShipOrderParam() 배송 완료 XML작성
> * requestProductOrderConfirm() 완료 요청
> * setModifyECouponValidDate() 쿠폰상태 수정 XML 작성
> * requestModifyECouponValidDateRequest() 완료 요청
#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 #### Return
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 <br /><br /><br /><br />

### 3. put
 #### 선물하기 주문 상세 확인 api

> * 선물하기 완료 처리
> * setShipOrderParam() 배송 완료 XML작성
> * requestProductOrderConfirm() 완료 요청
> * setModifyECouponValidDate() 쿠폰상태 수정 XML 작성
> * requestModifyECouponValidDateRequest() 완료 요청

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | Int |  false |  | 
| externalCode | String |  false |  | 
| storefarmOrderInfoId | Int|  false |  | 
 

 #### Return
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 <br /><br /><br /><br />

