# change-ecoupon-status

## /api/storefarm/change-ecoupon-status

### 1. post
#### 스토어팜 쿠폰 사용 확인 요청 

> * 발급된 쿠폰을 사용자가 사용시 스토어팜에 해당 쿠폰 사용으로 요청

> * findStoreFarmPurchase(): 주문ID로 해당 주문 검색

> * setParam(): 요청 XML 작성

> * sendCollectedExchange(): 스토어 팜에 요청 및 저장 

 <br/><br/>
#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| externalCode | int |  false | 쿠폰 번호 |
 

 <br/><br/>
 
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
