# bulk

## /api/storefarm/bulk

### 1. gets
#### 스토어팜 벌크 데이터 검색  

> * 권한 (roleUltraAdmin)

> * 어드민->기타관리->벌크데이터 의 남은 벌크 데이터 갯수 리턴

 <br /><br />
#### 요청 파라미터 
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 <br /><br />
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | count | 남은 상품 총합 카운트 | 
 | productId | 상품 Id | 

 <br /><br />
#### 응답 예제 
```
{
	"data": [
		{
			"count": 7,
			"productId": 42
		},
		{
			"count": 4,
			"productId": 1253
		}
	]
}
```
