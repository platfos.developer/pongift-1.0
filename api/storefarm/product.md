# product

## /api/storefarm/product

### 1. get
 #### 스토어팜 단일 상품 정보 가져오기

> * 스토어팜에 등록된 상품중 요청 ID의 상품정보 가져오기 

> * setParam() 스토어팜에 params.id로 상품정보 요청 xml 작성

> * requestProduct() 스토어팜에 상품정보 요청

#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
- 스토어팜 상품 객체 리턴

<br/><br/>
#### 응답 예제 (XML) 
```
{
   "status": 200,
   "data": {
      "n:ProductId": [
         "2047110659"
      ],
      "n:StatusType": [
         "SUSP"
      ],
      "n:SaleType": [
         "NEW"
      ],
      "n:CustomMade": [
         "N"
      ],
      "n:CategoryId": [
         "50001625"
      ],
      "n:LayoutType": [
         "BASIC"
      ],
      "n:Name": [
         "[롯데리아] 순살치킨풀팩"
      ],
      "n:Model": [
         {
            "n:ManufacturerName": [
               "롯데리아"
            ],
            "n:BrandName": [
               "롯데리아"
            ]
         }
      ],
      "n:ChildCertifiedProductExclusion": [
         "N"
      ],
      "n:KCCertifiedProductExclusion": [
         "N"
      ],
      "n:GreenCertifiedProductExclusion": [
         "N"
      ],
      "n:OriginArea": [
         {
            "n:Code": [
               "00"
            ],
            "n:Plural": [
               "N"
            ],
            "n:Content": [
               "국산"
            ]
         }
      ],
      "n:TaxType": [
         "TAX"
      ],
      "n:MinorPurchasable": [
         "Y"
      ],
      "n:Image": [
         {
            "n:Representative": [
               {
                  "n:URL": [
                     "http://shop1.phinf.naver.net/20171124_260/platfos0914_1511485800775EtliJ_JPEG/upload_a4ab493c7c2881f317a02af2c0c3b487.jpg"
                  ]
               }
            ]
         }
      ],
      "n:DetailContent": [
         "롯데리아

□이용방법\n★상품 교환방법\n -롯데리아 전매장 사용가능\n -일부 매장 제외(휴게소, 특설매장, 미판매점 교환 불가/홈서비스 제외)



★유의사항\n-유효기간 이내 사용\n-교환시 할인 및 무료증정 등 매장행사 적용 불가.\n-교환 시 제휴 통신사 및 카드사 할인, 멤버십 적립이 적용 불가.\n-본 교환권은 타 쿠폰(모바일 쿠폰, 제품교환권 등)과 중복 사용이 불가.\n-매장 별 재고 상황에 따라 동일 금액의 타상품으로 교환이 가능.\n-해당 금액보다 높을 경우 초과금액을 결제하시면 교환 가능. (단, 해당 금액보다 낮을 경우 잔액 환불이 되지 않습니다.)\n-세트 상품인 경우, 디저트 및 드링크의 변경이 가능하나 추가 금액이 발생할 수 있습니다.\n-해당상품 가격인상시, 차액에 대해 추가결제시 교환 가능합니다.



제조사/ 브랜드: 롯데리아

\"Smiley"
      ],
      "n:AfterServiceTelephoneNumber": [
         "02-597-0660"
      ],
      "n:AfterServiceGuideContent": [
         "상품 상세 참조"
      ],
      "n:PurchaseReviewExposure": [
         "Y"
      ],
      "n:RegularCustomerExclusiveProduct": [
         "N"
      ],
      "n:KnowledgeShoppingProductRegistration": [
         "Y"
      ],
      "n:SalePrice": [
         "15700"
      ],
      "n:StockQuantity": [
         "99999"
      ],
      "n:MaxPurchaseQuantityPerId": [
         "99999"
      ],
      "n:MaxPurchaseQuantityPerOrder": [
         "3"
      ],
      "n:ECoupon": [
         {
            "n:PeriodType": [
               "FB"
            ],
            "n:PeriodDays": [
               "93"
            ],
            "n:PublicInformationContents": [
               "(주)플랫포스"
            ],
            "n:ContactInformationContents": [
               "02-597-0660"
            ],
            "n:UsePlaceType": [
               "PLACE"
            ],
            "n:UsePlaceContents": [
               "전매장가능(일부매장제외)"
            ],
            "n:RestrictCart": [
               "N"
            ]
         }
      ],
      "n:ProductSummary": [
         {
            "n:MobileCoupon": [
               {
                  "n:NoRefundReason": [
                     "상품 상세 참조"
                  ],
                  "n:ReturnCostReason": [
                     "상품 상세 참조"
                  ],
                  "n:QualityAssuranceStandard": [
                     "상품 상세 참조"
                  ],
                  "n:CompensationProcedure": [
                     "상품 상세 참조"
                  ],
                  "n:TroubleShootingContents": [
                     "소비자분쟁해결기준(공정거래위원회 고시) 및 관계법령에 따릅니다."
                  ],
                  "n:Issuer": [
                     "(주)플랫포스"
                  ],
                  "n:UsableCondition": [
                     "90일"
                  ],
                  "n:UsableStore": [
                     "전매장가능(일부매장제외)"
                  ],
                  "n:CancelationPolicy": [
                     "상품 상세 참조"
                  ],
                  "n:CustomerServicePhoneNumber": [
                     "02-597-0660"
                  ]
               }
            ]
         }
      ],
      "n:SellerCommentUsable": [
         "N"
      ],
      "n:SellerCustomCode1": [
         "506"
      ]
   }
}
```

<br /><br /><br /><br />

### 2. gets
#### 스토어팜 상품 리스트 가져오기

> * 스토어팜에 등록된 상품정보 가져오기 

> * setParam() 스토어팜에 상품정보 요청 xml 작성

> * requestProduct() 스토어팜에 상품정보 요청

#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
- 스토어팜 상품 객체 리턴

<br/><br/>
#### 응답 예제 (XML) 
```
{
   "status": 200,
   "data": [{
      "n:ProductId": [
         "2047110659"
      ],
      "n:StatusType": [
         "SUSP"
      ],
      "n:SaleType": [
         "NEW"
      ],
      "n:CustomMade": [
         "N"
      ],
      "n:CategoryId": [
         "50001625"
      ],
      "n:LayoutType": [
         "BASIC"
      ],
      "n:Name": [
         "[롯데리아] 순살치킨풀팩"
      ],
      "n:Model": [
         {
            "n:ManufacturerName": [
               "롯데리아"
            ],
            "n:BrandName": [
               "롯데리아"
            ]
         }
      ],
      "n:ChildCertifiedProductExclusion": [
         "N"
      ],
      "n:KCCertifiedProductExclusion": [
         "N"
      ],
      "n:GreenCertifiedProductExclusion": [
         "N"
      ],
      "n:OriginArea": [
         {
            "n:Code": [
               "00"
            ],
            "n:Plural": [
               "N"
            ],
            "n:Content": [
               "국산"
            ]
         }
      ],
      "n:TaxType": [
         "TAX"
      ],
      "n:MinorPurchasable": [
         "Y"
      ],
      "n:Image": [
         {
            "n:Representative": [
               {
                  "n:URL": [
                     "http://shop1.phinf.naver.net/20171124_260/platfos0914_1511485800775EtliJ_JPEG/upload_a4ab493c7c2881f317a02af2c0c3b487.jpg"
                  ]
               }
            ]
         }
      ],
      "n:DetailContent": [
         "롯데리아

□이용방법\n★상품 교환방법\n -롯데리아 전매장 사용가능\n -일부 매장 제외(휴게소, 특설매장, 미판매점 교환 불가/홈서비스 제외)



★유의사항\n-유효기간 이내 사용\n-교환시 할인 및 무료증정 등 매장행사 적용 불가.\n-교환 시 제휴 통신사 및 카드사 할인, 멤버십 적립이 적용 불가.\n-본 교환권은 타 쿠폰(모바일 쿠폰, 제품교환권 등)과 중복 사용이 불가.\n-매장 별 재고 상황에 따라 동일 금액의 타상품으로 교환이 가능.\n-해당 금액보다 높을 경우 초과금액을 결제하시면 교환 가능. (단, 해당 금액보다 낮을 경우 잔액 환불이 되지 않습니다.)\n-세트 상품인 경우, 디저트 및 드링크의 변경이 가능하나 추가 금액이 발생할 수 있습니다.\n-해당상품 가격인상시, 차액에 대해 추가결제시 교환 가능합니다.



제조사/ 브랜드: 롯데리아

\"Smiley"
      ],
      "n:AfterServiceTelephoneNumber": [
         "02-597-0660"
      ],
      "n:AfterServiceGuideContent": [
         "상품 상세 참조"
      ],
      "n:PurchaseReviewExposure": [
         "Y"
      ],
      "n:RegularCustomerExclusiveProduct": [
         "N"
      ],
      "n:KnowledgeShoppingProductRegistration": [
         "Y"
      ],
      "n:SalePrice": [
         "15700"
      ],
      "n:StockQuantity": [
         "99999"
      ],
      "n:MaxPurchaseQuantityPerId": [
         "99999"
      ],
      "n:MaxPurchaseQuantityPerOrder": [
         "3"
      ],
      "n:ECoupon": [
         {
            "n:PeriodType": [
               "FB"
            ],
            "n:PeriodDays": [
               "93"
            ],
            "n:PublicInformationContents": [
               "(주)플랫포스"
            ],
            "n:ContactInformationContents": [
               "02-597-0660"
            ],
            "n:UsePlaceType": [
               "PLACE"
            ],
            "n:UsePlaceContents": [
               "전매장가능(일부매장제외)"
            ],
            "n:RestrictCart": [
               "N"
            ]
         }
      ],
      "n:ProductSummary": [
         {
            "n:MobileCoupon": [
               {
                  "n:NoRefundReason": [
                     "상품 상세 참조"
                  ],
                  "n:ReturnCostReason": [
                     "상품 상세 참조"
                  ],
                  "n:QualityAssuranceStandard": [
                     "상품 상세 참조"
                  ],
                  "n:CompensationProcedure": [
                     "상품 상세 참조"
                  ],
                  "n:TroubleShootingContents": [
                     "소비자분쟁해결기준(공정거래위원회 고시) 및 관계법령에 따릅니다."
                  ],
                  "n:Issuer": [
                     "(주)플랫포스"
                  ],
                  "n:UsableCondition": [
                     "90일"
                  ],
                  "n:UsableStore": [
                     "전매장가능(일부매장제외)"
                  ],
                  "n:CancelationPolicy": [
                     "상품 상세 참조"
                  ],
                  "n:CustomerServicePhoneNumber": [
                     "02-597-0660"
                  ]
               }
            ]
         }
      ],
      "n:SellerCommentUsable": [
         "N"
      ],
      "n:SellerCustomCode1": [
         "506"
      ]
   }.....]
}
```

### 3. post
#### 스토어팜 상품 등록

> * 어드민에서의 상품 정보 스토어팜  등록시 해당 상품을 스토어팜에 등록

> * findProduct(): 상품 정보 획득

> * setImageParam(): 상품 이미지 스토어팜에 등록 XML 생성

> * requestImageUpload(): 스토어팜에 이미지 등록

> * setParam(): 상품 스토어팜에 등록 XML 생성 - ManageProductRequest

> * requestCreateProduct(): 상품 스토어팜에 등록 요청 

> * findCategoryId(): 상품 카테고리 검색

> * createThirdParty(): 스토어팜에서 리턴한 데이터 AppThirdPartyProduct에 저장


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | Int |  false | 상품 번호 |
| categoryId | Int |  false | 카테고리 Id | 
| status | String |  false | 상품 상태(판매/전시/삭제) | 
| storeFarmProductOrderMax | Int |  false | 상품 1회 최대 구매  |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

| parameter | desc |
| ---------- | :--------- |
| productId | 상품 번호 |
| thirdPartyId | 스토어팜 상품 번호 |
| categoryId | 스토어팜 상품 카테고리  | 
| status | 상품 상태 | 
| storeFarmProductOrderMax | 상품 1회 최대 구매  |
| image | 스토어팜  이미지 경로  |
| name | 상품명  |

 <br /><br /><br /><br />

### 4. put
#### 스토어팜 상품 수정

> * 스토어팜의 등록된 상품 정보 수정

> * findProduct(): 상품 정보 획득

> * setImageParam(): 상품 이미지 스토어팜에 등록 XML 생성

> * requestImageUpload(): 스토어팜에 이미지 등록

> * setParam(): 상품 스토어팜에 등록 XML 생성 - ManageProductRequest

> * requestCreateProduct(): 상품 스토어팜에 등록 요청 

> * findCategoryId(): 상품 카테고리 검색

> * createThirdParty(): 스토어팜에서 리턴한 데이터 AppThirdPartyProduct에 저장

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| categoryId | Int |  false | 카테고리 ID | 
| status | String |  false | 상태 | 
| storeFarmProductOrderMax | Int |  false | 상품 1회 최대 구매  |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

| parameter | desc |
| ---------- | :--------- |
| productId | 상품 번호 |
| thirdPartyId | 스토어팜 상품 번호 |
| categoryId | 스토어팜 상품 카테고리  | 
| status | 상품 상태 | 
| storeFarmProductOrderMax | 상품 1회 최대 구매  |
| image | 스토어팜  이미지 경로  |
| name | 상품명  |

 <br /><br /><br /><br />

### 5. delete
 #### 스토어팜 상품 삭제

> * 스토어팜에 등록된 상품 삭제

> * findProduct(): AppThirdPartyProduct에서 상품 검색

> * setParam(): 상품 삭제 요청 XML 작성 - DeleteProductRequest

> * requestProductDelete(): 스토어팜 상품 삭제 요청 

> * deleteThirdParty(): AppThirdPartyProduct에서 상품 삭제


#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 <br /><br /><br /><br />

