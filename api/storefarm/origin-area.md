# origin-area

## /api/storefarm/origin-area

### 1. get
#### 스토어팜 상품 등록 지역 코드 가져오기 - 사용하지 않음

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
 

 #### Return

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
