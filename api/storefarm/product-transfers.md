# product-transfers

## /api/storefarm/product-transfers

### 1. post
#### 스토어팜 수신자 환불 

> * 어드민에서 환불 요청시 쿠폰 취소 및 정산 되돌림.
> * findAllProductTransfer() 해당 주문건 검색
> * smartInfiniCancelRequest() 스마트인피니 쿠폰 취소
> * omnitelPartCancelRequest() 옴니텔 쿠폰 취소 
> * initSupplyAdjustment() 공급사 정산 초기화
> * initDistributionAdjustment() 유통사 정산 초기화
> * updateProductTransfer() 정산 진행
> * storeFarmRefund() /storefarm/approve-return-application 호출 (스토어팜 환불 완료 처리 )
> * refindAllProductTransfer() 환불 완료된 주문건 리턴


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseId | int |  false | 구매 id |
| destination | string |  false | 수신자 번호 |
| state | enum |  false | 상태 disuse, use, partial, cancel, refundStart, refundFinish |
| refundRate | int |  false | 환불률 |
| refundPrice | int |  false | 환불금액 |
| refundBank | string |  false | 환불계좌은행 |
| refundDepositor | string |  false | 환불계좌예금주 |
| refundAccount | string |  false | 환불계좌번호 |
| refundStartedAt | date |  false | 환불승인일자 |
| refundFinishedAt | date |  false | 환불완료일자 |
 

 #### Return

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 <br /><br /><br /><br />

