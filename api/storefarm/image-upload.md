# image-upload

## /api/storefarm/image-upload

### 1. post
#### 스토어팜 상품 이미지 업로드

> * productId로 받은 상품의 이미지를 스토어팜 서버로 전송

> * 스토어팜 상품 등록시 해당 이미지는 링크 형식이 아닌 스토어팜 서버에 저장된 이미지만 사용되어 해당 API사용

> * 폰기프트 어드민에서 상품을 수정/등록시 마지막에 동작

> * AppThirdPartyProduct에 올린 링크를 

 <br /><br />
#### 요청 파라미터 

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | Int |  false | 상품 Id | 
 

 <br /><br />
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 <br /><br />
#### 응답 예제 
