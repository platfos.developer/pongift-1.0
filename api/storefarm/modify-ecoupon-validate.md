# modify-ecoupon-validate

## /api/storefarm/modify-ecoupon-validate

### 1. post
#### 선물 쿠폰 발송 배송완료 api

> * 스토어팜의 선물 배송 완료시 호출 API / 해당 API를 호출해야 선물하기는 완료처리 된다.


<br /><br />
#### 요청 파라미터 

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | Int |  false | 스토어팜에서 제공한 productOrderId |
| orderId | Int |  false | 스토어팜에서 제공한 orderId - (폐기 예정) |
| externalCode | String |  false | 옴니텔에서 제공한 쿠폰 번호 |
 

<br /><br />
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
