# purchase

## /api/storefarm/purchase

### 1. gets
#### 일반 주문 확인 상세 api

> * 스토어팜의 주문 내역 확인 API

> * series(): startStoreFarmOrderInfoId / endStoreFarmOrderInfoId 입력한 아이디사이의 모든 주문건 조회 

> * setParam(): 해당 주문건에 대한 상세 조회 요청 XML작성 - GetProductOrderInfoList

> * requestProductOrderList(): 상세 조회 요청 

> * findPurchase(): 해당 주문의 주문 내역 검색

> * updatePurchase(): 해당 주문의 신규 내용으로 업데이트 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| startStoreFarmOrderInfoId | Int  |  false |  StoreFarmOrderInfo Id | 
| endStoreFarmOrderInfoId | Int |  false | StoreFarmOrderInfo Id | 
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 <br /><br /><br /><br />

### 2. post
#### 폰기프트 결제 완료 요청 및 옴니텔 요청

> * setParam(): 주문에 대한 주문 객체 생성

> * getCurrentMeta(): 메타 검색

> * findNonmember(): 주문자에 대한 비회원 정보 검색 / 없을시 새로 생성

> * findProduct(): 주문 상품 검색

> * requestPurchase(): /api/pongift/purchases에 주문 생성 요청 

> * requestPruchaseSuccess(): /api/pongift/purchase-success에 주문 결제 완료 요청

> * requestKCPSend(): 주문한 상품의 templateImageKey가 있을시  /api/sender/storeFarm-mms에 쿠폰 요청 
                      없을시엔 /api/omnitel/kcp-send에 쿠폰 요청

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | Int |  false | 주문 상품 Id | 
| tno | String |  false | 주문 고유 번호 | 
| phoneNum | String |  false | 수신자 전화번호 | 
| name | String |  false | 주문자 이름 | 
| userId | Int |  false | 주문자 Id | 
| quantity | Int |  false | 상품 요청 갯수 | 
| orderType | String |  false | 스토어팜 주문 타입 일반/선물하기  |
| ordererTel1 | String |  false | 스토어팜 발신자의 전화번호 |
| type | String |  false | 결제 타입 |
| message | String |  false | 결제시 요청 메시지  |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

