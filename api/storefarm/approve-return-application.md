# approve-return-application

## /api/storefarm/approve-return-application

### 1. post
#### 스토어팜 반품 확인 요청 

> * 권한 (roleUltraAdmin)

> * 환불요청시 put /pongift/product-transfers을 실행시 내부 호출 

> * 스토어팜측에 해당 주문 환불 완료로 처리 요청

 <br /><br />
#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | int |  false | 스토어팜 상품주문Id | 
 
 <br /><br />
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

