# chatbot

## /api/storefarm/chatbot

### 1. get
 #### 챗봇

> * 

> * 

> * 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
 

 #### Return

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

### 2. post
 #### 스토어팜 상품 등록

> * 

> * 

> * 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | |  false | 상품 번호 |
| categoryId | |  false |  | 
| status | |  false |  | 
| storeFarmProductOrderMax | |  false | 상품 1회 최대 구매  |
 

 #### Return

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

