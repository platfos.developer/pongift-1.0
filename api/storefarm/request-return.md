# request-return

## /api/storefarm/request-return

### 1. post
#### 환불 요청 api

> * 스토어팜의 환불 완료 처리 API 

> * setRequestReturn() : 환불 완료 요청 XML 작성 - RequestReturnRequest

> * sendRequestReturn():  스토어팜 환불 요청

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | Int |  false | 스토어팜에서 제공한 productOrderId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 <br /><br /><br /><br />

