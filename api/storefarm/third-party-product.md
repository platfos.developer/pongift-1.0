# third-party-product

## /api/storefarm/third-party-product

### 1. get
#### 스토어팜 상품 조회 

> * 스토어팜에 기존 상품 등록후 조회 API

> * setParam(): 요청한 상품 id를 통해 AppThirdPartyProduct의 테이블에 등록된 상품 정보 검색 및 리턴 


#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

<br/><br/>
#### 응답 예제 
```
{
   "data": [
      {
         "productId": 52,
         "thirdPartyId": "2000246348",
         "name": "그린더마 마일드 로션",
         "type": "storeFarm",
         "status": "use",
         "categoryId": 93,
         "image": "http://beta.shop1.phinf.naver.net/20170918_299/qa2tc354_1505701704121i6AIn_JPEG/upload_1f7c2db5abc1bd28d8164ea58589c53a.jpg",
         "salePrice": 15000,
         "productOrderMax": 3,
         "isUse": true,
         "createdAt": 1505701696509591,
         "updatedAt": 1505701696509649,
         "id": 65
      }
   ]
}
```

