# product-order-detail

## /api/storefarm/product-order-detail

### 1. gets
 #### 일반 주문 확인 상세 api

> * productOrderId로 해당 주문건의 상세 내역을 요청

> * setParam() 요청 XML 작성

> * requestProductOrderList() 스토어팜에 상세 내역 요청

> * createStoreFarmOrderList() 주문 내역 업데이트


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | Int |  false |  | 
 

 #### Return
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 <br /><br /><br /><br />

### 2. post
 #### 일반 주문 확인 상세 api

> * 완료되지 않은 주문건에 대하여 상세 내역 요청 및 발송

> * getStoreFarmOrderProduct() 완료되지 않은 주문건 조회
> * setParam() 요청 XML 작성
> * requestProductOrderList() 스토어팜에 상세 내역 요청
> * updateStoreFarmOrderList() 상세내역 업데이트
> * findThirdPartyProduct() 스토어팜 상품 확인
> * findProduct() 상품 확인
> * uniqueCheckPurchaseTno() tno(productOrderId) 고유 값 확인
> * reqPurchase() /api/storefarm/purchase 요청
> * findProductTransferSuccess() 발송 완료 처리 
> * setShipOrderParam() 배송 완료 XML작성
> * requestProductOrderConfirm() 완료 요청


> * createStoreFarmOrderList() 주문 내역 업데이트

#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음
 

 #### Return
 ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 <br /><br /><br /><br />

