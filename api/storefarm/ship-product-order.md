# ship-product-order

## /api/storefarm/ship-product-order

### 1. post
#### 선물 쿠폰 발송 에러시 배송완료 api

> * setShipOrderParam(): 상품 배송완료 XML 작성 - ShipProductOrderRequest

> * requestProductOrderConfirm(): 스토어팜에 배송완료 요청 후 배송완료로 업데이트

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productOrderId | |  false | 스토어팜에서 제공한 productOrderId |
| externalCode | |  false | 옴니텔에서제공한 쿠폰 번호 |
| storefarmOrderId | |  false | DB상의 스토어팜 결제 ID (appStoreFarmOrderInfos id)  |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
 <br /><br /><br /><br />

