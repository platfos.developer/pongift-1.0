# purchase-cancel

## /api/storefarm/purchase-cancel

### 1. post
#### 구매 취소 - 사용하지 않음


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| tno | |  false | 주문 승인 번호 |
| phoneNum | |  false | 비회원 전화번호 |
 

 #### Return

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

