# resend-omnitel

## /api/storefarm/resend-omnitel

### 1. post
#### 옴니텔 상품 재전송

> * 스토어팜의 상품 전송시 에러가 발생하여 상품이 전송이 되지 않을시 어드민에서 재발송을 통해 해당 상품의 쿠폰 전송 기능

> * findPurchase(): 해당 주문 건 조회

> * findStoreFramOrder(): 스토어팜 주문건 조회

> * requestOmnitel(): /omnitel/part-send로 재발송 요청 - 요청시 tno로 옴니텔에 같은 번호로 요청이 불가능하여 뒤에 랜덤으로 숫자를 늘려준다.

> * setShipOrderParam(): 스토어팜 상품 배송 완료 XML 작성

> * requestShipOrder(): 스토어팜 상품 배송 완료 요청


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseId | Int |  false | purchase Id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 <br /><br /><br /><br />

