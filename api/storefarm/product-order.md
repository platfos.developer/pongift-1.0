# product-order

## /api/storefarm/product-order

### 1. gets
#### 주문 확인 api

> * 일정 기간의 주문 내역건을 요청

> * setParam() 요청 XML 작성

> * requestProductOrderList() 스토어팜 요청

> * createStoreFarmOrderList() 해당 주문건을 StorefarmOrderInfo에 저장

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| startDate | Date |  false | 2017-05-10T00:00:00 |
| endDate | Date |  false | 2017-05-10T00:00:00 |
 

 #### Return

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음
 <br /><br /><br /><br />

### 2. post
 #### 주문 확인 api

> * 10분 동안의 주문 내역건을 요청

> * setParam() 요청 XML 작성

> * requestProductOrderList() 스토어팜 요청

> * createStoreFarmOrderList() 해당 주문건을 StorefarmOrderInfo에 저장

#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

#### Return
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 <br /><br /><br /><br />

### 3. put
#### 주문 확인 api - 테스트 API

> * 작성된 XML로 요청

#### 파라미터
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

#### Return
![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음
 <br /><br /><br /><br />

### 4. delete
#### 주문 확인 api - 사용하지 않음

#### 파라미터

#### Return
<br /><br /><br /><br />

