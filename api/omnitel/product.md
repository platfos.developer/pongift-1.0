# product

## /api/omnitel/product

### 1. get
#### 옴니텔에서 사용가능한 상품 리스트 리턴

> * request(): 요청받은 파라미터로 옴니텔에 상품 리스트 조회 - PRODUCTLIST


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| POC_ID | String |  false |  폰기프트 발급 고유 번호 | 
 


#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

<br/><br/>
#### 응답 예제 
```
{
   "PRODUCTLIST": {
      "RESULT_CODE": [
         "00"
      ],
      "RESULT_MSG": [
         "처리 성공"
      ],
      "LIST_CNT": [
         "1"
      ],
      "PRODUCT_INFO": [
         {
            "CAT_ID": [
               "CAT0000621"
            ],
            "GOODS_ID": [
               "0000004618"
            ],
            "GOODS_NAME": [
               "농심)새우깡_MCP"
            ],
            "GOODS_ORI_PRICE": [
               "1000"
            ],
            "GOODS_PRICE": [
               "1000"
            ],
            "GOODS_INFO": [
               "한걸음 앞서나가는 GS25의 다양한 서비스 상품으로 더욱 편리한 생활을 누려보세요.\r\n전국 GS25편의점에서 유효기간 이내에 본 쿠폰을 제시하시면 실물상품과 교환 가능합니다."
            ],
            "USE_GUIDE": [
               "[교환안내]
\r\n① 선물하실 상품을 선택하고, [선물하기] 혹은 [구매하기] 버튼을 클릭합니다.
\r\n② 선물 받으실 분의 폰번호, 상품수량을 입력 후 결제를 완료합니다.
\r\n③ 선물 받을 친구에게 선물메시지(MMS)가 전송됩니다. (구매하기의 경우 '받은 쿠폰함'에서 확인)
\r\n④ 표시된 교환처에서 선물메시지를 제시하시면, 실물상품으로 교환해 드립니다.
\r\n* 선물 받은 쿠폰의 교환 유효기간은 상품마다 상이하니 상품 상세 정보를 확인하세요.
\r\n* 유효기간이 경과한 모바일쿠폰에 대한 환불은 이용약관을 참조하시길 바랍니다.
"
            ],
            "EXC_BRANCH": [
               "GS25"
            ],
            "VALID_END_TYPE": [
               "D"
            ],
            "VALID_END_DATE": [
               "60"
            ],
            "SEND_TYPE": [
               "N"
            ],
            "IMAGE_PATH_S": [
               ""
            ],
            "IMAGE_PATH_M": [
               ""
            ],
            "IMAGE_PATH_B": [
               ""
            ],
            "IMAGE_SIZE_S_W": [
               "-1"
            ],
            "IMAGE_SIZE_S_H": [
               "-1"
            ],
            "IMAGE_SIZE_M_W": [
               "-1"
            ],
            "IMAGE_SIZE_M_H": [
               "-1"
            ],
            "IMAGE_SIZE_B_W": [
               "-1"
            ],
            "IMAGE_SIZE_B_H": [
               "-1"
            ]
         }
      ]
   }
}
```

 <br /><br /><br /><br />

