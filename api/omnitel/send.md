# send

## /api/omnitel/send

### 1. get
#### 옴니텔에 요청한 주문상태 조회 API

> * request(): 옴니텔 쿠폰 발송 요청 API - coufunSend.do

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| POC_ID | String |  false | 고객사 ID (옴니텔 제공) |
| GOODS_ID | String |  false | 판매상품 ID (옴니텔 제공) |
| SEND_PHONE | String |  false | 발신자 번호 |
| RECEIVER_MOBILE | String |  false | 수신자 번호 (여러건인 경우 콤마구분) |
| SEND_TITLE | String |  false | 문자 전송시 제목 |
| SEND_CONTENT | String |  false | 문자 전송시 내용 |
| TR_ID | String |  false | 업체측 고유 ID (고유 식별키로 사용) |
| RESERVE1 | String |  false | 추가 필드 1 |
| RESERVE2 | String |  false | 추가 필드 2 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

<br/><br/>
#### 응답 예제 

```
{ COUFUNSEND: 
   { RESULT_CODE: [ '00' ],
     RESULT_MSG: [ '처리 성공' ],
     ORDER_ID: [ '201712010000000057113110110546' ],
     CAT_ID: [ 'CAT0001019' ],
     GOODS_ID: [ '0000056066' ],
     SEND_PHONE: [ '01068278005' ],
     TR_ID: [ '1512093946246278' ],
     ORDER_CNT: [ '1' ],
     VALID_END_DATE: [ '20180304' ],
     SEND_TYPE: [ 'MMS' ],
     SEND_TITLE: [ '완전딸기바나나 빽스치노 (베..' ],
     SEND_CONTENT: [ '' ],
     RESERVE1: [ '' ],
     RESERVE2: [ '' ],
     ORDER_INFO: [ [Object] ] } }
```

 <br /><br /><br /><br />

