# coupon-create

## /api/omnitel/coupon-create

### 1. post
#### kcp 서버로부터 옴니텔 상품 생성

> * findPurchaseByPurchaseCode(): PurchaseCode로 주문 내역 조회

> * omnitelSendRequest(): 옴니텔에 해당 상품의 쿠폰 생성 요청 - COUFUNCREATE

> * updatePurchase(): 발급받은 쿠폰 번호와 유효기간 주문에 업데이트

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseCode | String |  false | 주문 신청 번호 |
| purchaseAuthorizationCode |String  |  false | 카드결제시 주문 승인 번호 / 모바일 결제시 전화번호 / 계좌이체시 현금영수증 인증 번호 |
| tno | String |  false | transaction number: 실질적인 승인번호 |
| cash_no | String |  false | 계좌이체시 현금영수증 요청 번호 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200


 <br /><br /><br /><br />

