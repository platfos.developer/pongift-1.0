# part-send

## /api/omnitel/part-send

### 1. post
#### 옴니텔 에러가 난 미발송건 재발송


> * findPurchaseByPurchaseCode(): PurchaseCode로 해당 주문건 조회

> * omnitelSendRequest(): 옴니텔에 해당 주문건 쿠폰 발송 요청 (재발송시 같은 tno로 요청시 에러가 난다.: 고유번호이므로 : 뒤에 랜덤값을 붙여서 전송)

> * updatePurchase(): 발급 받은 쿠폰 번호와 유효기간 업데이트 


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseCode | String |  false | 주문 신청 번호 |
| tno | String |  false | transaction number: 실질적인 승인번호 |
 


#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

