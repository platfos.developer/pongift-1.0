# part-status

## /api/omnitel/part-status

### 1. get
#### 옴니텔에서 받은 쿠폰정보를 통해 해당 쿠폰의 정보를 요청하는 API

> * request(): 요청받은 파라미터로 옴니텔에 해당 쿠폰의 상태 조회 - coufunPartStatus.do

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| POC_ID | String |  false |  폰기프트 발급 번호 | 
| GOODS_ID | String |  false |  상품 발급 번호  | 
| BARCODE_NUM | String |  false |  옴니텔 발급 핀번호 | 
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

<br/><br/>
#### 응답 예제 
```
<?xml version="1.0" encoding="euc-kr"?>
<COUFUNCANCEL>
	<RESULT_CODE>02</RESULT_CODE>
	<RESULT_MSG><![CDATA[POC ID ¿À·ù]]></RESULT_MSG>
</COUFUNCANCEL>

 <br /><br /><br /><br />

