## 옴니텔

### 1. [쿠폰 취소 (주문단위)](/markdown/api/omnitel/all-cancel.md)
### 2. [쿠폰 상태 조회 (주문단위)](/markdown/api/omnitel/all-status.md)
### 3. [쿠폰 발급](/markdown/api/omnitel/coupon-create.md)
### 4. [쿠폰 발급 및 전송](/markdown/api/omnitel/kcp-send.md)
### 5. [쿠폰 취소 (쿠폰단위)](/markdown/api/omnitel/part-cancel.md)
### 6. [쿠폰 발급 및 전송](/markdown/api/omnitel/part-send.md)
### 7. [쿠폰 상태 조회 (쿠폰단위)](/markdown/api/omnitel/part-status.md)
### 8. [상품 리스트 조회](/markdown/api/omnitel/product.md)
### 9. [재발송 요청](/markdown/api/omnitel/resend.md)
### 10. [쿠폰 발급 및 전송](/markdown/api/omnitel/send.md)
