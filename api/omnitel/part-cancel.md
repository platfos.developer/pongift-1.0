# part-cancel

## /api/omnitel/part-cancel

### 1. get
#### 옴니텔에서 발급한 쿠폰 번호 취소

> * request(): 옴니텔에 파라미터로 받은 쿠폰 번호의 취소를 요청 - coufunPartCancel.do

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| POC_ID | String |  false |  폰기프트 발급 번호 | 
| GOODS_ID | String |  false |  상품 발급 번호  | 
| ORDER_ID | String |  false |  주문 번호 | 
 

#### 응답 예제 (XML)
```
<?xml version="1.0" encoding="euc-kr"?>
<COUFUNCANCEL>
	<RESULT_CODE>02</RESULT_CODE>
	<RESULT_MSG><![CDATA[POC ID ¿À·ù]]></RESULT_MSG>
</COUFUNCANCEL>
```

 <br /><br /><br /><br />

