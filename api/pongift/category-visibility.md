# category-visibility

## /api/pongift/category-visibility

### 1. put

#### 카테고리 노출여부 수정

> * 권한 (roleUltraAdmin)

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| visibility | boolean | true | 카테고리 노출여부 |
 
 <br /><br /><br /><br />

