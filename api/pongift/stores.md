# stores

## /api/pongift/stores

### 1. get
 #### 단일 얻기

> * get.validate() : 유효성검사

> * get.setParam() : 조건에 맞는 AppStore테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | ture | AppStoreId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppStore | AppStores객체 |
 | AppCorporation | AppCorporations객체 |
 | AppStoreImage | AppStoreImages객체 |
 | AppServiceStore | AppServiceStores객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "corporationId": 21,
   "name": "ㅂㅈㄷㅂㄷㅈㅂㅈㄷ",
   "address": "대한민국 서울특별시 중구 플랫포스",
   "location": {
      "type": "Point",
      "coordinates": [
         37.560363,
         126.993467
      ]
   },
   "phoneNum": "123123123123",
   "info": "ㅁㄴㅇㄹ",
   "createdAt": 1492505789282218,
   "updatedAt": 1496036635869349,
   "deletedAt": null,
   "id": 17,
   "corporation": {
      "authorId": 20,
      "corporationId": null,
      "type": "corporation",
      "number": "123-45-67899",
      "name": "플랫포스",
      "address": "서울 중구 퇴계로36길 2 201호 동국대학교 충무로관 신관",
      "leader": "폰기프트",
      "condition": "O2O",
      "category": "기프트콘",
      "logoImgId": 585,
      "state": "contract",
      "contractedAt": null,
      "canceledAt": null,
      "country": "kr",
      "createdAt": 1488959838936056,
      "updatedAt": 1489019496830264,
      "deletedAt": null,
      "id": 21,
      "logoImg": {
         "authorId": 20,
         "folder": "corporation",
         "dateFolder": "2017-03-08",
         "name": "upload_e432d060d76748f496fcda6fd1d46de2.png",
         "authorized": true,
         "createdAt": 1488959764215076,
         "updatedAt": 1488959764215124,
         "deletedAt": null,
         "id": 585
      }
   },
   "serviceStores": [],
   "storeImages": [
      {
         "storeId": 17,
         "imageId": 683,
         "index": 1,
         "createdAt": 1496036635860996,
         "updatedAt": 1496036635861041,
         "id": 41,
         "image": {
            "authorId": 20,
            "folder": "store",
            "dateFolder": "2017-05-29",
            "name": "upload_3a8f4f69ce6550e86f17a8a85278e77d.PNG",
            "authorized": true,
            "createdAt": 1496036632823313,
            "updatedAt": 1496036632823359,
            "deletedAt": null,
            "id": 683
         }
      }
   ]
}
```

### 2. gets
 #### 매장 조회

> * 

> * 

> * 

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id, name, phoneNum, address |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microTimestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| corporationId | int |  false | 법인 id |
| corporationSearchItem | string |  false | 법인 명 검색 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppStore | AppStores객체 |
 | AppCorporation | AppCorporations객체 |
 | AppStoreImage | AppStoreImages객체 |
 | AppServiceStore | AppServiceStores객체 |


 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "corporationId": 21,
   "name": "ㅂㅈㄷㅂㄷㅈㅂㅈㄷ",
   "address": "대한민국 서울특별시 중구 플랫포스",
   "location": {
      "type": "Point",
      "coordinates": [
         37.560363,
         126.993467
      ]
   },
   "phoneNum": "123123123123",
   "info": "ㅁㄴㅇㄹ",
   "createdAt": 1492505789282218,
   "updatedAt": 1496036635869349,
   "deletedAt": null,
   "id": 17,
   "corporation": {
      "authorId": 20,
      "corporationId": null,
      "type": "corporation",
      "number": "123-45-67899",
      "name": "플랫포스",
      "address": "서울 중구 퇴계로36길 2 201호 동국대학교 충무로관 신관",
      "leader": "폰기프트",
      "condition": "O2O",
      "category": "기프트콘",
      "logoImgId": 585,
      "state": "contract",
      "contractedAt": null,
      "canceledAt": null,
      "country": "kr",
      "createdAt": 1488959838936056,
      "updatedAt": 1489019496830264,
      "deletedAt": null,
      "id": 21,
      "logoImg": {
         "authorId": 20,
         "folder": "corporation",
         "dateFolder": "2017-03-08",
         "name": "upload_e432d060d76748f496fcda6fd1d46de2.png",
         "authorized": true,
         "createdAt": 1488959764215076,
         "updatedAt": 1488959764215124,
         "deletedAt": null,
         "id": 585
      }
   },
   "serviceStores": [],
   "storeImages": [
      {
         "storeId": 17,
         "imageId": 683,
         "index": 1,
         "createdAt": 1496036635860996,
         "updatedAt": 1496036635861041,
         "id": 41,
         "image": {
            "authorId": 20,
            "folder": "store",
            "dateFolder": "2017-05-29",
            "name": "upload_3a8f4f69ce6550e86f17a8a85278e77d.PNG",
            "authorized": true,
            "createdAt": 1496036632823313,
            "updatedAt": 1496036632823359,
            "deletedAt": null,
            "id": 683
         }
      }
   ]
}

```

### 3. post
 #### 매장 생성

> * post.validate() : 유효성검사

> * post.setParam() : AppStore테이블 생성

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int |  false | 법인 id |
| name | string |  false | 매장명 |
| address | string |  false | 매장 주소 |
| phoneNum | string |  false | 매장 연락처 |
| info | string |  false | 매장 정보 |
| storeImageIds | id |  false | 매장 이미지 ids |
| serviceIds | id |  false | 서비스 ids |
| lng | int |  false | lng |
| lat | int |  false | lat |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppStore | AppStores객체 |
 | storeImage | storeImages객체 |
 | serviceStores | serviceStores객체 |
 | corporation | corporation객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "corporationId": 3,
   "name": "매장명",
   "address": "매장주소",
   "location": null,
   "phoneNum": "01",
   "info": "정보",
   "createdAt": 1512699855870276,
   "updatedAt": 1512699855870344,
   "deletedAt": null,
   "id": 18,
   "storeImages": [],
   "serviceStores": [],
   "corporation": {
      "authorId": 8,
      "corporationId": null,
      "type": "corporation",
      "number": "215-86-65050",
      "name": "(주)탐앤탐스",
      "address": "서울특별시 강남구 논현로 163길 10 베드로빌딩 2F",
      "leader": "김도균",
      "condition": "음식",
      "category": "커피, 다류",
      "logoImgId": 31,
      "state": "contract",
      "contractedAt": null,
      "canceledAt": null,
      "country": "kr",
      "createdAt": 1476255642759516,
      "updatedAt": 1480398514074323,
      "deletedAt": null,
      "id": 3
   }
}

```

### 4. put
 #### 매장 수정

> * put.validate() : 유효성검사

> * put.hasAuthorization () : 권한조회

> * put.findDeleteImages() : Image테이블 로우조회

> * put.updateStore() : AppStore테이블 로우 업데이트

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int |  false | 법인 id |
| name | string |  false | 매장명 |
| address | string |  false | 매장 주소 |
| phoneNum | string |  false | 매장 연락처 |
| info | string |  false | 매장 정보 |
| storeImageIds | int |  false | 매장 이미지 ids |
| deleteImageIds | int |  false | 삭제할 이미지 ids |
| serviceIds | int |  false | 서비스 ids |
| deleteServiceIds | int |  false | 매장 등록 해지할 서비스 ids |
| lng | int |  false | lng |
| lat | int |  false | lat |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppStore | AppStores객체 |
 | storeImage | storeImages객체 |
 | serviceStores | serviceStores객체 |
 | corporation | corporation객체 |
 
 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "corporationId": 3,
   "name": "매장명",
   "address": "매장주소",
   "location": null,
   "phoneNum": "01",
   "info": "정보",
   "createdAt": 1512699855870276,
   "updatedAt": 1512700004547794,
   "deletedAt": null,
   "id": 18,
   "storeImages": [],
   "serviceStores": [],
   "corporation": {
      "authorId": 8,
      "corporationId": null,
      "type": "corporation",
      "number": "215-86-65050",
      "name": "(주)탐앤탐스",
      "address": "서울특별시 강남구 논현로 163길 10 베드로빌딩 2F",
      "leader": "김도균",
      "condition": "음식",
      "category": "커피, 다류",
      "logoImgId": 31,
      "state": "contract",
      "contractedAt": null,
      "canceledAt": null,
      "country": "kr",
      "createdAt": 1476255642759516,
      "updatedAt": 1480398514074323,
      "deletedAt": null,
      "id": 3
   }
}

```

### 5. delete
 #### 매장 제거

> * del.validate() : 유효성검사

> * del.hasAuthorization() : 권한조회

> * del.findStoreImages() : AppStoreImage테이블 조회

> * del.destroy() : 조건에 맞는 AppStore테이블 로우삭제


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppStoreId |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 

