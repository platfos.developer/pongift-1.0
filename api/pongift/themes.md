# themes

## /api/pongift/themes

### 1. get
 #### 

> * get.validate() : 유효성검사

> * get.setParam() : 조건에 맞는 AppTheme테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppThemes객체 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppTheme | AppThemes객체 |
 | iconImg | Images객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
   "authorId": 1,
   "iconImgId": 58,
   "type": "personal",
   "name": "생일",
   "visibility": true,
   "createdAt": 1476350209314441,
   "updatedAt": 1489467157743014,
   "deletedAt": null,
   "id": 1,
   "iconImg": {
      "authorId": 8,
      "folder": "theme",
      "dateFolder": "2016-12-13",
      "name": "upload_383178ab4a733061b3b2e0f1d2536481.png",
      "authorized": true,
      "createdAt": 1476350249132767,
      "updatedAt": 1476350249132808,
      "deletedAt": null,
      "id": 58
   }
}

```

### 2. gets
 #### 테마 조회

> * gets.validate() : 유효성검사

> * gets.setParam() : 조건에 맞는 AppTheme테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id, name |
| orderBy | enum |  false | 정렬 기준 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microTimestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| type | enum |  false | 테마 유형 personal, public |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "count": 2,
   "list": [
      {
         "authorId": 91,
         "iconImgId": 714,
         "type": "public",
         "name": "테스트 테마",
         "visibility": false,
         "createdAt": 1496659003305716,
         "updatedAt": 1510124313883022,
         "deletedAt": null,
         "id": 56,
         "iconImg": {
            "authorId": 91,
            "folder": "theme",
            "dateFolder": "2017-06-05",
            "name": "upload_27c9681a27fb0bdfdfae805bc414dd45.png",
            "authorized": true,
            "createdAt": 1496659022027235,
            "updatedAt": 1496659022027292,
            "deletedAt": null,
            "id": 714
         }
      },
      {
         "authorId": 1,
         "iconImgId": 293,
         "type": "public",
         "name": "기념일",
         "visibility": false,
         "createdAt": 1479224920738374,
         "updatedAt": 1489478242437640,
         "deletedAt": null,
         "id": 20,
         "iconImg": {
            "authorId": 8,
            "folder": "theme",
            "dateFolder": "2016-12-13",
            "name": "upload_9bf203b4caf2cc5573133adecb00250b.jpeg",
            "authorized": true,
            "createdAt": 1479224913777449,
            "updatedAt": 1480646056534157,
            "deletedAt": null,
            "id": 293
         }
      }
   ]
}

```

### 3. post
 #### 테마 생성

> * post.validate() : 유효성검사

> * post.setParam() : AppTheme테이블 로우생성

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| name | string |  false | 테마명 |
| iconImgId | int |  false | 아이콘 이미지 id |
| type | enum |  false | 테마 유형 personal, public |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "iconImgId": null,
   "type": "public",
   "name": "xptmxm1",
   "visibility": false,
   "createdAt": 1512698450400007,
   "updatedAt": 1512698450400075,
   "deletedAt": null,
   "id": 57,
   "iconImg": null
}
```

### 4. put
 #### 테마 수정

> * put.validate() : 유효성검사

> * put.findDeleteImage() : 수정할 AppTheme테이블 로우조회

> * put.updateTheme() : AppTheme테이블 로우수정

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| name | |  false | 테마명 |
| iconImgId | |  false | 아이콘 이미지 id |
| type | |  false | 테마 유형 |
| visibility | |  false | 메인 표시 유무 |
| productIds | |  false | 테마 적용시킬 상품 ids |
| deleteProductIds | |  false | 테마 해제시킬 상품 ids |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppTheme | AppThemes객체 |
 | iconImg | Images객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "iconImgId": 293,
   "type": "public",
   "name": "수정",
   "visibility": false,
   "createdAt": 1479224920738374,
   "updatedAt": 1512698640572982,
   "deletedAt": null,
   "id": 20,
   "iconImg": {
      "authorId": 8,
      "folder": "theme",
      "dateFolder": "2016-12-13",
      "name": "upload_9bf203b4caf2cc5573133adecb00250b.jpeg",
      "authorized": true,
      "createdAt": 1479224913777449,
      "updatedAt": 1480646056534157,
      "deletedAt": null,
      "id": 293
   }
}

```

### 5. delete
 #### 테마 제거

> * del.validate() : 유효성검사

> * del.findThemeImage() : 삭제할 AppTheme테이블 로우조회

> * del.destroy() : AppTheme테이블 로우

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppThemeId |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 

