# book-product-authorizations

## /api/pongift/book-product-authorizations

### 1. gets
#### 상품인증예약 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| orderBy | enum |  false | 정렬 기준 필드 bookAt, createdAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |

#### 응답 파라미터

[AppBookProductAuthorizations](/markdown/db/AppBookProductAuthorizations.md)

<br /><br /><br /><br />

### 2. post
#### 상품인증예약 생성

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int | true | 상품 ID |
| authorized | boolean | true | 인증 여부 |
| bookAt | datetime | true | 예약일 |


#### 응답 파라미터

[AppBookProductAuthorizations](/markdown/db/AppBookProductAuthorizations.md)

<br /><br /><br /><br />

### 3. delete
#### 상품인증예약 제거

#### 응답 코드 204

<br /><br /><br /><br />

