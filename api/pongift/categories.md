# categories

## /api/pongift/categories

### 1. get
 #### 단일 얻기

> * 입력받은 ID에 해당하는 카테고리 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | Int | true | 카테고리 ID |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
[AppCategories 테이블 참고](/markdown/db/AppCategories.md)
```
{
   "authorId": 8,
   "type": "service",
   "name": "베이커리/떡",
   "visibility": false,
   "code": null,
   "depth": null,
   "createdAt": 1476764955608544,
   "updatedAt": 1495615811325437,
   "deletedAt": null,
   "id": 31
}
```


 <br /><br /><br /><br />

### 2. gets
#### 카테고리 목록 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| type | ENUM |  false | 카테고리 유형 service, product, storeFarm |
| visibility | Int |  false | 표시 유무 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
[AppCategories 테이블 참고](/markdown/db/AppCategories.md)
```
{
   "count": 3,
   "list": [
      {
         "authorId": 8,
         "type": "service",
         "name": "베이커리/떡",
         "visibility": false,
         "code": null,
         "depth": null,
         "createdAt": 1476764955608544,
         "updatedAt": 1495615811325437,
         "deletedAt": null,
         "id": 31
      },
      {
         "authorId": 8,
         "type": "service",
         "name": "안경",
         "visibility": true,
         "code": null,
         "depth": null,
         "createdAt": 1479130153042118,
         "updatedAt": 1495615814526503,
         "deletedAt": null,
         "id": 47
      },
      {
         "authorId": 8,
         "type": "service",
         "name": "영화/엔터테인먼트",
         "visibility": true,
         "code": null,
         "depth": null,
         "createdAt": 1476764476887836,
         "updatedAt": 1476764479139625,
         "deletedAt": null,
         "id": 28
      },
   ]
}
```


 <br /><br /><br /><br />

### 3. post
#### 카테고리 생성

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| type | ENUM | false | 카테고리 유형 service, product, storeFarm |
| name | varchar | false | 카테고리명 |
 

#### 응답 코드 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 201

<br /><br /><br /><br />

### 4. put
#### 카테고리 수정

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| type | ENUM |  false | 카테고리 유형 service, product, storeFarm |
| name | varchar |  false | 카테고리명 |
| visibility | Int |  false | 사용/미사용 |
| id | Int | true | 카테고리 아이디 |

#### 응답 코드  - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 <br /><br /><br /><br />

### 5. delete
#### 카테고리 제거

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | Int | true | 카테고리 아이디 |
 

#### 응답 코드  - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 204

 <br /><br /><br /><br />

