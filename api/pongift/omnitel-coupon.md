# omnitel-coupon

## /api/pongift/omnitel-coupon

### 1. gets
#### 문화상품권 쿠폰번호 cs 처리

> * 권한 (roleAdmin)

> * 사용하지 않고 미전송상태인 발송정보를 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int | true | 유통대행 서비스 ID |
| couponNumber | string |  false | 옴니텔에서 받은 쿠폰 번호 |
| sendPhoneNum | string |  true | 보내는 전화번호 |
| receivePhoneNum | string |  true | 받는 전화번호 |
| requestPurchaseNum | string |  false | 주문 ID |
| price | int |  true | 상품가격 |

#### 응답 파라미터

| parameter | desc |
| --------- | :---- |
| id | 구매 id |
| supplyPrice | 공급가 |
| realPrice | 실제가격 |
| tno | 결제 번호(KCP) |
| productTransferId | 발송정보 id |
| sendState | 발송 상태 |
| destination | 받는 전화번호 |
| externalCode | 쿠폰번호 |
| phoneNum | 보내는 전화번호 |
| serviceUid | 서비스 유저 계정id |
| name | 발신인 이름 |
| productName | 상품명 |
| storefarmOrderInfoId | 스토어팜 |


<br /><br /><br /><br />

### 2. post
#### 문화상품권 자체발송 후 스토어팜 쿠폰번호 변경 및 변경여부 체크

> * 권한 (roleAdmin)

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| couponNum | string |  true | 쿠폰번호 |
| couponOrderNum | string |  false | 주문 ID |
| purchaseId | int |  true | purchaseId |
| productTransferId | int |  true | productTransferId |


#### 응답 코드 200

<br /><br /><br /><br />

