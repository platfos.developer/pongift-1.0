# product-upload

## /api/pongift/product-upload

### 1. post
 #### 상품 생성 (벌크)

> * post.validate() : 유효성검사

> * post.checkExistService() : 서비스 유효성검사

> * post.createImportHistory() : AppImportHistory테이블 로우생성 (업로드이력을 남기기위해)

> * post.series() : 상품생성 <br>
             1. post.seriesParseCsvFile() : CSV파일 파싱 <br>
             2. post.seriesUpsertProducts() : AppProduct테이블 로우생성



#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| folder | string |  false | 업로드할 폴더 |
| serviceId | int |  false | 서비스 ID |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppImportHistory | AppImportHistory객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
  "AppImportHistory" : {}
}

```
