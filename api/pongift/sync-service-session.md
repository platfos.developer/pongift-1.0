# sync-service-session

## /api/pongift/sync-service-session

### 1. post
 #### 서비스아이디와 uid 쿠키 확인  api

> * post.validate() : 유효성검사

> * post.setParam() : 테이블조건을 위한 설정

> * post.setServiceSession() : AppServiceSession테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스아이디 |
| uid | int |  false | 유저 id |
 
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppServiceSession | AppServiceSessions객체 |
 
 <br /><br /><br /><br />
 
 
#### 응답 예제 (JSON)  

```json
{
   "data": [
      {
         "cookie": "slogupSessionId=s%3AAwOG3nBWuPdi6VuQfc984bLkpcIoRaUN.QhWIfMYo1YLVE90SNuHsMQQ2xhwipCVY%2Fy1ZDrWqxkk",
         "expiration": 1521338887061,
         "serviceId": "1",
         "uid": "admin",
         "createdAt": null,
         "updatedAt": 1512698887142733,
         "deletedAt": null,
         "id": 1
      }
   ]
}
```
