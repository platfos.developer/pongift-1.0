# user-profile

## /api/pongift/user-profile

### 1. put
 #### 자기소개 수정

> * put.validate() : 유효성검사

> * put.updateProfile() : Profile테이블 로우업데이트

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userId | |  false | 유저아이디 |
| state | |  false | 유저상태 normal, active, sleep, leave |
| wedding | |  false | 결혼기념일 |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음 204

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
