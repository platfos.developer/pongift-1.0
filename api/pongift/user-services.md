# user-services

## /api/pongift/user-services

### 1. get
 #### user-services 단일 얻기

> * get.validate() : 유효성검사

> * get.setParam() : 조건에 맞는 AppUserManage테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppUserManageId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppUserManage | AppUserManages객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  
 
 ```json
{
   "authorId": 2,
   "userId": 3,
   "corporationId": 1,
   "serviceId": null,
   "createdAt": 1484732930096110,
   "updatedAt": 1484732930096146,
   "id": 1
}
```

### 2. gets
 #### 유저매니지 브릿지 조회

> * gets.validate() : 유효성검사

> * gets.setParam() : 조건에맞는 AppUserManage테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userId | |  false | user id |
| serviceId | |  false | 서비스 id |
| orderBy | |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | |  true  | 정렬 방식 DESC, ASC |
| last | |  false | 조회 기준 데이터 일자 |
| size | |  false | 가져올 데이터 갯수 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppUserManage | AppUserManage객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
[
   {
      "authorId": 2,
      "userId": 163,
      "corporationId": null,
      "serviceId": 53,
      "createdAt": 1506763743012045,
      "updatedAt": 1506763743012074,
      "id": 53,
      "user": {
         "aid": "cultureland2@pongift.com",
         "email": "cultureland2@pongift.com",
         "secret": "=",
         "salt": "=",
         "phoneNum": null,
         "name": null,
         "nick": "컬쳐랜드(벌크서비스)",
         "role": "roleD",
         "gender": null,
         "birth": null,
         "isVerifiedEmail": false,
         "country": "kr",
         "language": "ko",
         "isReviewed": false,
         "agreedEmail": true,
         "agreedPhoneNum": true,
         "agreedTermsAt": null,
         "profileId": 163,
         "di": null,
         "ci": null,
         "createdAt": 1506763743004254,
         "updatedAt": 1506763743004284,
         "deletedAt": null,
         "passUpdatedAt": 1506763213389503,
         "id": 163
      }
   }
]

```

### 3. post
 #### 유저매니지 브릿지 생성

> * post.validate() : 유효성검사

> * post.checkAlreadyHasUserManage() : AppUserManage테이블 조회 ( 기존에 생성된 AppUserManage 로우가 있는지)

> * post.setParam() :  AppUserManage테이블 로우 업데이트 or 생성

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userId | int |  false | user id |
| serviceId | int |  false | 서비스 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManage객체 |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
  userManage : {}
}

```

### 4. put
 #### 유저매니지 브릿지 수정

> * put.validate() : 유효성검사

> * put.updateUserService() : 조건에 맞는 AppUserManage테이블 로우 업데이트

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userId | int |  false | user id |
| serviceId | int |  false | 서비스 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManage객체 |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
  userManage : {}
}

```

### 5. delete
 #### 유저매니지 브릿지 제거

> * del.validate() : 유효성검사

> * del.hasAuthorization() : 조건에 해당하는 AppUserManage테이블 로우 권한여부 조회

> * del.findUserManage() :조건에 해당하는 AppUserManage테이블 로우조회

> * del.destroy() : AppUserManage테이블 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppUserManageId | 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200


 | parameter | desc |
 | --------- | :---- |
 | AppUserManage | AppUserManage객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
  userManage : {}
}

```

