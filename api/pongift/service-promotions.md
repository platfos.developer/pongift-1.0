# service-promotions

## /api/pongift/service-promotions

### 1. get
#### 서비스 프로모션 단일 조회

#### 응답 파라미터

[AppServicePromotions](/markdown/db/AppServicePromotions.md)
<br /><br /><br /><br />

### 2. gets
#### 검색조건에 따라 서비스 프로모션 브릿지 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt, index |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 데이터 |
| size | int |  false | 가져올 데이터 갯수 |
| serviceId | int |  false | 서비스 id |
| promotionId | int |  false | 프로모션 id |
| corporationId | int |  false | 법인 id |
| visibility | boolean |  false | visibility |

#### 응답 파라미터

[AppServicePromotions](/markdown/db/AppServicePromotions.md)


<br /><br /><br /><br />

### 3. post
#### 서비스 프로모션 브릿지 생성

> * 권한 (roleAdmin 이상 && (유저id == 서비스 id) )

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int | true | 서비스 id |
| promotionId | int | true | 프로모션 id |


#### 응답 파라미터

[AppServicePromotions](/markdown/db/AppServicePromotions.md)

<br /><br /><br /><br />

### 4. put
#### 서비스 프로모션 브릿지 수정
> * 권한 (roleAdmin 이상 && (유저id == 서비스 id) )

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | |  false | 서비스 id |
| promotionId | |  false | 프로모션 id |


#### 응답 파라미터

[AppServicePromotions](/markdown/db/AppServicePromotions.md)

<br /><br /><br /><br />

### 5. delete
#### 서비스 프로모션 브릿지 제거
> * 권한 (roleAdmin 이상 && (유저id == 서비스 id) )

#### 응답 코드 204

<br /><br /><br /><br />

