# purchase-get

## /api/pongift/purchase-get

### 1. get
#### 구매 요청

#### 요청 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int | true | 서비스 id |
| productId | int|  true | 상품 id |
| promotionId int| |  false | 프로모션 id |
| themeId | int|  false | 테마 id |
| type | enum |  false | 구매 유형 |
| destinations | string|  true | 수신인 |
| amounts | int |  false | 구매 수량 |
| charge | int |  true | 수수료 |
| customerPrice | int|  true | 소비자가격 |
| supplyPrice | int |  true | 공급가격 |
| realPrice | int |  true | 실판매가격 |
| vatType | enum |  true | vat 포함 유무 |


#### 응답 파라미터

| parameter | desc |
| --------- | :---- |

<br /><br /><br /><br />

