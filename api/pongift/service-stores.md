# service-stores

## /api/pongift/service-stores

### 1. get
 #### 매장 서비스 조회

> * get.validate() : 유효성거사

> * get.setParam() : 조건에 맞는 AppServiceStore테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppServiceStorerId |
 
#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200


 | parameter | desc |
 | --------- | :---- |
 | AppServiceStore | AppServiceStores객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 1,
   "serviceId": 46,
   "storeId": 7,
   "createdAt": 1491806629328276,
   "updatedAt": 1491806629328295,
   "id": 10
}

```

### 2. gets
 #### 매장 서비스 브릿지 조회

> * gets.validate() : 유효성검사

> * gets.setParam () : AppServiceStore테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | id |  false | 검색할 필드 id |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microTimestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| serviceId | int |  false | 서비스 id |
| storeId | int |  false | 매장 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppServiceStore | AppServiceStores객체 |
 | service | Appservices객체 |
 | store | Appstores객체 |
 | author | Appauthors객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
[
   {
      "authorId": 1,
      "serviceId": 46,
      "storeId": 7,
      "createdAt": 1491806629328276,
      "updatedAt": 1491806629328295,
      "id": 10,
      "service": {
         "accessKey": "==",
         "authorId": 1,
         "affiliationId": 1,
         "corporationId": 1,
         "categoryId": null,
         "externalEnterprise": null,
         "externalCode": null,
         "state": "standby",
         "type": "distribution",
         "calculationType": "used",
         "name": "대행서비스생성",
         "pongiftContactId": 1,
         "local": false,
         "pgUsage": true,
         "pgCardUsage": true,
         "pgPhoneUsage": false,
         "pgAccountUsage": false,
         "paymentBank": null,
         "paymentDepositor": null,
         "paymentAccount": null,
         "customerCenterPhoneNum": null,
         "logoImgId": null,
         "distribution": "online",
         "businessRange": "channel",
         "info": null,
         "country": "kr",
         "headerColor": null,
         "templateImageKey": null,
         "createdAt": 1489029391664310,
         "updatedAt": 1489029476124496,
         "deletedAt": null,
         "id": 46
      },
      "store": {
         "authorId": 1,
         "corporationId": 1,
         "name": "슬로그업 합동점",
         "address": "대한민국 서울특별시 마포구 서교동 459-4",
         "location": {
            "type": "Point",
            "coordinates": [
               37.5552971,
               126.91601390000005
            ]
         },
         "phoneNum": "01000000000",
         "info": "slogup",
         "createdAt": 1483673309141454,
         "updatedAt": 1491897866119288,
         "deletedAt": null,
         "id": 7
      },
      "author": {
         "aid": "dusrn@slogup.com",
         "email": "dusrn@slogup.com",
         "secret": "=",
         "salt": "==",
         "phoneNum": "+821092185131",
         "name": "김종엽",
         "nick": "dusrn",
         "role": "roleS",
         "gender": null,
         "birth": null,
         "isVerifiedEmail": false,
         "country": "KR",
         "language": "ko",
         "isReviewed": false,
         "agreedEmail": true,
         "agreedPhoneNum": true,
         "agreedTermsAt": null,
         "profileId": 1,
         "di": null,
         "ci": null,
         "createdAt": 1475995092714968,
         "updatedAt": 1488855458740117,
         "deletedAt": null,
         "passUpdatedAt": 1488855458737335,
         "id": 1
      }
   },
   {
      "authorId": 1,
      "serviceId": 47,
      "storeId": 7,
      "createdAt": 1491806629327685,
      "updatedAt": 1491806629327739,
      "id": 9,
      "service": {
         "accessKey": "==",
         "authorId": 1,
         "affiliationId": 1,
         "corporationId": 1,
         "categoryId": null,
         "externalEnterprise": null,
         "externalCode": null,
         "state": "standby",
         "type": "distribution",
         "calculationType": "purchased",
         "name": "ACCESSKEY CREATE TEST",
         "pongiftContactId": 1,
         "local": false,
         "pgUsage": true,
         "pgCardUsage": false,
         "pgPhoneUsage": false,
         "pgAccountUsage": false,
         "paymentBank": null,
         "paymentDepositor": null,
         "paymentAccount": null,
         "customerCenterPhoneNum": null,
         "logoImgId": null,
         "distribution": "online",
         "businessRange": "corporation",
         "info": null,
         "country": "kr",
         "headerColor": null,
         "templateImageKey": null,
         "createdAt": 1490951747045486,
         "updatedAt": 1490951747074219,
         "deletedAt": null,
         "id": 47
      },
      "store": {
         "authorId": 1,
         "corporationId": 1,
         "name": "슬로그업 합동점",
         "address": "대한민국 서울특별시 마포구 서교동 459-4",
         "location": {
            "type": "Point",
            "coordinates": [
               37.5552971,
               126.91601390000005
            ]
         },
         "phoneNum": "01000000000",
         "info": "slogup",
         "createdAt": 1483673309141454,
         "updatedAt": 1491897866119288,
         "deletedAt": null,
         "id": 7
      },
      "author": {
         "aid": "dusrn@slogup.com",
         "email": "dusrn@slogup.com",
         "secret": "=",
         "salt": "==",
         "phoneNum": "+821092185131",
         "name": "김종엽",
         "nick": "dusrn",
         "role": "roleS",
         "gender": null,
         "birth": null,
         "isVerifiedEmail": false,
         "country": "KR",
         "language": "ko",
         "isReviewed": false,
         "agreedEmail": true,
         "agreedPhoneNum": true,
         "agreedTermsAt": null,
         "profileId": 1,
         "di": null,
         "ci": null,
         "createdAt": 1475995092714968,
         "updatedAt": 1488855458740117,
         "deletedAt": null,
         "passUpdatedAt": 1488855458737335,
         "id": 1
      }
   },
   {
      "authorId": 1,
      "serviceId": 36,
      "storeId": 3,
      "createdAt": 1483698647524381,
      "updatedAt": 1483698647524414,
      "id": 7,
      "service": {
         "accessKey": "==",
         "authorId": 20,
         "affiliationId": 1,
         "corporationId": 13,
         "categoryId": null,
         "externalEnterprise": null,
         "externalCode": null,
         "state": "use",
         "type": "distribution",
         "calculationType": "used",
         "name": "스마트dmb",
         "pongiftContactId": 20,
         "local": false,
         "pgUsage": true,
         "pgCardUsage": true,
         "pgPhoneUsage": false,
         "pgAccountUsage": false,
         "paymentBank": null,
         "paymentDepositor": null,
         "paymentAccount": null,
         "customerCenterPhoneNum": null,
         "logoImgId": 271,
         "distribution": "offline",
         "businessRange": "corporation",
         "info": null,
         "country": "kr",
         "headerColor": null,
         "templateImageKey": null,
         "createdAt": 1477037490277743,
         "updatedAt": 1487040078696459,
         "deletedAt": null,
         "id": 36
      },
      "store": {
         "authorId": 1,
         "corporationId": 13,
         "name": "명동1점",
         "address": "서울시 중구 명동 322",
         "location": {
            "type": "Point",
            "coordinates": [
               37.55998,
               126.9858296
            ]
         },
         "phoneNum": "02-333-4444",
         "info": "명동 전철역 4번출구역에서 100미터",
         "createdAt": 1477037397920589,
         "updatedAt": 1483924358461514,
         "deletedAt": null,
         "id": 3
      },
      "author": {
         "aid": "dusrn@slogup.com",
         "email": "dusrn@slogup.com",
         "secret": "=",
         "salt": "==",
         "phoneNum": "+821092185131",
         "name": "김종엽",
         "nick": "dusrn",
         "role": "roleS",
         "gender": null,
         "birth": null,
         "isVerifiedEmail": false,
         "country": "KR",
         "language": "ko",
         "isReviewed": false,
         "agreedEmail": true,
         "agreedPhoneNum": true,
         "agreedTermsAt": null,
         "profileId": 1,
         "di": null,
         "ci": null,
         "createdAt": 1475995092714968,
         "updatedAt": 1488855458740117,
         "deletedAt": null,
         "passUpdatedAt": 1488855458737335,
         "id": 1
      }
   }
]

```

### 3. post
 #### 매장 서비스 브릿지 생성

> * post.validate() : 유효성검사

> * post.setParam() : AppServiceStore테이블 생성

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스 id |
| storeId | int |  false | 매장 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManages객체 |

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
{
   "authorId": 1,
   "serviceId": 46,
   "storeId": 7,
   "createdAt": 1491806629328276,
   "updatedAt": 1491806629328295,
   "id": 10
}

```


### 4. put
 #### 매장 서비스 브릿지 수정

> * put.validate() : 유효성검사

> * put.updateServiceStore() : AppServiceStore테이블 업데이트

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스 id |
| storeId | int |  false | 매장 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManages객체 |
 
 
 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 1,
   "serviceId": 46,
   "storeId": 7,
   "createdAt": 1491806629328276,
   "updatedAt": 1491806629328295,
   "id": 10
}

```

### 5. delete
 #### 매장 서비스 브릿지 제거

> * del.validate() : 유효성검사

> * del.destroy() : AppServiceStore테이블 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppServiceStoreId |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |


