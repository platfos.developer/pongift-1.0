## 폰기프트

### 1. [정산처리](/markdown/api/pongift/adjustments.md)
### 2. [제휴사](/markdown/api/pongift/affiliations.md)
### 3. [상품 승인](/markdown/api/pongift/apply-product-authorization.md)
### 4. [상품 인증 예약 업로드](/markdown/api/pongift/book-product-authorization-upload.md)
### 5. [상품 인증 예약](/markdown/api/pongift/book-product-authorizations.md)
### 6. [정산 비교](/markdown/api/pongift/calculation-comparison.md)
### 7. [정산 정보](/markdown/api/pongift/calculations.md)
### 8. [카테고리](/markdown/api/pongift/categories.md)
### 9. [카테고리 노출여부](/markdown/api/pongift/category-visibility.md)
### 10. [담당자](/markdown/api/pongift/contacts.md)
### 11. [법인](/markdown/api/pongift/corporations.md)
### 12. [이벤트](/markdown/api/pongift/events.md)
### 13. [CSV 다운로드](/markdown/api/pongift/export-histories.md)
### 14. [스토어팜 수신자 환불](/markdown/api/pongift/force-purchase.md)
### 15. [이미지](/markdown/api/pongift/images.md)
### 16. [CSV 업로드](/markdown/api/pongift/import-histories.md)
### 17. [메타정보](/markdown/api/pongift/meta.md)
### 18. [공지사항](/markdown/api/pongift/notices.md)
### 19. [옴니텔 미전송시 쿠폰번호 cs 처리](/markdown/api/pongift/omnitel-coupon.md)
### 20. [상품 이미지](/markdown/api/pongift/product-images.md)
### 21. [상품 정보](/markdown/api/pongift/product-infos.md)
### 22. [상품 프로모션](/markdown/api/pongift/product-promotions.md)
### 23. [상품 테마](/markdown/api/pongift/product-themes.md)
### 24. [쿠폰](/markdown/api/pongift/product-transfers.md)
### 25. [쿠폰 cs](/markdown/api/pongift/product-transfers-cs.md)
### 26. [상품 csv 업로드](/markdown/api/pongift/product-upload.md)
### 27. [상품](/markdown/api/pongift/products.md)
### 28. [프로모션 노출여부](/markdown/api/pongift/promotion-visibility.md)
### 29. [프로모션](/markdown/api/pongift/promotions.md)
### 30. [구매 취소](/markdown/api/pongift/purchase-cancel.md)
### 31. [오래된 구매내역 삭제](/markdown/api/pongift/purchase-delete.md)
### 32. [구매 제거](/markdown/api/pongift/purchase-fail.md)
### 33. [구매 요청](/markdown/api/pongift/purchase-get.md)
### 34. [구매 성공](/markdown/api/pongift/purchase-success.md)
### 35. [구매](/markdown/api/pongift/purchases.md)
### 36. [푸시](/markdown/api/pongift/pushes.md)
### 37. [환불 인증번호 전송](/markdown/api/pongift/refund-auth.md)
### 38. [재전송](/markdown/api/pongift/resend.md)
### 39. [SDK 초기화](/markdown/api/pongift/sdk-initial.md)
### 40. [서비스 프로모션](/markdown/api/pongift/service-promotions.md)
### 41. [서비스 매장](/markdown/api/pongift/service-stores.md)
### 42. [서비스](/markdown/api/pongift/services.md)
### 43. [매장 이미지](/markdown/api/pongift/store-images.md)
### 44. [매장](/markdown/api/pongift/stores.md)
### 45. [서비스아이디와 uid 쿠키 확인](/markdown/api/pongift/sync-service-session.md)
### 46. [테마](/markdown/api/pongift/themes.md)
### 47. [서드파티 상품](/markdown/api/pongift/third-party-product.md)
### 48. [유저매니저](/markdown/api/pongift/user-corporations.md)
### 49. [유저매니저](/markdown/api/pongift/user-manages.md)
### 50. [유저 프로필](/markdown/api/pongift/user-profile.md)
### 51. [유저 서비스](/markdown/api/pongift/user-services.md)
### 52. [유저](/markdown/api/pongift/users.md)