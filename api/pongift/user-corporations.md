# user-corporations

## /api/pongift/user-corporations

### 1. get
 #### 법인 정보 조회

> * get.validate() : 유효성검사

> * get.setParam() : 조건에 맞는 AppUserManage테이블의 로우 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppUserManageId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppUserManage | AppUserManages객체 |
 | AppCorporation | AppCorporations객체 |
 | AppService | AppServices객체 |
 

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
  "AppUserManage" : {
                    "corporation" : {
                                    "corporation" : {},
                                    "logoImg" : {}
                                    },
                    "service" : {
                                "affiliation" : {},
                                "corporation" : {},
                                "category": {},
                                "pongiftContact": {},
                                "logoImg" : {},
                                "calculations" : {}
                                }
                    }
}

```


### 2. gets
 #### 유저매니지 브릿지 조회

> * gets.validate() : 유효성검사

> * gets.setParam() : 조건에 맞는 AppUserManage테이블의 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userId | int |  false | user id |
| corporationId | int |  false | 법인 id |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microTimestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppUserManage | AppUserManages객체 |
 | user | Users객체 |
 | corporation | AppCorporation객체 |
 | service | AppService객체 |
 

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
  AppUserManage : {
                  "user" : {},
                  "corporation" : {},
                  "service" : {}
                  }
}

```

### 3. post
 #### 유저매니지 브릿지 생성

> * post.validate() : 유효성검사

> * post.checkAlreadyHasUserManage() : 기존데이터가 있는지 AppUserManage테이블 로우조회

> * post.setParam() : AppUserManage테이블 update or create

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userId | int |  false | user id |
| corporationId | int |  false | 법인 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManages객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
  "userManage" : {}
}

```

### 4. put
 #### 유저매니지 브릿지 수정

> * put.validate() : 유효성검사

> * put.updateUserCorporation() : AppUserManage테이블의 update or create

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| userId | int |  false | user id |
| corporationId | int |  false | 법인 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 

#### 응답 예제 (JSON)  

```json
{
  "userManage" : {}
}

```

### 5. delete
 #### 유저매니지 브릿지 제거

> * del.validate() : 유효성검사

> * del.findUserManage() : AppUserManage테이블의 로우조회

> * del.destroy() : 조건에 맞는 AppUserManage테이블의 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | ture | AppUserManageId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManages객체 |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
  "userManage" : {}
}

```

