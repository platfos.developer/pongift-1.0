# purchase-cancel

## /api/pongift/purchase-cancel

### 1. post
#### 구매 취소

> * 옴니텔, 스마트인피니 쿠폰 취소요청

> * 정산처리 후 구매취소 처리

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| tno | string | true | 주문 승인 번호 |


#### 응답 파라미터

[AppPurchases](/markdown/db/AppPurchases.md)

<br /><br /><br /><br />

