# purchases

## /api/pongift/purchases

### 1. get
#### 단일 얻기
 > * 구매아이디에 해당하는 구매정보를 조회한다.
 > * 제품 프로모션이 진행중이라면 할인정보를 포함하여 데이터를 리턴한다.
 > * 스토어팜의 구매정보도 포함하여 데이터를 리턴한다.
 
#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | Int | true | 구매 아이디 |

#### 응답 파라미터

테이블 참고

[AppPurchases](/markdown/db/AppPurchases.md)

[AppProducts](/markdown/db/AppProducts.md)

[AppCategories](/markdown/db/AppCategories.md)

[AppServices](/markdown/db/AppServices.md)

[AppThemes](/markdown/db/AppThemes.md)

[AppPromotions](/markdown/db/AppPromotions.md)

[AppProductPromotions](/markdown/db/AppProductPromotions.md)

[AppProductTransfers](/markdown/db/AppProductTransfers.md)

[AppStorefarmOrderInfos](/markdown/db/AppStorefarmOrderInfos.md)

#### 응답 예제
```
{
   "userId": null,
   "nonmemberId": 36,
   "serviceId": 52,
   "productId": 42,
   "promotionId": null,
   "productPromotionId": null,
   "themeId": null,
   "state": "purchase",
   "type": "cardEasyPayment",
   "sendMethod": "sms",
   "charge": 8,
   "distributionCharge": 0,
   "customerPrice": 5400,
   "supplyPrice": 4968,
   "realPrice": 5400,
   "vatType": "separate",
   "message": null,
   "purchasedAt": "2017-09-26T00:39:30.000Z",
   "purchaseCode": "1506386369652200",
   "purchaseAuthorizationCode": "2017092612033811",
   "tno": "2017092612033811",
   "cash_no": null,
   "cash_authno": null,
   "externalCode": "201709260000000001091293093654",
   "canceledAt": null,
   "cancelCode": null,
   "cancelAuthorizationCode": null,
   "canCancelAt": "2017-11-25T00:39:30.000Z",
   "createdAt": 1506386369963901,
   "updatedAt": 1506386377502691,
   "deletedAt": null,
   "id": 1007,
   "product": {
      "authorId": 91,
      "serviceId": 31,
      "categoryId": 38,
      "state": "authorized",
      "type": "exchange",
      "weight": 0,
      "externalCode": "0000004618",
      "name": "불고기와퍼스토어팜",
      "optionName": null,
      "info1": "전국 버거킹 매장에서 사용 가능합니다 \n※사용불가 매장\n- 오션월드점, 대명비발디점, 잠실야구장점, 문학야구장점, 여주휴게소점, 지산리조트점 , 경남대점, 삼성라이온즈파크점",
      "info2": "버거킹은 미국의 패스트 푸드 업체이다. 오스트레일리아에서는 '헝그리 잭스'라는 레스토랑 프랜차이즈를 두고 있으며, 일본에서는 롯데리아가 총판 운영되고 있다. 버거킹의 독자 햄버거 이름은 와퍼이기도 하다.",
      "info3": "※사용불가 매장\n- 오션월드점, 대명비발디점, 잠실야구장점, 문학야구장점, 여주휴게소점, 지산리조트점 , 경남대점, 삼성라이온즈파크점\n※일부 메뉴 제외 매장\n- 강남점: 갈릭스테이크 단품, 갈릭스테이크 세트, X-TRA 크런치버거 단품, X-TRA 크런치버거 세트\n- 인천신세계점: 아이스크림메뉴 제외\n※쿠폰사용가능시간\n- 오전10시 ~ 오후 10시",
      "info4": "★유의사항\n-모바일쿠폰에 기재 된 메뉴로만 교환이 가능합니다.\n-컨디멘트 추가 가능, 올엑스트라 가능\n-사이드 메뉴 및 음료 교환 가능 (단, 행사메뉴 및 할인적용메뉴 제외)\n-상품교환 시 매장 내 행사 혜택에서 제외됩니다.\n-본 상품권 교환 시 OK Cashbag 적립은 불가합니다.\n-현금영수증 요청 시 발행 가능합니다.\n-환불,교환,승인문의는 상품권 구매처에 문의하여 주시기 바랍니다\n-대량 상품권 사용 시 사전에 사용희망 매장에 전화 문의하시면 빠른 처리가 가능합니다.",
      "info5": "매장 계산대에서 직원에게 바코드를 제시해주세요.",
      "expiryDay": 99999,
      "endDay": null,
      "storeFarmType": "use",
      "tax": "taxFree",
      "authorizedAt": null,
      "createdAt": 1476872987717278,
      "updatedAt": 1505200005085404,
      "deletedAt": null,
      "id": 42,
      "category": {
         "authorId": 8,
         "type": "product",
         "name": "버거/피자/치킨",
         "visibility": true,
         "code": null,
         "depth": null,
         "createdAt": 1476782406143421,
         "updatedAt": 1488154927037907,
         "deletedAt": null,
         "id": 38
      },
      "service": {
         "accessKey": "6EpiQmif/h0T9pu0ocaJaQ==",
         "authorId": 1,
         "affiliationId": 1,
         "corporationId": 10,
         "categoryId": 37,
         "externalEnterprise": "omnitel",
         "externalCode": "POC0000126",
         "state": "use",
         "type": "supply",
         "calculationType": "used",
         "name": "버거킹",
         "pongiftContactId": 20,
         "local": false,
         "pgUsage": true,
         "pgCardUsage": true,
         "pgPhoneUsage": false,
         "pgAccountUsage": false,
         "paymentBank": "국민",
         "paymentDepositor": "(주)비케이알",
         "paymentAccount": "1245879-5254-63-21",
         "customerCenterPhoneNum": "080-022-8163",
         "logoImgId": 113,
         "distribution": null,
         "businessRange": null,
         "info": "★유의사항\n-모바일쿠폰에 기재 된 메뉴로만 교환이 가능합니다.\n-컨디멘트 추가 가능, 올엑스트라 가능\n-사이드 메뉴 및 음료 교환 가능 (단, 행사메뉴 및 할인적용메뉴 제외)\n-상품교환 시 매장 내 행사 혜택에서 제외됩니다.\n-본 상품권 교환 시 OK Cashbag 적립은 불가합니다.\n-현금영수증 요청 시 발행 가능합니다.\n-환불,교환,승인문의는 상품권 구매처에 문의하여 주시기 바랍니다\n-대량 상품권 사용 시 사전에 사용희망 매장에 전화 문의하시면 빠른 처리가 가능합니다.",
         "country": "kr",
         "headerColor": null,
         "templateImageKey": null,
         "createdAt": 1476853742400884,
         "updatedAt": 1486450318584386,
         "deletedAt": null,
         "id": 31,
         "corporation": {
            "authorId": 1,
            "corporationId": null,
            "type": "corporation",
            "number": "123-45-67891",
            "name": "(주)비케이알",
            "address": "부산 부산진구 서전로10번길 64",
            "leader": "문영주",
            "condition": "프랜차이즈",
            "category": "외식",
            "logoImgId": 99,
            "state": "contract",
            "contractedAt": null,
            "canceledAt": null,
            "country": "kr",
            "createdAt": 1476771063100574,
            "updatedAt": 1482981491337656,
            "deletedAt": null,
            "id": 10
         }
      },
      "productImages": [
         {
            "productId": 42,
            "imageId": 163,
            "index": 1,
            "createdAt": 1505200005079356,
            "updatedAt": 1505200005079387,
            "id": 813,
            "image": {
               "authorId": 8,
               "folder": "product",
               "dateFolder": "2016-12-13",
               "name": "upload_071fa44de4fca757c867372a91968d45.jpg",
               "authorized": true,
               "createdAt": 1476927415520262,
               "updatedAt": 1476927415520297,
               "deletedAt": null,
               "id": 163
            }
         }
      ]
   },
   "service": {
      "accessKey": "Ey6k6E/xmoJBw7+uCq/jqA==",
      "authorId": 91,
      "affiliationId": 1,
      "corporationId": 22,
      "categoryId": null,
      "externalEnterprise": null,
      "externalCode": null,
      "state": "use",
      "type": "distribution",
      "calculationType": "purchased",
      "name": "스토어팜",
      "pongiftContactId": 91,
      "local": false,
      "pgUsage": true,
      "pgCardUsage": false,
      "pgPhoneUsage": false,
      "pgAccountUsage": false,
      "paymentBank": null,
      "paymentDepositor": null,
      "paymentAccount": null,
      "customerCenterPhoneNum": null,
      "logoImgId": 659,
      "distribution": "online",
      "businessRange": "corporation",
      "info": null,
      "country": "kr",
      "headerColor": null,
      "templateImageKey": null,
      "createdAt": 1493800880731079,
      "updatedAt": 1497316261922218,
      "deletedAt": null,
      "id": 52,
      "corporation": {
         "authorId": 91,
         "corporationId": null,
         "type": "corporation",
         "number": "00000000000",
         "name": "스토어팜",
         "address": null,
         "leader": "스토어팜",
         "condition": null,
         "category": null,
         "logoImgId": 658,
         "state": "standby",
         "contractedAt": null,
         "canceledAt": null,
         "country": "kr",
         "createdAt": 1493800648408488,
         "updatedAt": 1493800648408541,
         "deletedAt": null,
         "id": 22
      }
   },
   "theme": null,
   "promotion": null,
   "productPromotion": null,
   "productTransfers": [
      {
         "purchaseId": 1007,
         "state": "disuse",
         "sendState": "send",
         "destination": "+821029590224",
         "externalCode": "990189192445",
         "externalTransactionCode": null,
         "exchangeTransactionCode": null,
         "charge": null,
         "distributionCharge": null,
         "amount": 1,
         "usedAt": null,
         "useCanceledAt": null,
         "canceledAt": null,
         "usedLocationCode": null,
         "usedLocation": null,
         "refundStartedAt": null,
         "refundFinishedAt": null,
         "refundRate": null,
         "refundPrice": null,
         "refundBank": null,
         "refundDepositor": null,
         "refundAccount": null,
         "expiryAt": "2017-11-25T15:00:00.000Z",
         "extendExpiryDayCount": 0,
         "recentExtendExpiryDayAt": null,
         "reTransferCount": 0,
         "createdAt": 1506386369980281,
         "updatedAt": 1506386369980308,
         "deletedAt": null,
         "id": 1135
      },
      {
         "purchaseId": 1007,
         "state": "disuse",
         "sendState": "send",
         "destination": "+821029590224",
         "externalCode": "990560952870",
         "externalTransactionCode": null,
         "exchangeTransactionCode": null,
         "charge": null,
         "distributionCharge": null,
         "amount": 1,
         "usedAt": null,
         "useCanceledAt": null,
         "canceledAt": null,
         "usedLocationCode": null,
         "usedLocation": null,
         "refundStartedAt": null,
         "refundFinishedAt": null,
         "refundRate": null,
         "refundPrice": null,
         "refundBank": null,
         "refundDepositor": null,
         "refundAccount": null,
         "expiryAt": "2017-11-25T15:00:00.000Z",
         "extendExpiryDayCount": 0,
         "recentExtendExpiryDayAt": null,
         "reTransferCount": 0,
         "createdAt": 1506386369980387,
         "updatedAt": 1506386369980397,
         "deletedAt": null,
         "id": 1136
      },
      {
         "purchaseId": 1007,
         "state": "disuse",
         "sendState": "send",
         "destination": "+821029590224",
         "externalCode": "990868875392",
         "externalTransactionCode": null,
         "exchangeTransactionCode": null,
         "charge": null,
         "distributionCharge": null,
         "amount": 1,
         "usedAt": null,
         "useCanceledAt": null,
         "canceledAt": null,
         "usedLocationCode": null,
         "usedLocation": null,
         "refundStartedAt": null,
         "refundFinishedAt": null,
         "refundRate": null,
         "refundPrice": null,
         "refundBank": null,
         "refundDepositor": null,
         "refundAccount": null,
         "expiryAt": "2017-11-25T15:00:00.000Z",
         "extendExpiryDayCount": 0,
         "recentExtendExpiryDayAt": null,
         "reTransferCount": 0,
         "createdAt": 1506386369980463,
         "updatedAt": 1506386369980472,
         "deletedAt": null,
         "id": 1138
      }
   ],
   "user": null,
   "storefarmOrderInfo": {
      "orderId": "2017092611561221",
      "productOrderId": "2017092612033811",
      "productOrderStatus": "PAYED",
      "giftReceivingStatus": null,
      "orderType": "normal",
      "paymentDate": "2017-09-26T00:34:54.00Z",
      "claimStatus": null,
      "refundStandbyStatus": null,
      "orderName": "현승재",
      "ordererId": "s040****\b\b\b\b\b\b\b\b",
      "productID": "2000236459",
      "tel": "+821029590224",
      "externalCode": null,
      "quantity": 3,
      "state": "send",
      "createdAt": 1506386366873756,
      "updatedAt": 2017,
      "deletedAt": null,
      "id": 662
   }
}
```
 <br /><br /><br /><br />

### 2. gets
#### 구매 조회

> * 거래내역조회에 필요한 데이터를 리턴한다.

> * query.fileDownlod 값이 true일 경우 csv파일을 생성한다.
거래내역을 36등분(기본값:36) 한 뒤 async를 사용해 작업을 36개 돌리며 검색, 저장한다. exportHistroy테이블의 progress에 진행도를 업데이트 한다.

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | |  false | 검색할 내용 |
| searchField | |  false | 검색할 필드 id |
| orderBy | |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | |  true  | 정렬 방식 DESC, ASC |
| last | |  false | 조회 기준 데이터 일자 |
| size | |  false | 가져올 데이터 갯수 |
| userId | |  false | 구매자 id |
| productId | |  false | 상품 id |
| promotionId | |  false | 프로모션 id |
| themeId | |  false | 테마 id |
| serviceId | |  false | 서비스 id |
| state | |  false | 구매 상태 standby, purchase, cancel |
| notState | |  false | 구매 상태 제외 standby, purchase, cancel |
| sendMethod | |  false | 전송 방식 sms, lms, mms |
| serviceType | |  false | 서비스 유형 supply, distribution, etc |
| productTransferState | |  false | 상품 사용여부 disuse, use, partial, cancel, refundStart, refundFinish |
| externalCode | |  false | 상품 코드 |
| purchaseCode | |  false | 결제 요청 코드 |
| purchaseAuthorizationCode | |  false | 결제 승인 코드 |
| destination | |  false | 수신자 전화번호 |
| phoneNum | |  false | 발신자 전화번호  |
| manageCorporationId | |  false | 관리자 법인 id |
| manageServiceId | |  false | 관리자 서비스 id |
| purchaseStartAt | |  false | 거래내역조회 검색 시작일 | 
| purchaseEndAt | |  false | 거래내역조회 검색 종료일 | 
| fileDownload | |  false | csv file export 선택 |
| externalIsNull | |  false |  | 
| adjustmentServiceId | |  false | 정산 기준 조회 서비스 Id |
| adjustmentServiceType | |  false | 정산 기준 조회 서비스 유형 supply, distribution, etc |
| adjustmentStartAt | |  false | 정산 기준 조회 시작일 |
| adjustmentEndAt | |  false | 정산 기준 조회 끝일 |
| adjustmentCalculationType | |  false | 정산 기준 조회 정산기준 purchased, used |
| adjustmentVatType | |  false | 정산 기준 조회 VAT 유무 separate, include |
 

#### 응답 파라미터

테이블 참고

[AppPurchases](/markdown/db/AppPurchases.md)

[AppProducts](/markdown/db/AppProducts.md)

[AppCategories](/markdown/db/AppCategories.md)

[AppServices](/markdown/db/AppServices.md)

[AppThemes](/markdown/db/AppThemes.md)

[AppPromotions](/markdown/db/AppPromotions.md)

[AppProductPromotions](/markdown/db/AppProductPromotions.md)

[AppProductTransfers](/markdown/db/AppProductTransfers.md)

[AppStorefarmOrderInfos](/markdown/db/AppStorefarmOrderInfos.md)

 
#### 응답 예제

```
{
   "count": 1276,
   "list": [
      {
         "service": {
            "corporation": {
               "authorId": 20,
               "corporationId": null,
               "type": "corporation",
               "number": "123-55-21234",
               "name": "스마트테스트",
               "address": null,
               "leader": "스마트테스트",
               "condition": null,
               "category": null,
               "logoImgId": null,
               "state": "contract",
               "contractedAt": null,
               "canceledAt": null,
               "country": "kr",
               "createdAt": 1499044877768209,
               "updatedAt": 1499386682081648,
               "deletedAt": null,
               "id": 23
            },
            "accessKey": "OGjgO2SXlZOd9oM7jMkXlg==",
            "authorId": 20,
            "affiliationId": 1,
            "corporationId": 23,
            "categoryId": 47,
            "externalEnterprise": "smartInfini",
            "externalCode": null,
            "state": "use",
            "type": "supply",
            "calculationType": "used",
            "name": "스마트인피니(단품)",
            "pongiftContactId": 20,
            "local": 0,
            "pgUsage": 1,
            "pgCardUsage": 1,
            "pgPhoneUsage": 1,
            "pgAccountUsage": 1,
            "paymentBank": null,
            "paymentDepositor": null,
            "paymentAccount": null,
            "customerCenterPhoneNum": null,
            "logoImgId": null,
            "distribution": null,
            "businessRange": null,
            "info": null,
            "country": "kr",
            "headerColor": null,
            "templateImageKey": null,
            "createdAt": 1501657460680494,
            "updatedAt": 1501724720959855,
            "deletedAt": null,
            "id": 72
         },
         "product": {
            "service": {
               "accessKey": "OGjgO2SXlZOd9oM7jMkXlg==",
               "authorId": 20,
               "affiliationId": 1,
               "corporationId": 23,
               "categoryId": 47,
               "externalEnterprise": "smartInfini",
               "externalCode": null,
               "state": "use",
               "type": "supply",
               "calculationType": "used",
               "name": "스마트인피니(단품)",
               "pongiftContactId": 20,
               "local": 0,
               "pgUsage": 1,
               "pgCardUsage": 1,
               "pgPhoneUsage": 1,
               "pgAccountUsage": 1,
               "paymentBank": null,
               "paymentDepositor": null,
               "paymentAccount": null,
               "customerCenterPhoneNum": null,
               "logoImgId": null,
               "distribution": null,
               "businessRange": null,
               "info": null,
               "country": "kr",
               "headerColor": null,
               "templateImageKey": null,
               "createdAt": 1501657460680494,
               "updatedAt": 1501724720959855,
               "deletedAt": null,
               "id": 72
            },
            "authorId": 20,
            "serviceId": 72,
            "categoryId": 24,
            "state": "authorized",
            "type": "exchange",
            "weight": 0,
            "externalCode": "P201703282967",
            "name": "테디베어",
            "optionName": "소인/어린이",
            "info1": "교환처",
            "info2": "상품정보",
            "info3": "제한사항",
            "info4": "주의사항",
            "info5": "안내사항",
            "expiryDay": 60,
            "endDay": "2018-05-01",
            "storeFarmType": "del",
            "tax": "tax",
            "authorizedAt": null,
            "createdAt": 1501825966270186,
            "updatedAt": 1511235384710173,
            "deletedAt": null,
            "id": 1228
         },
         "productTransfers": [
            {
               "purchaseId": 1365,
               "state": "disuse",
               "sendState": "send",
               "destination": "+821040528027",
               "externalCode": "874393932013",
               "externalTransactionCode": null,
               "exchangeTransactionCode": null,
               "charge": null,
               "distributionCharge": null,
               "amount": 1,
               "usedAt": null,
               "useCanceledAt": null,
               "canceledAt": null,
               "usedLocationCode": null,
               "usedLocation": null,
               "refundStartedAt": null,
               "refundFinishedAt": null,
               "refundRate": null,
               "refundPrice": null,
               "refundBank": null,
               "refundDepositor": null,
               "refundAccount": null,
               "expiryAt": "2018-05-01T00:00:00.000Z",
               "extendExpiryDayCount": 0,
               "recentExtendExpiryDayAt": null,
               "reTransferCount": 0,
               "createdAt": 1511319474024693,
               "updatedAt": 1511319474024730,
               "deletedAt": null,
               "id": 1640
            }
         ],
         "promotion": null,
         "productPromotion": null,
         "id": 1365,
         "userId": null,
         "nonmemberId": 3,
         "serviceId": 72,
         "productId": 1228,
         "promotionId": null,
         "productPromotionId": null,
         "themeId": null,
         "state": "purchase",
         "type": "phone",
         "sendMethod": "sms",
         "charge": 10,
         "distributionCharge": null,
         "customerPrice": 1000,
         "supplyPrice": 900,
         "realPrice": 1000,
         "vatType": "include",
         "message": null,
         "purchasedAt": "2017-11-22T02:57:54.000Z",
         "purchaseCode": "1511319473893039",
         "purchaseAuthorizationCode": "837",
         "tno": "4343",
         "cash_no": null,
         "cash_authno": null,
         "externalCode": "1511319473893039",
         "canceledAt": null,
         "cancelCode": null,
         "cancelAuthorizationCode": null,
         "canCancelAt": "2018-05-01T00:00:00.000Z",
         "createdAt": 1511319474010783,
         "updatedAt": 1511319477305852,
         "deletedAt": null
      }
   ]
}
```

### 3. post
#### 구매 생성

> * purchase를 생성 후 productTransfer도 같이 생성한다.

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| nonmemberId | |  false | 비회원아이디 |
| serviceId | |  false | 서비스 id |
| productId | |  false | 상품 id |
| promotionId | |  false | 프로모션 id |
| themeId | |  false | 테마 id |
| type | |  false | 구매 유형 |
| destinations | |  false | 수신인 |
| amounts | |  false | 구매 수량 |
| charge | |  false | 수수료 |
| customerPrice | |  false | 소비자가격 |
| supplyPrice | |  false | 공급가격 |
| realPrice | |  false | 실판매가격 |
| vatType | |  false | vat 포함 유무 |
| message | |  false | 메세지 첨부 |
| productServiceId | |  false | 해당 상품의 서비스 ID |
 

#### 응답 코드 : 201


 <br /><br /><br /><br />

### 4. delete
#### 구매 제거

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | Int | true | 구매 아이디 | 

#### 응답 코드 : 200


 <br /><br /><br /><br />

