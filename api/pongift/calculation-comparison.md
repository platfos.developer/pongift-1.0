# calculation-comparison

## /api/pongift/calculation-comparison

### 1. post
 #### 정산 생성

> * post.validate() : 유효성검사

> * post.hasAuthorization() : AppUserManage 권한조회

> * post.setParam() : AppCalculation 테이블 로우


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| | | |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{

}

```

### 2. delete
 #### 제거

> * 

> * 

> * 

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />


#### 응답 예제 (JSON)  

```json
{

}

```
