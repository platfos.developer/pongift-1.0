# corporations

## /api/pongift/corporations

### 1. get
#### 법인정보 단일 얻기

> * 권한 roleUltraAdmin


#### 요청 파라미터
없음


#### 응답 파라미터

[AppCorporations 테이블 참고](/markdown/db/AppCorporations.md)

<br /><br /><br /><br />

### 2. gets
#### 법인 조회

> * 옵션조건에 따라 법인 정보 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string | false | 검색할 내용 |
| searchField | enum | false | 검색할 필드 id, number, name, condition, category |
| orderBy | enum | false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum | true | 정렬 방식 DESC, ASC |
| last | microtime | false | 조회 기준 데이터 일자 |
| size | int | false | 가져올 데이터 갯수 |
| type | enum | false | 법인 유형 corporation, personal |
| state | enum | false | 현재 상태 standby, contract, cancel |


#### 응답 파라미터

[AppCorporations 테이블 참고](/markdown/db/AppCorporations.md)

<br /><br /><br /><br />

### 3. post
#### 법인 생성

> * 권한 (roleUltraAdmin)

> * createUser: signUpType에 따라 이메일/일반아이디로 계정 생성

> * createCorporation: 법인 정보 생성

> * createUserManage: userManager 브릿지 추가


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int |  false | 법인 id |
| type | enum | true | 법인 유형 corporation, personal |
| number | int | true | 사업자 번호 |
| name | string | true | 법인명 |
| address | string | false | 업체 주소 |
| leader | string | false | 대표 |
| condition | string | false | 업태 |
| category | string | false | 업종 |
| logoImgId | int | false | 로고 이미지 id |
| state | enum | true | 현재 상태 standby, contract, cancel |
| contractedAt | datetime | false | 계약일 |
| canceledAt | datetime | false | 해지일 |
| country | enum | true | 국가코드 kr |
| signUpType | enum | false | 가입 형태(제공자타입) email, phone, social, phoneId, normalId, phoneEmail, authCi |
| uid | int | false | 아이디, 그냥 아이디로만 가입할땐 일반 아이디, 이메일 가입이면 이메일, 전화번호가입이면 전화번호 |
| nick | string | false | 닉네임 |
| secret | string | false | 비밀번호 혹은 엑세스토큰, 전화번호가입이면 인증번호 |


#### 응답 파라미터

생성된 데이터 객체 리턴

[AppCorporations 테이블 참조](/markdown/db/AppCorporations.md)

<br /><br /><br /><br />

### 4. put
#### 법인 수정

> * 권한 roleUltraAdmin 또는 roleSuperAdmin && 본인 법인정보

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int | false | 법인 id |
| type | | enum | 법인 유형 corporation, personal |
| number | int | false | 사업자 번호 |
| name | string | false | 법인명 |
| address | string | false | 업체 주소 |
| leader | string | false | 대표 |
| condition | string |  false | 업태 |
| category | string |  false | 업종 |
| logoImgId | int |  false | 로고 이미지 id |
| state | enum | false | 현재 상태 standby, contract, cancel |
| contractedAt | datetime | false | 계약일 |
| canceledAt | datetime | false | 해지일 |
| country | enum |  false | 국가코드 kr |


#### 응답 파라미터

수정된 데이터 객체 리턴

[AppCorporations 테이블 참조](/markdown/db/AppCorporations.md)

<br /><br /><br /><br />

### 5. delete
#### 법인 제거

> * updateUserRole: 유저 권한을 roleUser로 업데이트

> * deleteUserManage: UserManage 테이블에서 해당하는 법인 로우 삭제

> * deleteImage: 법인 이미지 삭제

> * deleteCorporation: 법인정보 삭제

#### 요청 파라미터
없음

#### 응답 코드 204

<br /><br /><br /><br />

