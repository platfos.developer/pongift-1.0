# adjustments

## /api/pongift/adjustments

### 1. get
#### 단일 얻기
> * 정산정보를 가지고 온다

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | 정산 id |

#### 응답 예제
```pre
{
   "serviceId": 72,
   "calculationId": 85,
   "transferAt": "2017-12-25T00:00:00.000Z",
   "vatType": "include",
   "state": "beforeAuthorized",
   "count": 10,
   "totalMoney": 180000,
   "cancelMoney": 0,
   "refundMoney": 0,
   "chargeMoney": 18000,
   "createdAt": 1511762372847588,
   "updatedAt": 1511762372847650,
   "deletedAt": null,
   "id": 162,
   "service": {
      "accessKey": "OGjgO2SXlZOd9oM7jMkXlg==",
      "authorId": 20,
      "affiliationId": 1,
      "corporationId": 23,
      "categoryId": 47,
      "externalEnterprise": "smartInfini",
      "externalCode": null,
      "state": "use",
      "type": "supply",
      "calculationType": "used",
      "name": "스마트인피니(단품)",
      "pongiftContactId": 20,
      "local": false,
      "pgUsage": true,
      "pgCardUsage": true,
      "pgPhoneUsage": true,
      "pgAccountUsage": true,
      "paymentBank": null,
      "paymentDepositor": null,
      "paymentAccount": null,
      "customerCenterPhoneNum": null,
      "logoImgId": null,
      "distribution": null,
      "businessRange": null,
      "info": null,
      "country": "kr",
      "headerColor": null,
      "templateImageKey": null,
      "createdAt": 1501657460680494,
      "updatedAt": 1501724720959855,
      "deletedAt": null,
      "id": 72,
      "corporation": {
         "authorId": 20,
         "corporationId": null,
         "type": "corporation",
         "number": "123-55-21234",
         "name": "스마트테스트",
         "address": null,
         "leader": "스마트테스트",
         "condition": null,
         "category": null,
         "logoImgId": null,
         "state": "contract",
         "contractedAt": null,
         "canceledAt": null,
         "country": "kr",
         "createdAt": 1499044877768209,
         "updatedAt": 1499386682081648,
         "deletedAt": null,
         "id": 23
      }
   },
   "calculation": {
      "authorId": 20,
      "serviceId": 72,
      "chargeState": "supply",
      "chargeType": "chargeTypeTotal",
      "charge": 5,
      "period": "onceAMonth",
      "startDay": 25,
      "transferDay": 1,
      "bookAt": "2017-08-02T07:04:20.000Z",
      "createdAt": 1501657460743132,
      "updatedAt": 1501657460743175,
      "deletedAt": null,
      "id": 85
   }
}
```

 <br /><br /><br /><br />

### 2. gets
#### 정산 조회

> * 옵션에 따라 정산정보를 조회한다.
> * 옵션 중 파일다운로드 값(fileDownload)이 참일 경우 csv파일 다운로드 로직이 포함돼있다.

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | varchar | false | 서비스ID or 서비스명 like 검색 키워드 | 
| last | bigint | false | Limit offset에 사용될 microtime 값 | 
| size | int |  false | limit 값 | 
| sort | varchar |  false  | 정렬값 | 
| orderBy | varchar |  false | 정렬 컬럼 | 
| state | enum |  false | 정산 테이블 상태 |
| serviceType | enum |  false | 서비스 유형 supply, distribution, etc |
| calculationType | enum |  false | 정산 기준 purchased, used |
| calculationPeriod | int |  false | 정산 주기 onceAMonth, twiceAMonth |
| vatType | enum |  false | vat 여부 separate, include |
| corporationId | int |  false | 법인 id |
| serviceId | int |  false | 서비스 id |
| fileDownload | int |  false | csv 파일 다운로드 여부 |

### 3. put
#### 수정

> * 입력받은 정산id들에 해당하는 row를 입력받은 state 값으로 업데이트 한다.

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| state | enum | false | 정산 테이블 유형 beforeAuthorized, beforeTransfer, complete |
| adjustmentIds | int | false | 적용시킬 정산 Ids |

 <br /><br /><br /><br />

