# affiliations

## /api/pongift/affiliations

### 1. get
#### 제휴사 조회 (단일 얻기)

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | id |

<br /><br /><br /><br />

### 2. gets
#### 제휴사 조회

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | string |  false | 검색할 필드 id, name |
| orderBy | string |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | sting |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |

<br /><br /><br /><br />

### 3. post
#### 제휴사 생성

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| name | string | false | 제휴사명 |

 <br /><br /><br /><br />

### 4. put
#### 제휴사 수정

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| name | string | false | 제휴사명 |
| id | int | true | id |


 <br /><br /><br /><br />

### 5. delete
#### 제휴사 제거

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | id |

