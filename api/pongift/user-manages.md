# user-manages

## /api/pongift/user-manages

### 1. get
 #### 단일 얻기

> * get.validate() : 유효성검사

> * get.setParam() : AppUserManage테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: |
| id | int | true | AppUserManageId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppUserManage | AppUserManages객체 |
 | AppCorporation | AppCorporations객체 |
 | AppService | AppServices객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{

}

```
