# product-themes

## /api/pongift/product-themes

### 1. get
#### 상품 기념일 브릿지 단일 조회

#### 응답 파라미터

[AppProductThemes 테이블 참고](/markdown/db/AppProductThemes.md)

<br /><br /><br /><br />

### 2. gets
#### 검색조건에 따라 상품 기념일 브릿지 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | |  false | 검색할 내용 |
| searchField | |  false | 검색할 필드 id |
| orderBy | |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | |  true  | 정렬 방식 DESC, ASC |
| last | |  false | 조회 기준 데이터 일자 |
| size | |  false | 가져올 데이터 갯수 |
| productId | |  false | 상품 id |
| themeId | |  false | 테마 id |
| corporationId | |  false | 법인 id |
| serviceId | |  false | 서비스 id |


#### 응답 파라미터

[AppProductThemes 테이블 참고](/markdown/db/AppProductThemes.md)

<br /><br /><br /><br />

### 3. post
#### 상품 기념일 브릿지 생성

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int | true | 상품 id |
| themeId | int | true | 기념일 id |

#### 응답 파라미터

[AppProductThemes 테이블 참고](/markdown/db/AppProductThemes.md)

<br /><br /><br /><br />

### 4. put
#### 상품 기념일 브릿지 수정

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int | true | 상품 id |
| themeId | int | true | 기념일 id |

#### 응답 파라미터

[AppProductThemes 테이블 참고](/markdown/db/AppProductThemes.md)

<br /><br /><br /><br />

### 5. delete
#### 상품 기념일 브릿지 제거

#### 응답 코드 204

<br /><br /><br /><br />

