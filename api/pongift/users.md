# users

## /api/pongift/users

### 1. gets
 #### 유저조회

> * gets.validate() : 유효성검사

> * gets.getTotal() : AppUserManage테이블 조회

> * gets.setParam() : 조건에 맞는 AppUserManage테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | enum |  false | userid / nick / email / phone |
| role | string |  false | 사용자 role |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | int |  false | 마지막 유저 id |
| size | int |  false | 가져올 데이터 갯수 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | rows | Users객체 |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
   "rows": [
      {
         "id": 163,
         "aid": "cultureland2@pongift.com",
         "email": "cultureland2@pongift.com",
         "nick": "컬쳐랜드(벌크서비스)",
         "role": "roleD",
         "phoneNum": null,
         "createdAt": 1506763743004254,
         "type": "supply"
      },
      {
         "id": 162,
         "aid": "cultureland@pongift.com",
         "email": "cultureland@pongift.com",
         "nick": "컬쳐랜드(벌크)",
         "role": "roleE",
         "phoneNum": null,
         "createdAt": 1506763396975795,
         "type": null
      },
      {
         "id": 157,
         "aid": "smartInfini@pongift.com",
         "email": "smartInfini@pongift.com",
         "nick": "smartInfini",
         "role": "roleD",
         "phoneNum": null,
         "createdAt": 1506310248018365,
         "type": "supply"
      },
      {
         "id": 15,
         "aid": "index@pongift.com",
         "email": "index@pongift.com",
         "nick": "지수",
         "role": "roleB",
         "phoneNum": "+821029923273",
         "createdAt": 1487039900289623,
         "type": null
      },
      {
         "id": 2,
         "aid": "admin@pongift.com",
         "email": "admin@pongift.com",
         "nick": "pongift",
         "role": "roleF",
         "phoneNum": null,
         "createdAt": 1484730154064418,
         "type": null
      }
   ],
   "count": 5
}
```



### 2. put
 #### 회원정보수정

> * put.validate() : 유효성검사

> * put.findUser() : User조회

> * put.changePassword() : 비미런호변경

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| aid | string |  false | 회원계정 |
| nick | string |  false | 닉네임 |
| email | string |  false | 이메일 |
| newPass | string |  false | 새로운 비밀번호 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | User | Users객체 |

 <br /><br /><br /><br />

