# images

## /api/pongift/images

### 1. get
 #### 이미지 단일 조회

> * get.validate() : 유효성 검사

> * get.setInclude() : Image테이블 설정값

> * get.setParam() : Image테이블 로우조회


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | ImageId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
```json
{
   "authorId": 8,
   "folder": "promotion",
   "dateFolder": "2016-12-13",
   "name": "upload_a382b9e694c67ec59a281f85ce8cb682.jpg",
   "authorized": true,
   "createdAt": 1476177297328768,
   "updatedAt": 1476177297328811,
   "deletedAt": null,
   "id": 25,
   "promotion1": {
      "authorId": 1,
      "eventId": null,
      "serviceId": null,
      "corporationId": null,
      "type": "present",
      "serviceType": "total",
      "productType": "cost",
      "name": "플랫포스 창사기념 특선",
      "memo": "테스트 입니다.",
      "totalPurchaseCount": 9999,
      "purchaseCountTotal": null,
      "purchaseCountAMonth": 30,
      "purchaseCountADay": 1,
      "totalPurchaseAmount": null,
      "purchaseAmountTotal": null,
      "purchaseAmountAMonth": null,
      "purchaseAmountADay": null,
      "purchaseAmountATry": null,
      "totalPurchasePrice": 2147483647,
      "purchasePriceTotal": null,
      "purchasePriceAMonth": 300000,
      "purchasePriceADay": 100000,
      "purchasePriceATry": 10000,
      "startAt": "2016-10-11T15:00:00.000Z",
      "endAt": "2016-10-12T15:00:00.000Z",
      "sdkBannerImgId1": 25,
      "sdkBannerImgId2": null,
      "webBannerImgId1": 26,
      "webBannerImgId2": null,
      "visibility": false,
      "createdAt": 1476177301733488,
      "updatedAt": 1477631263264130,
      "deletedAt": null,
      "id": 3
   },
   "promotion2": null,
   "promotion3": null,
   "promotion4": null
}

```

