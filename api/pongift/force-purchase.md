# force-purchase

## /api/pongift/force-purchase

### 1. get
#### 스토어팜 수신자 환불 

> * 스마트인피니, 옴니텔 등 쿠폰 공급사에 쿠폰 취소요청

> * 정산정보 업데이트

> * 쿠폰전송 정보(productTransfer) 업데이트

> * 스토어팜 환불 승인 처리

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseId | int |  false | 구매 id |
| destination | string |  false | 수신자 번호 |
| state | enum |  false | 상태 disuse, use, partial, cancel, refundStart, refundFinish |
| refundRate | int |  false | 환불률 |
| refundPrice | int |  false | 환불금액 |
| refundBank | string |  false | 환불계좌은행 |
| refundDepositor | string |  false | 환불계좌예금주 |
| refundAccount | string |  false | 환불계좌번호 |
| refundStartedAt | datetime | false | 환불승인일자 |
| refundFinishedAt | datetime |  false | 환불완료일자 |
 
#### 응답 파라미터

[AppProductTransfer 테이블 참고](/markdown/db/AppProductTransfers.md)

