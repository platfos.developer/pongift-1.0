# calculations

## /api/pongift/calculations

### 1. get
#### 단일 얻기

> * 입력한 id에 해당하는 정보를 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | 고유 id |


<br /><br /><br /><br />

### 2. gets
#### 정산 조회

> * 옵션에 따라 정산정보를 조회

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | string |  false | 검색할 필드 id |
| orderBy | string |  false | 정렬 기준 createdAt, updatedAt, bookAt |
| sort | string |  true  | 정렬 방식DESC, ASC |
| last | microtime |  false | 검색 기준 마지막 데이타 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| serviceId | int |  false | 서비스 id |
 
<br /><br /><br /><br />

### 3. post
#### 정산 생성

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스 id |
| chargeState | string |  false | 수수료 상태 supply, distribution |
| chargeType | string |  false | 수수료 유형 chargeTypeTotal, chargeTypeSeparate |
| charge | int | false | 수수료 (%) |
| period | enum | false | 정산주기 onceAMonth, twiceAMonth |
| startDay | date | false | 정산 시작일 |
| transferDay | date | false | 입금일 (D + N일) |
| bookAt | date |  false | 예약 적용일 |

 <br /><br /><br /><br />

### 4. put
#### 정산 수정

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스 id |
| chargeState | enum |  false | 수수료 상태 supply, distribution |
| chargeType | enum |  false | 수수료 유형 chargeTypeTotal, chargeTypeSeparate |
| charge | int |  false | 수수료 (%) |
| period | enum |  false | 정산주기 onceAMonth, twiceAMonth |
| startDay | date |  false | 정산 시작일 |
| transferDay | date |  false | 입금일 (D + N일) |
| bookAt | date |  false | 예약 적용일 |

 <br /><br /><br /><br />

### 5. delete
#### 제거

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | 정산 id |

 <br /><br /><br /><br />

