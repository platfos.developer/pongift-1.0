# product-images

## /api/pongift/product-images

### 1. get
#### 상품이미지 단일 조회

#### 응답 파라미터

[AppProductImages 테이블 참고](/markdown/db/AppProductImages.md)

<br /><br /><br /><br />

### 2. gets
#### 옵션에 따라 상품 이미지 브릿지 조회 

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt, index |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 데이터 |
| size | int |  false | 가져올 데이터 갯수 |
| productId | int |  false | 상품 id |


#### 응답 파라미터

[AppProductImages 테이블 참고](/markdown/db/AppProductImages.md)

<br /><br /><br /><br />

### 3. post
#### 상품 이미지 브릿지 생성

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int |  true | 상품 id |
| imageId | int |  true | 이미지 id |
| index | int |  true | 이미지 순서 |


#### 응답 파라미터

[AppProductImages 테이블 참고](/markdown/db/AppProductImages.md)

<br /><br /><br /><br />

### 4. put
#### 상품 이미지 브릿지 수정

> * 이미지 정보 업데이트 후 기존이미지 삭제

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int |  false | 상품 id |
| imageId | int |  false | 이미지 id |
| index | int |  false | 이미지 순서 |


#### 응답 파라미터

[AppProductImages 테이블 참고](/markdown/db/AppProductImages.md)

<br /><br /><br /><br />

### 5. delete
#### 상품 이미지 브릿지 제거

#### 응답 코드 204



<br /><br /><br /><br />

