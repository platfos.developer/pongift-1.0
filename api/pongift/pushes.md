# pushes

## /api/pongift/pushes

### 1. get
 #### 푸쉬 단일조회

> * get.validate() : 유효성검사

> * get.setParam() : 조건에 맞는 AppPush테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: |
| id | int | true | AppPushId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPush | AppPush객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "themeId": 3,
   "name": "테마2",
   "visibility": false,
   "pushAtType": "designation",
   "repeat": "years",
   "pushAt": "2017-02-14T03:20:00.000Z",
   "message": "테마2입니다.",
   "createdAt": 1487042091200624,
   "updatedAt": 1487042118825591,
   "deletedAt": null,
   "id": 15
}

```

### 2. gets
 #### 푸시 조회

> * gets.validate() : 유효성 검사

> * gets.setParam() : 조건에 맞는 AppPush테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum | false | 검색할 필드 id, name |
| orderBy | enum |  false | 정렬 기준 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microTimestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| noSize |  boolean|  false | 가져올 데이터 갯수 유무 |
| visibility | boolean |  false | 푸시 사용 유무 |
| pushAtType | enum |  false | 날짜 지정 여부 birth, wedding, designation |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPush | AppPush [ ] |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
   "count": 2,
   "list": [
      {
         "authorId": 20,
         "themeId": 3,
         "name": "테마2",
         "visibility": false,
         "pushAtType": "designation",
         "repeat": "years",
         "pushAt": "2017-02-14T03:20:00.000Z",
         "message": "테마2입니다.",
         "createdAt": 1487042091200624,
         "updatedAt": 1487042118825591,
         "deletedAt": null,
         "id": 15,
         "theme": {
            "authorId": 20,
            "iconImgId": 529,
            "type": "personal",
            "name": "선물",
            "visibility": true,
            "createdAt": 1476350255180859,
            "updatedAt": 1490586615266572,
            "deletedAt": null,
            "id": 3
         }
      },
      {
         "authorId": 91,
         "themeId": 2,
         "name": "PUSH TEST (빼빼로 데이)",
         "visibility": true,
         "pushAtType": "birth",
         "repeat": "years",
         "pushAt": null,
         "message": "오늘은 빼빼로데이!! 연인에게 선물을 보내세요.",
         "createdAt": 1480058031805397,
         "updatedAt": 1490233619384727,
         "deletedAt": null,
         "id": 14,
         "theme": {
            "authorId": 1,
            "iconImgId": 57,
            "type": "personal",
            "name": "결혼",
            "visibility": true,
            "createdAt": 1476350255180858,
            "updatedAt": 1489460124369860,
            "deletedAt": null,
            "id": 2
         }
      }
   ]
}

```


### 3. post
 #### 푸시 생성

> * post.validate() : 유효성검사

> * post.setParam() : AppPush테이블 로우생성

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| name | string |  false | 푸시명 |
| themeId | int |  false | 테마 id |
| pushAtType | enum |  false | 날짜 지정 여부 birth, wedding, designation |
| repeat | enum |  false | 반복 여부 years, once |
| pushAt | date |  false | 푸시 날짜 |
| message | string |  false | 푸시 내용 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPush | AppPush 객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "themeId": 3,
   "name": "push",
   "visibility": false,
   "pushAtType": "designation",
   "repeat": "once",
   "pushAt": "2017-12-11T00:00:00.000Z",
   "message": "테스트 푸시알림",
   "createdAt": 1512961895691596,
   "updatedAt": 1512961895691655,
   "deletedAt": null,
   "id": 20,
   "theme": {
      "authorId": 20,
      "iconImgId": 529,
      "type": "personal",
      "name": "선물",
      "visibility": true,
      "createdAt": 1476350255180859,
      "updatedAt": 1490586615266572,
      "deletedAt": null,
      "id": 3
   }
}

```

### 4. put
 #### 푸시 수정

> * put.validate() : 유효성검사

> * put.updatePush() : AppPush

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| themeId | int |  false | 테마 id |
| name | string |  false | 푸시명 |
| visibility | boolean |  false | 사용 여부 |
| pushAtType | enum |  false | 날짜 지정 여부 birth, wedding, designation |
| repeat | enum |  false | 반복 여부 years, once |
| pushAt | date |  false | 푸시 날짜 |
| message | string |  false | 푸시 내용 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPush | AppPush 객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "themeId": 3,
   "name": "push",
   "visibility": false,
   "pushAtType": "designation",
   "repeat": "once",
   "pushAt": "2017-12-11T00:00:00.000Z",
   "message": "테스트알림 수정",
   "createdAt": 1512961895691596,
   "updatedAt": 1512962069368522,
   "deletedAt": null,
   "id": 20,
   "theme": {
      "authorId": 20,
      "iconImgId": 529,
      "type": "personal",
      "name": "선물",
      "visibility": true,
      "createdAt": 1476350255180859,
      "updatedAt": 1490586615266572,
      "deletedAt": null,
      "id": 3
   }
}
```

### 5. delete
 #### 푸시 제거

> * del.validate() : 유효성검사

> * del.destroy() : AppPush테이블 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppPushId | 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

