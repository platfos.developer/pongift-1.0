# products

## /api/pongift/products

### 1. get
#### 상품 정보 단일 조회

#### 응답 파라미터

[AppProducts 테이블 참고](/markdown/db/AppProducts.md)

<br /><br /><br /><br />

### 2. gets
#### 상품 정보 조회

> * 검색조건에 따라 상품정보 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id, name |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| corporationId | int |  false | 법인 id |
| serviceId | int |  false | 서비스 id |
| serviceName | string |  false | 서비스명 |
| categoryId | int |  false | 카테고리 id |
| state | enum |  false | 현재 상태 authorized, unauthorized |
| type | enum |  false | 상품 유형 exchange, cost |
| noSize | |  false | limit 적용 유무 |
| productId | int |  false | 상품 id |
| corporationState | enum |  false | 법인 상태 standby, contract, cancel |
| serviceState | enum |  false | 서비스 상태 standby, use, close |
| serviceItem | string |  false | 서비스 검색어 |
| weight | enum |  false | 정렬기준 가중치 (true / false) |
| offset | int |  false | offset |
| isStoreFarm | boolean |  false | 스토어팜 사용 여부 |


#### 요청 파라미터

[AppProducts 테이블 참고](/markdown/db/AppProducts.md)

<br /><br /><br /><br />

### 3. post
#### 상품 생성

> * 권한 (roleAdmin 이상 && (유저id == 상품의 법인 id || 서비스 id) )

> * 예약일 체크

> * 상품정보 생성

> * mms 상품이미지 생성

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int | true | 서비스 id |
| categoryId | int |  false | 카테고리 id |
| type | enum |  true | 상품 유형 exchange, cost |
| state | enum |  false | 상품 상태 authorized, unauthorized |
| weight | int |  false | 상품 노출 가중치 |
| externalCode | string |  false | 외부 코드 |
| name | string |  true | 상품명 |
| info1 | string |  false | 교환처 |
| info2 | string |  false | 상품정보 |
| info3 | string |  false | 제한사항 |
| info4 | string |  false | 주의사항 |
| info5 | string |  false | 안내사항 |
| expiryDay | int |  true | 유효기간 (발급일로부터) |
| productImageIds | int |  false | 상품 이미지 ids |
| charge | int |  true | 수수료(%) |
| customerPrice | int |  true | 소비자가격 |
| supplyPrice | int |  true | 공급가격 |
| realPrice | int |  true | 실거래가격 |
| vatType | enum |  true | VAT 포함여부 separate, include |
| storeFarmType | boolean |  false | 스토어팜 상품 flag |
| productOptionDatas | string |  false | 스마트인피니 json데이터 |
| optionName | |  false |  | 
| endDay | |  false | 스마트인피니 사용기간 |


#### 응답 파라미터

[AppProducts 테이블 참고](/markdown/db/AppProducts.md)

<br /><br /><br /><br />

### 4. put
#### 상품 수정

> * 권한 (roleAdmin 이상 && (유저id == 상품의 법인 id || 서비스 id) )

> * 상품 정보를 수정하고 입력값에따라 스토어팜에 정보변경 요청

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int |  false | 서비스 id |
| categoryId | int |  false | 카테고리 id |
| state | enum |  false | 상품 인증 상태 authorized, unauthorized |
| weight | int |  false | 상품 노출 가중치 |
| type | enum |  false | 상품 유형 exchange, cost |
| externalCode | string |  false | 외부 코드 |
| name | string |  false | 상품명 |
| tax | enum |  false | 상품 과세 여부 |
| info1 | string |  false | 교환처 |
| info2 | string |  false | 상품정보 |
| info3 | string |  false | 제한사항 |
| info4 | string |  false | 주의사항 |
| info5 | string |  false | 안내사항 |
| expiryDay | int |  false | 유효기간 (발급일로부터) |
| productImageIds | string |  false | 상품 이미지 ids |
| deleteImageIds | string |  false | 삭제할 이미지 ids |
| charge | string |  false | 수수료(%) |
| customerPrice | int |  false | 소비자가격 |
| supplyPrice | int |  false | 공급가격 |
| realPrice | int |  false | 실거래가격 |
| vatType | enum |  false | VAT 포함여부 separate, include |
| bookAt | datetime |  false | 예약일 |
| deleteProductInfoIds | string |  false | 삭제할 상품 정보 ids |
| storeFarmType | enum |  false | 스토어팜 상품 flag |
| storeFarmCategory | int |  false | 스토어팜 상품 카테고리 |
| storeFarmProductOrderMax | int |  false | 스토어팜 상품 1회 갯수 |
| productOptionDatas | string |  false | 스마트인피니 json데이터 |
| optionName | |  false |  | 
| endDay | |  false | 스마트인피니 사용기간 |


#### 응답 파라미터

[AppProducts 테이블 참고](/markdown/db/AppProducts.md)

<br /><br /><br /><br />

### 5. delete
#### 상품 제거

> * 권한 (roleAdmin 이상 && (유저id == 상품의 법인 id || 서비스 id) )

> * 상품이미지 삭제

> * 스토어팜 상품 삭제

#### 응답코드 204
 
<br /><br /><br /><br />

