# sdk-initial

## /api/pongift/sdk-initial

### 1. get
#### 앱을 초기 구동 시킬 때 서비스를 얻어오는 정보

> * 

> * 

> * 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| accessKey | |  false | 서비스에서 부여받은 아이디 |
| secretKey | |  false |  | 


#### Return

| parameter | desc |
| --------- | :---- |

<br /><br /><br /><br />

### 2. post
#### 앱을 초기 구동 시킬 때 서비스를 얻어오는 정보

> * 

> * 

> * 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| accessKey | |  false | 서비스에서 부여받은 아이디 |
| secretKey | |  false |  | 


#### Return

| parameter | desc |
| --------- | :---- |

<br /><br /><br /><br />

### 3. put
#### 토큰 갱신

> * 

> * 

> * 

#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 


#### Return

| parameter | desc |
| --------- | :---- |

<br /><br /><br /><br />

