# contacts

## /api/pongift/contacts

### 1. get
#### 단일 얻기

> * 권한 (roleAdmin 이상)
> * 담당자 정보를 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | 담당자 고유 id |

#### 응답 파라미터

[AppContacts 테이블 참고](/markdown/db/AppContacts.md)


<br /><br /><br /><br />

### 2. gets
#### 담당자 조회

> * 권한 (roleAdmin 이상)
> * 옵션조건에 따라 담당자 정보를 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string | false | 검색할 내용 |
| searchField | string | false | 검색할 db 필드 |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| serviceId | int |  false | 서비스 id |


#### 응답 파라미터

[AppContacts 테이블 참고](/markdown/db/AppContacts.md)

<br /><br /><br /><br />

### 3. post
#### 담당자 생성

> * 권한 (roleAdmin 이상)

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int | true | 서비스 id |
| type | enum | false | 담당자 유형 |
| part | string | false | 담당자 부서 |
| position | string | false | 담당자 직급 |
| name | string | false | 담당자 성명 |
| telephoneNum | string | false | 담당자 연락처 |
| cellphoneNum | string | false | 담당자 휴대전화 연락처 |
 

#### 응답 코드 200

<br /><br /><br /><br />

### 4. put
#### 담당자 수정

> * 권한 (roleAdmin 이상)

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| serviceId | int | false | 서비스 id |
| type | enum | false | 담당자 유형 |
| part | string | false | 담당자 부서 |
| position | string | false | 담당자 직급 |
| name | string | false | 담당자 성명 |
| telephoneNum | string | false | 담당자 연락처 |
| cellphoneNum | string | false | 담당자 휴대전화 연락처 |
 

#### 응답 코드 200

| parameter | desc |
| --------- | :---- |

<br /><br /><br /><br />

### 5. delete
#### 담당자 제거

> * 권한 (roleAdmin 이상)

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | 삭제할 담당자 id |

#### 응답 코드 204

<br /><br /><br /><br />

