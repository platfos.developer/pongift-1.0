# promotion-visibility

## /api/pongift/promotion-visibility

### 1. put
#### 프로모션 visibility 수정

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| visibility | boolean | true | 프로모션 visibility |


#### 응답 파라미터

[AppPromotions 테이블 참고](/markdown/db/AppPromotions.md)

<br /><br /><br /><br />

