# product-infos

## /api/pongift/product-infos

### 1. get
#### 상품 금액에 대한 정보 단일 조회

#### 응답 파라미터

[AppProductInfos 테이블 참고](/markdown/db/AppProductInfos.md)

<br /><br /><br /><br />

### 2. gets
#### 검색 조건에 따라 상품 금액에 대한 정보 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | string |  false | 검색할 필드 id |
| orderBy | enum |  false | 정렬 기준 createdAt, updatedAt, bookAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 마지막 데이터 기준 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| productId | int |  false | 상품 id |


#### 응답 파라미터

[AppProductInfos 테이블 참고](/markdown/db/AppProductInfos.md)

<br /><br /><br /><br />

### 3. post
#### 상품 정보 생성

> * 권한 (roleAdmin 이상 && (유저id == 상품의 법인 id || 서비스 id) )

#### 요청 파라미터
| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int | true | 상품 id |
| charge | int | true | 수수료(%) |
| customerPrice | int | true | 소비자가격 |
| supplyPrice | int | true | 공급가격 |
| realPrice | int | true | 실거래가격 |
| vatType | int | true | VAT 포함여부 separate, include |
| bookAt | datetime| false | 예약일 |


#### 응답 파라미터

[AppProductInfos 테이블 참고](/markdown/db/AppProductInfos.md)

<br /><br /><br /><br />

### 4. put
#### 상품 정보 수정

> * 권한 (roleAdmin 이상 && (유저id == 상품의 법인 id || 서비스 id) )

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int | true | 상품 id |
| charge | int | true | 수수료(%) |
| customerPrice | int | true | 소비자가격 |
| supplyPrice | int | true | 공급가격 |
| realPrice | int | true | 실거래가격 |
| vatType | int | true | VAT 포함여부 separate, include |
| bookAt | datetime | false | 예약일 |

#### 응답 파라미터

[AppProductInfos 테이블 참고](/markdown/db/AppProductInfos.md)

<br /><br /><br /><br />

### 5. delete
#### 상품 정보 제거

> * 권한 (roleAdmin 이상 && (유저id == 상품의 법인 id || 서비스 id) )

#### 응답 코드 204

<br /><br /><br /><br />

