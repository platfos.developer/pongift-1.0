# events

## /api/pongift/events

### 1. get
 #### 단일 얻기

> * get.validate() : 유효성검사

> * get.setParam() : AppEvent테이블 로우조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppEventId |

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppEvent | AppEvent 객체 |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
   "authorId": 8,
   "name": "event",
   "visibility": true,
   "createdAt": 1488186619144969,
   "updatedAt": 1488186619692072,
   "deletedAt": null,
   "id": 1
}
```

### 2. gets
 #### 이벤트 조회

> * gets.validate() : 유효성검사

> * gets.setParam() : 조건에 맞는 AppEvent테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| last | microTimestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
   "list": [
      {
         "authorId": 8,
         "name": "event",
         "visibility": true,
         "createdAt": 1488186619144969,
         "updatedAt": 1488186619692072,
         "deletedAt": null,
         "id": 1,
         "author": null
      }
   ]
}
```

### 3. post
 #### 이벤트 생성

> * post.validate() : 유효성검사

> * post.setParam() : AppEvent테이블 로우생성

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| name | string |  false | 이벤트명 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "name": "event999",
   "visibility": false,
   "createdAt": 1512966226910205,
   "updatedAt": 1512966226910242,
   "deletedAt": null,
   "id": 3
}

```

### 4. put
 #### 이벤트 수정

> * put.validate() : 유효성검사

> * put.findPromotions() : 프로모션조회

> * put.updateEvent() : AppEvent테이블 로우업데이트

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| name | string |  false | 이벤트명 |
| visibility | boolean |  false | 이벤트 표시 유무 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppEvent | AppEvents객체 |

 <br /><br /><br /><br />
 
#### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "name": "event999",
   "visibility": false,
   "createdAt": 1512966226910205,
   "updatedAt": 1512966429415051,
   "deletedAt": null,
   "id": 3
}

```

### 5. delete
 #### 이벤트 제거

> * del.validate() : 유효성검사

> * del.findPromotions() : AppPromotion테이블 로우조회

> * del.destroy() : AppEvent테이블 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppPromotionId |


#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
 
