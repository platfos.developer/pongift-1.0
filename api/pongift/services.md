# services

## /api/pongift/services

### 1. get
 #### 단일 얻기

> * get.validate() : 유효성검사

> * get.countCalculation() : AppCalculation테이블 로우조회

> * get.setParam() : AppService테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| state | |  false | 서비스 상태 standby, use, close |
| corporationState | |  false | 법인 상태 standby, contract, cancel |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | logoImg | images객체 |
 | category | Appcategorys객체 |
 | corporation | Appcorporations객체 |
 | affiliation | Appaffiliations객체 |
 | pongiftContact | ApppongiftContacs객체 | 
 | calculations | Appcalculations객체 |
 | contacts | Appcontacts객체 |
 | userManages | AppuserManages객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "accessKey": "sRuWet95N5MZ8uW9vSkSGw==",
   "authorId": 1,
   "affiliationId": 1,
   "corporationId": 1,
   "categoryId": null,
   "externalEnterprise": null,
   "externalCode": null,
   "state": "standby",
   "type": "distribution",
   "calculationType": "purchased",
   "name": "312312",
   "pongiftContactId": 1,
   "local": false,
   "pgUsage": true,
   "pgCardUsage": false,
   "pgPhoneUsage": false,
   "pgAccountUsage": false,
   "paymentBank": null,
   "paymentDepositor": null,
   "paymentAccount": null,
   "customerCenterPhoneNum": null,
   "logoImgId": null,
   "distribution": "online",
   "businessRange": "corporation",
   "info": null,
   "country": "kr",
   "headerColor": null,
   "templateImageKey": null,
   "createdAt": 1509498727275729,
   "updatedAt": 1509498727297961,
   "deletedAt": null,
   "id": 87,
   "affiliation": {
      "authorId": 1,
      "name": "pongift",
      "createdAt": 1475995528881034,
      "updatedAt": 1475995528881034,
      "deletedAt": null,
      "id": 1
   },
   "corporation": {
      "authorId": 1,
      "corporationId": null,
      "type": "corporation",
      "number": "201-17-52716",
      "name": "슬로그업",
      "address": "서울 용산구 청파동 47길",
      "leader": "이화랑",
      "condition": "서비스업",
      "category": "소프트웨어",
      "logoImgId": 298,
      "state": "contract",
      "contractedAt": "2016-10-15T15:00:00.000Z",
      "canceledAt": null,
      "country": "kr",
      "createdAt": 1475995528881034,
      "updatedAt": 1484546757687247,
      "deletedAt": null,
      "id": 1,
      "logoImg": {
         "authorId": 1,
         "folder": "corporation",
         "dateFolder": "2016-12-13",
         "name": "upload_a526dd7c68c2a6f8ea1eed981f304edd.png",
         "authorized": true,
         "createdAt": 1479618484371349,
         "updatedAt": 1479776733708153,
         "deletedAt": null,
         "id": 298
      }
   },
   "category": null,
   "logoImg": null,
   "pongiftContact": {
      "aid": "dusrn@slogup.com",
      "email": "dusrn@slogup.com",
      "secret": "8eS0Euk5FHy/OdHXHH+z33c0xNH05LEunUsZ18XB7J/WPZpTze57q5neJ4oPsVjbVRxF7nNnZcsxgy5ee1Js2A==",
      "salt": "KFzVG37h8n6vxOS825jYnA==",
      "phoneNum": "+821092185131",
      "name": "김종엽",
      "nick": "dusrn",
      "role": "roleS",
      "gender": null,
      "birth": null,
      "isVerifiedEmail": false,
      "country": "KR",
      "language": "ko",
      "isReviewed": false,
      "agreedEmail": true,
      "agreedPhoneNum": true,
      "agreedTermsAt": null,
      "profileId": 1,
      "di": null,
      "ci": null,
      "createdAt": 1475995092714968,
      "updatedAt": 1488855458740117,
      "deletedAt": null,
      "passUpdatedAt": 1488855458737335,
      "id": 1
   },
   "calculations": [
      {
         "authorId": 1,
         "serviceId": 87,
         "chargeState": "distribution",
         "chargeType": "chargeTypeTotal",
         "charge": 100,
         "period": "onceAMonth",
         "startDay": 1,
         "transferDay": 1,
         "bookAt": "2017-11-01T01:12:07.000Z",
         "createdAt": 1509498727306670,
         "updatedAt": 1509498727306699,
         "deletedAt": null,
         "id": 100
      }
   ],
   "contacts": [
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "affiliation",
         "part": null,
         "position": null,
         "name": "1",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315215,
         "updatedAt": 1509498727315246,
         "id": 292
      },
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "calculation",
         "part": null,
         "position": null,
         "name": "2",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315385,
         "updatedAt": 1509498727315393,
         "id": 293
      },
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "development",
         "part": null,
         "position": null,
         "name": "3",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315458,
         "updatedAt": 1509498727315464,
         "id": 294
      }
   ],
   "userManages": [
      {
         "authorId": 1,
         "userId": 176,
         "corporationId": null,
         "serviceId": 87,
         "createdAt": 1509498727362057,
         "updatedAt": 1509498727362088,
         "id": 75,
         "user": {
            "aid": "asdfasdf3",
            "email": null,
            "secret": "ECZxDIuvz9c8KzKOI2QpVytyxoqQZE0MUAs6YJDRsZIywrVPexSrMiDLE9KqaH+cdYSEqdx5WCMvbyZtpVzZEQ==",
            "salt": "KFZn7PAP94UMpYDazIJZfQ==",
            "phoneNum": null,
            "name": null,
            "nick": "fasdfaf23",
            "role": "roleD",
            "gender": null,
            "birth": null,
            "isVerifiedEmail": false,
            "country": "kr",
            "language": "ko",
            "isReviewed": false,
            "agreedEmail": true,
            "agreedPhoneNum": true,
            "agreedTermsAt": null,
            "profileId": 176,
            "di": null,
            "ci": null,
            "createdAt": 1509498727353314,
            "updatedAt": 1509498727353360,
            "deletedAt": null,
            "passUpdatedAt": 1509498032436320,
            "id": 176
         }
      }
   ]
}
```

### 2. gets
 #### 서비스 조회

> * gets.validate() : 유효성검사

> * gets.setParam() : 조건에 맞는 AppService테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | |  false | 검색할 내용 |
| searchField | |  false | 검색할 필드 id, name |
| orderBy | |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | |  true  | 정렬 방식 DESC, ASC |
| last | |  false | 조회 기준 데이터 일자 |
| size | |  false | 가져올 데이터 갯수 |
| corporationId | |  false | 법인 id |
| categoryId | |  false | 카테고리 id |
| externalEnterprise | |  false | 외부 업체 omnitel, smartInfini, cultureland |
| state | |  false | 현재 상태 standby, use, close |
| type | |  false | 서비스 유형 supply, distribution, etc |
| calculationType | |  false | 정산 유형 purchased, used |
| notType | |  false | 제외할 서비스 유형 supply, distribution, etc |
| noSize | |  false | limit 적용 유무 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | logoImg | images객체 |
 | category | Appcategorys객체 |
 | corporation | Appcorporations객체 |
 | affiliation | Appaffiliations객체 |
 | pongiftContact | ApppongiftContacs객체 | 
 | calculations | Appcalculations객체 |
 | contacts | Appcontacts객체 |
 | userManages | AppuserManages객체 |
 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "count": 1,
   "list": [
      {
        "accessKey": "sRuWet95N5MZ8uW9vSkSGw==",
        "authorId": 1,
        "affiliationId": 1,
        "corporationId": 1,
        "categoryId": null,
        "externalEnterprise": null,
        "externalCode": null,
        "state": "standby",
        "type": "distribution",
        "calculationType": "purchased",
        "name": "312312",
        "pongiftContactId": 1,
        "local": false,
        "pgUsage": true,
        "pgCardUsage": false,
        "pgPhoneUsage": false,
        "pgAccountUsage": false,
        "paymentBank": null,
        "paymentDepositor": null,
        "paymentAccount": null,
        "customerCenterPhoneNum": null,
        "logoImgId": null,
        "distribution": "online",
        "businessRange": "corporation",
        "info": null,
        "country": "kr",
        "headerColor": null,
        "templateImageKey": null,
        "createdAt": 1509498727275729,
        "updatedAt": 1509498727297961,
        "deletedAt": null,
        "id": 87,
        "affiliation": {
           "authorId": 1,
           "name": "pongift",
           "createdAt": 1475995528881034,
           "updatedAt": 1475995528881034,
           "deletedAt": null,
           "id": 1
        },
        "corporation": {
           "authorId": 1,
           "corporationId": null,
           "type": "corporation",
           "number": "201-17-52716",
           "name": "슬로그업",
           "address": "서울 용산구 청파동 47길",
           "leader": "이화랑",
           "condition": "서비스업",
           "category": "소프트웨어",
           "logoImgId": 298,
           "state": "contract",
           "contractedAt": "2016-10-15T15:00:00.000Z",
           "canceledAt": null,
           "country": "kr",
           "createdAt": 1475995528881034,
           "updatedAt": 1484546757687247,
           "deletedAt": null,
           "id": 1,
           "logoImg": {
              "authorId": 1,
              "folder": "corporation",
              "dateFolder": "2016-12-13",
              "name": "upload_a526dd7c68c2a6f8ea1eed981f304edd.png",
              "authorized": true,
              "createdAt": 1479618484371349,
              "updatedAt": 1479776733708153,
              "deletedAt": null,
              "id": 298
           }
        },
        "category": null,
        "logoImg": null,
        "pongiftContact": {
           "aid": "dusrn@slogup.com",
           "email": "dusrn@slogup.com",
           "secret": "8eS0Euk5FHy/OdHXHH+z33c0xNH05LEunUsZ18XB7J/WPZpTze57q5neJ4oPsVjbVRxF7nNnZcsxgy5ee1Js2A==",
           "salt": "KFzVG37h8n6vxOS825jYnA==",
           "phoneNum": "+821092185131",
           "name": "김종엽",
           "nick": "dusrn",
           "role": "roleS",
           "gender": null,
           "birth": null,
           "isVerifiedEmail": false,
           "country": "KR",
           "language": "ko",
           "isReviewed": false,
           "agreedEmail": true,
           "agreedPhoneNum": true,
           "agreedTermsAt": null,
           "profileId": 1,
           "di": null,
           "ci": null,
           "createdAt": 1475995092714968,
           "updatedAt": 1488855458740117,
           "deletedAt": null,
           "passUpdatedAt": 1488855458737335,
           "id": 1
        },
        "calculations": [
           {
              "authorId": 1,
              "serviceId": 87,
              "chargeState": "distribution",
              "chargeType": "chargeTypeTotal",
              "charge": 100,
              "period": "onceAMonth",
              "startDay": 1,
              "transferDay": 1,
              "bookAt": "2017-11-01T01:12:07.000Z",
              "createdAt": 1509498727306670,
              "updatedAt": 1509498727306699,
              "deletedAt": null,
              "id": 100
           }
        ],
        "contacts": [
           {
              "authorId": 1,
              "serviceId": 87,
              "type": "affiliation",
              "part": null,
              "position": null,
              "name": "1",
              "telephoneNum": null,
              "cellphoneNum": null,
              "email": null,
              "createdAt": 1509498727315215,
              "updatedAt": 1509498727315246,
              "id": 292
           },
           {
              "authorId": 1,
              "serviceId": 87,
              "type": "calculation",
              "part": null,
              "position": null,
              "name": "2",
              "telephoneNum": null,
              "cellphoneNum": null,
              "email": null,
              "createdAt": 1509498727315385,
              "updatedAt": 1509498727315393,
              "id": 293
           },
           {
              "authorId": 1,
              "serviceId": 87,
              "type": "development",
              "part": null,
              "position": null,
              "name": "3",
              "telephoneNum": null,
              "cellphoneNum": null,
              "email": null,
              "createdAt": 1509498727315458,
              "updatedAt": 1509498727315464,
              "id": 294
           }
        ],
        "userManages": [
           {
              "authorId": 1,
              "userId": 176,
              "corporationId": null,
              "serviceId": 87,
              "createdAt": 1509498727362057,
              "updatedAt": 1509498727362088,
              "id": 75,
              "user": {
                 "aid": "asdfasdf3",
                 "email": null,
                 "secret": "ECZxDIuvz9c8KzKOI2QpVytyxoqQZE0MUAs6YJDRsZIywrVPexSrMiDLE9KqaH+cdYSEqdx5WCMvbyZtpVzZEQ==",
                 "salt": "KFZn7PAP94UMpYDazIJZfQ==",
                 "phoneNum": null,
                 "name": null,
                 "nick": "fasdfaf23",
                 "role": "roleD",
                 "gender": null,
                 "birth": null,
                 "isVerifiedEmail": false,
                 "country": "kr",
                 "language": "ko",
                 "isReviewed": false,
                 "agreedEmail": true,
                 "agreedPhoneNum": true,
                 "agreedTermsAt": null,
                 "profileId": 176,
                 "di": null,
                 "ci": null,
                 "createdAt": 1509498727353314,
                 "updatedAt": 1509498727353360,
                 "deletedAt": null,
                 "passUpdatedAt": 1509498032436320,
                 "id": 176
              }
           }
        ]
     }
   ]
}

```

### 3. post
 #### 서비스 생성

> * post.validate() : 유효성검사

> * post.validateContact() : 담당자 유효성검사

> * post.setParam() : AppService테이블 생성

> * post.sendEmail() : 이메일 전송

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| affiliationId | int |  false | 제휴사 id |
| corporationId | int |  false | 법인 id |
| categoryId | int |  false | 카테고리 id |
| externalEnterprise | enum |  false | 외부 업체 omnitel, smartInfini, cultureland |
| externalCode | enum |  false | 외부 업체 코드 (옴니텔일 경우 POC0000090, POC0000094) |
| type | enum |  false | 서비스 유형 supply, distribution, etc |
| calculationType | enum |  false | 정산 유형 purchased, used |
| name | string |  false | 서비스명 |
| pongiftContactId | id |  false | 폰기프트 담당자 id |
| local | boolean |  false | local 유무 |
| pgUsage | boolean |  false | PG사용 유무 |
| pgCardUsage | boolean |  false | PG Card 사용 유무 |
| pgPhoneUsage | boolean |  false | PG Phone 사용 유무 |
| pgAccountUsage | boolean |  false | PG 계좌 사용 유무 |
| paymentBank | enum |  false | 입금 계좌 은행 KEB하나, 외환(하나), 국민, 농협, 우리, 신한, 기업, 경남, 광주, 대구, 도이치, 부산, 산업, 상호저축, 새마을금고, 수협중앙회, 신용협동조합, 우체국, 전북, 제주, 한국씨티, BOA, HSBC, JP모간, SC, 교보증권, 대신증권, 대우증권, 동부증권, 유안타증권, 메리츠증권, 미래에셋, 부국증권, 삼성증권, 신영증권, 신한금융투자, NH투자증권, 유진증권, 키움증권, 하나금융투자, 하이투자증권, 한국투자, 한화투자증권, 현대증권, 이베스트투자증권, HMC증권, LIG증권, SK증권, 산림조합, 중국공상은행, BNP파리바은행, KB투자증권, 펀드온라인코리아 |
| paymentDepositor | string |  false | 입금 계좌 예금주 |
| paymentAccount | string |  false | 입금 계좌 |
| customerCenterPhoneNum | string |  false | 고객센터 전화번호 |
| logoImgId | id |  false | 로고 이미지 id |
| distribution | enum |  false | 유통채널 유형 online, offline |
| businessRange | enum |  false | 영업범위 corporation, channel |
| info | string |  false | 상품 공통 안내 |
| country | enum |  false | 국가코드 kr |
| headerColor | enum |  false | 서비스 헤더 칼러 |
| chargeState | enum |  false | 수수료 상태 supply, distribution |
| chargeType | enum |  false | 수수료 유형 chargeTypeTotal, chargeTypeSeparate |
| charge | int |  false | 수수료 (%) |
| period | enum |  false | 정산주기 onceAMonth, twiceAMonth |
| startDay | int |  false | 정산 시작일 |
| transferDay | int |  false | 입금일 (D + N일) |
| signUpType | enum |  false | 가입 형태(제공자타입) email, phone, social, phoneId, normalId, phoneEmail, authCi |
| uid | string |  false | 아이디, 그냥 아이디로만 가입할땐 일반 아이디, 이메일 가입이면 이메일, 전화번호가입이면 전화번호 |
| nick | string |  false | 닉네임 |
| secret | string |  false | 비밀번호 혹은 엑세스토큰, 전화번호가입이면 인증번호 |
| contactTypes | enum |  false | 담당자 타입 affiliation, calculation, development |
| contactParts | string |  false | 담당자 부서 |
| contactPositions | string |  false | 담당자 직급 |
| contactNames | string |  false | 담당자 이름 |
| contactTelephoneNums | string |  false | 담당자 일반 전화번호 |
| contactCellphoneNums | string |  false | 담당자 전화번호 |
| contactEmails | string |  false | 담당자 이메일 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | logoImg | images객체 |
 | category | Appcategorys객체 |
 | corporation | Appcorporations객체 |
 | affiliation | Appaffiliations객체 |
 | pongiftContact | ApppongiftContacs객체 | 
 | calculations | Appcalculations객체 |
 | contacts | Appcontacts객체 |
 | userManages | AppuserManages객체 |
 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "accessKey": "sRuWet95N5MZ8uW9vSkSGw==",
   "authorId": 1,
   "affiliationId": 1,
   "corporationId": 1,
   "categoryId": null,
   "externalEnterprise": null,
   "externalCode": null,
   "state": "standby",
   "type": "distribution",
   "calculationType": "purchased",
   "name": "312312",
   "pongiftContactId": 1,
   "local": false,
   "pgUsage": true,
   "pgCardUsage": false,
   "pgPhoneUsage": false,
   "pgAccountUsage": false,
   "paymentBank": null,
   "paymentDepositor": null,
   "paymentAccount": null,
   "customerCenterPhoneNum": null,
   "logoImgId": null,
   "distribution": "online",
   "businessRange": "corporation",
   "info": null,
   "country": "kr",
   "headerColor": null,
   "templateImageKey": null,
   "createdAt": 1509498727275729,
   "updatedAt": 1509498727297961,
   "deletedAt": null,
   "id": 87,
   "affiliation": {
      "authorId": 1,
      "name": "pongift",
      "createdAt": 1475995528881034,
      "updatedAt": 1475995528881034,
      "deletedAt": null,
      "id": 1
   },
   "corporation": {
      "authorId": 1,
      "corporationId": null,
      "type": "corporation",
      "number": "201-17-52716",
      "name": "슬로그업",
      "address": "서울 용산구 청파동 47길",
      "leader": "이화랑",
      "condition": "서비스업",
      "category": "소프트웨어",
      "logoImgId": 298,
      "state": "contract",
      "contractedAt": "2016-10-15T15:00:00.000Z",
      "canceledAt": null,
      "country": "kr",
      "createdAt": 1475995528881034,
      "updatedAt": 1484546757687247,
      "deletedAt": null,
      "id": 1,
      "logoImg": {
         "authorId": 1,
         "folder": "corporation",
         "dateFolder": "2016-12-13",
         "name": "upload_a526dd7c68c2a6f8ea1eed981f304edd.png",
         "authorized": true,
         "createdAt": 1479618484371349,
         "updatedAt": 1479776733708153,
         "deletedAt": null,
         "id": 298
      }
   },
   "category": null,
   "logoImg": null,
   "pongiftContact": {
      "aid": "dusrn@slogup.com",
      "email": "dusrn@slogup.com",
      "secret": "8eS0Euk5FHy/OdHXHH+z33c0xNH05LEunUsZ18XB7J/WPZpTze57q5neJ4oPsVjbVRxF7nNnZcsxgy5ee1Js2A==",
      "salt": "KFzVG37h8n6vxOS825jYnA==",
      "phoneNum": "+821092185131",
      "name": "김종엽",
      "nick": "dusrn",
      "role": "roleS",
      "gender": null,
      "birth": null,
      "isVerifiedEmail": false,
      "country": "KR",
      "language": "ko",
      "isReviewed": false,
      "agreedEmail": true,
      "agreedPhoneNum": true,
      "agreedTermsAt": null,
      "profileId": 1,
      "di": null,
      "ci": null,
      "createdAt": 1475995092714968,
      "updatedAt": 1488855458740117,
      "deletedAt": null,
      "passUpdatedAt": 1488855458737335,
      "id": 1
   },
   "calculations": [
      {
         "authorId": 1,
         "serviceId": 87,
         "chargeState": "distribution",
         "chargeType": "chargeTypeTotal",
         "charge": 100,
         "period": "onceAMonth",
         "startDay": 1,
         "transferDay": 1,
         "bookAt": "2017-11-01T01:12:07.000Z",
         "createdAt": 1509498727306670,
         "updatedAt": 1509498727306699,
         "deletedAt": null,
         "id": 100
      }
   ],
   "contacts": [
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "affiliation",
         "part": null,
         "position": null,
         "name": "1",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315215,
         "updatedAt": 1509498727315246,
         "id": 292
      },
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "calculation",
         "part": null,
         "position": null,
         "name": "2",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315385,
         "updatedAt": 1509498727315393,
         "id": 293
      },
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "development",
         "part": null,
         "position": null,
         "name": "3",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315458,
         "updatedAt": 1509498727315464,
         "id": 294
      }
   ],
   "userManages": [
      {
         "authorId": 1,
         "userId": 176,
         "corporationId": null,
         "serviceId": 87,
         "createdAt": 1509498727362057,
         "updatedAt": 1509498727362088,
         "id": 75,
         "user": {
            "aid": "asdfasdf3",
            "email": null,
            "secret": "ECZxDIuvz9c8KzKOI2QpVytyxoqQZE0MUAs6YJDRsZIywrVPexSrMiDLE9KqaH+cdYSEqdx5WCMvbyZtpVzZEQ==",
            "salt": "KFZn7PAP94UMpYDazIJZfQ==",
            "phoneNum": null,
            "name": null,
            "nick": "fasdfaf23",
            "role": "roleD",
            "gender": null,
            "birth": null,
            "isVerifiedEmail": false,
            "country": "kr",
            "language": "ko",
            "isReviewed": false,
            "agreedEmail": true,
            "agreedPhoneNum": true,
            "agreedTermsAt": null,
            "profileId": 176,
            "di": null,
            "ci": null,
            "createdAt": 1509498727353314,
            "updatedAt": 1509498727353360,
            "deletedAt": null,
            "passUpdatedAt": 1509498032436320,
            "id": 176
         }
      }
   ]
}

```

### 4. put
 #### 서비스 수정

> * put.validate() : 유효성검사

> * put.validateCalculation() : 정산 유효성검사

> * put.validateContact() : 담당자 유효성검사

> * put.findDeleteImage() : AppService 조회

> * put.updateService() : AppService테이블 수정

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| affiliationId | int |  false | 제휴사 id |
| corporationId | int |  false | 법인 id |
| categoryId | int |  false | 카테고리 id |
| externalEnterprise | enum |  false | 외부 업체 omnitel, smartInfini, cultureland |
| externalCode | enum |  false | 외부 업체 코드 (옴니텔일 경우 POC0000090, POC0000094) |
| state | enum |  false | 현재 상태 standby, use, close |
| type | enum |  false | 서비스 유형 supply, distribution, etc |
| calculationType | enum |  false | 정산 유형 purchased, used |
| name | int |  false | 서비스명 |
| pongiftContactId | int |  false | 폰기프트 담당자 id |
| local | boolean |  false | local 유무 |
| pgUsage | boolean |  false | PG사용 유무 |
| pgCardUsage |  boolean |  false | PG Card 사용 유무 |
| pgPhoneUsage | boolean |  false | PG Phone 사용 유무 |
| pgAccountUsage | boolean |  false | PG 계좌 사용 유무 |
| paymentBank | enum |  false | 입금 계좌 은행 KEB하나, 외환(하나), 국민, 농협, 우리, 신한, 기업, 경남, 광주, 대구, 도이치, 부산, 산업, 상호저축, 새마을금고, 수협중앙회, 신용협동조합, 우체국, 전북, 제주, 한국씨티, BOA, HSBC, JP모간, SC, 교보증권, 대신증권, 대우증권, 동부증권, 유안타증권, 메리츠증권, 미래에셋, 부국증권, 삼성증권, 신영증권, 신한금융투자, NH투자증권, 유진증권, 키움증권, 하나금융투자, 하이투자증권, 한국투자, 한화투자증권, 현대증권, 이베스트투자증권, HMC증권, LIG증권, SK증권, 산림조합, 중국공상은행, BNP파리바은행, KB투자증권, 펀드온라인코리아 |
| paymentDepositor | string |  false | 입금 계좌 예금주 |
| paymentAccount | string |  false | 입금 계좌 |
| customerCenterPhoneNum | string |  false | 고객센터 전화번호 |
| logoImgId | id |  false | 로고 이미지 id |
| distribution | enum |  false | 유통채널 유형 online, offline |
| businessRange  enum| |  false | 영업범위 corporation, channel |
| info | string |  false | 상품 공통 안내 |
| country | enum |  false | 국가코드 kr |
| headerColor | enum |  false | 서비스 헤더 칼러 |
| chargeState | enum |  false | 수수료 상태 supply, distribution |
| chargeType | enum |  false | 수수료 유형 chargeTypeTotal, chargeTypeSeparate |
| charge | int |  false | 수수료 (%) |
| period | enum |  false | 정산주기 onceAMonth, twiceAMonth |
| startDay | int |  false | 정산 시작일 |
| transferDay | int |  false | 입금일 (D + N일) |
| bookAt | date  |  false | 예약 적용일 |
| deleteCalculationIds | id |  false | 삭제할 정산 ids |
| contactTypes | enum |  false | 담당자 타입 affiliation, calculation, development |
| contactParts | string |  false | 담당자 부서 |
| contactPositions | string |  false | 담당자 직급 |
| contactNames | string |  false | 담당자 이름 |
| contactTelephoneNums | string |  false | 담당자 일반 전화번호 |
| contactCellphoneNums | string |  false | 담당자 전화번호 |
| contactEmails | string |  false | 담당자 이메일 |
| deleteContactIds | id |  false | 삭제할 담당자 ids |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | logoImg | images객체 |
 | category | Appcategorys객체 |
 | corporation | Appcorporations객체 |
 | affiliation | Appaffiliations객체 |
 | pongiftContact | ApppongiftContacs객체 | 
 | calculations | Appcalculations객체 |
 | contacts | Appcontacts객체 |
 | userManages | AppuserManages객체 |
 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "accessKey": "sRuWet95N5MZ8uW9vSkSGw==",
   "authorId": 1,
   "affiliationId": 1,
   "corporationId": 1,
   "categoryId": null,
   "externalEnterprise": null,
   "externalCode": null,
   "state": "standby",
   "type": "distribution",
   "calculationType": "purchased",
   "name": "312312",
   "pongiftContactId": 1,
   "local": false,
   "pgUsage": true,
   "pgCardUsage": false,
   "pgPhoneUsage": false,
   "pgAccountUsage": false,
   "paymentBank": null,
   "paymentDepositor": null,
   "paymentAccount": null,
   "customerCenterPhoneNum": null,
   "logoImgId": null,
   "distribution": "online",
   "businessRange": "corporation",
   "info": null,
   "country": "kr",
   "headerColor": null,
   "templateImageKey": null,
   "createdAt": 1509498727275729,
   "updatedAt": 1509498727297961,
   "deletedAt": null,
   "id": 87,
   "affiliation": {
      "authorId": 1,
      "name": "pongift",
      "createdAt": 1475995528881034,
      "updatedAt": 1475995528881034,
      "deletedAt": null,
      "id": 1
   },
   "corporation": {
      "authorId": 1,
      "corporationId": null,
      "type": "corporation",
      "number": "201-17-52716",
      "name": "슬로그업",
      "address": "서울 용산구 청파동 47길",
      "leader": "이화랑",
      "condition": "서비스업",
      "category": "소프트웨어",
      "logoImgId": 298,
      "state": "contract",
      "contractedAt": "2016-10-15T15:00:00.000Z",
      "canceledAt": null,
      "country": "kr",
      "createdAt": 1475995528881034,
      "updatedAt": 1484546757687247,
      "deletedAt": null,
      "id": 1,
      "logoImg": {
         "authorId": 1,
         "folder": "corporation",
         "dateFolder": "2016-12-13",
         "name": "upload_a526dd7c68c2a6f8ea1eed981f304edd.png",
         "authorized": true,
         "createdAt": 1479618484371349,
         "updatedAt": 1479776733708153,
         "deletedAt": null,
         "id": 298
      }
   },
   "category": null,
   "logoImg": null,
   "pongiftContact": {
      "aid": "dusrn@slogup.com",
      "email": "dusrn@slogup.com",
      "secret": "8eS0Euk5FHy/OdHXHH+z33c0xNH05LEunUsZ18XB7J/WPZpTze57q5neJ4oPsVjbVRxF7nNnZcsxgy5ee1Js2A==",
      "salt": "KFzVG37h8n6vxOS825jYnA==",
      "phoneNum": "+821092185131",
      "name": "김종엽",
      "nick": "dusrn",
      "role": "roleS",
      "gender": null,
      "birth": null,
      "isVerifiedEmail": false,
      "country": "KR",
      "language": "ko",
      "isReviewed": false,
      "agreedEmail": true,
      "agreedPhoneNum": true,
      "agreedTermsAt": null,
      "profileId": 1,
      "di": null,
      "ci": null,
      "createdAt": 1475995092714968,
      "updatedAt": 1488855458740117,
      "deletedAt": null,
      "passUpdatedAt": 1488855458737335,
      "id": 1
   },
   "calculations": [
      {
         "authorId": 1,
         "serviceId": 87,
         "chargeState": "distribution",
         "chargeType": "chargeTypeTotal",
         "charge": 100,
         "period": "onceAMonth",
         "startDay": 1,
         "transferDay": 1,
         "bookAt": "2017-11-01T01:12:07.000Z",
         "createdAt": 1509498727306670,
         "updatedAt": 1509498727306699,
         "deletedAt": null,
         "id": 100
      }
   ],
   "contacts": [
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "affiliation",
         "part": null,
         "position": null,
         "name": "1",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315215,
         "updatedAt": 1509498727315246,
         "id": 292
      },
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "calculation",
         "part": null,
         "position": null,
         "name": "2",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315385,
         "updatedAt": 1509498727315393,
         "id": 293
      },
      {
         "authorId": 1,
         "serviceId": 87,
         "type": "development",
         "part": null,
         "position": null,
         "name": "3",
         "telephoneNum": null,
         "cellphoneNum": null,
         "email": null,
         "createdAt": 1509498727315458,
         "updatedAt": 1509498727315464,
         "id": 294
      }
   ],
   "userManages": [
      {
         "authorId": 1,
         "userId": 176,
         "corporationId": null,
         "serviceId": 87,
         "createdAt": 1509498727362057,
         "updatedAt": 1509498727362088,
         "id": 75,
         "user": {
            "aid": "asdfasdf3",
            "email": null,
            "secret": "ECZxDIuvz9c8KzKOI2QpVytyxoqQZE0MUAs6YJDRsZIywrVPexSrMiDLE9KqaH+cdYSEqdx5WCMvbyZtpVzZEQ==",
            "salt": "KFZn7PAP94UMpYDazIJZfQ==",
            "phoneNum": null,
            "name": null,
            "nick": "fasdfaf23",
            "role": "roleD",
            "gender": null,
            "birth": null,
            "isVerifiedEmail": false,
            "country": "kr",
            "language": "ko",
            "isReviewed": false,
            "agreedEmail": true,
            "agreedPhoneNum": true,
            "agreedTermsAt": null,
            "profileId": 176,
            "di": null,
            "ci": null,
            "createdAt": 1509498727353314,
            "updatedAt": 1509498727353360,
            "deletedAt": null,
            "passUpdatedAt": 1509498032436320,
            "id": 176
         }
      }
   ]
}

```

### 5. delete
 #### 서비스 제거

> * del.validate() : 유효성검사

> * del.findServiceImage() : 조건에 맞는 AppService테이블 로우조회

> * del.destroy() : 조건에 맞는 AppService테이블 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppServiceId |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />
