# product-promotions

## /api/pongift/product-promotions

### 1. get
#### 상품 프로모션 단일 조회

#### 응답 파라미터

[AppProductPromotions 테이블 참고](/markdown/db/AppProductPromotions.md)

<br /><br /><br /><br />

### 2. gets
#### 검색 조건에 따른 상품 프로모션 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string | false | 검색할 내용 |
| searchField | enum | false | 검색할 필드 id |
| orderBy | enum | false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum | true | 정렬 방식 DESC, ASC |
| last | microtime | false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| productId | int | false | 상품 id |
| promotionId | int | false | 프로모션 id |
| presentId | int | false | 증정 상품 id |
| serviceId | int | false | 서비스 id |


#### 응답 파라미터

[AppProductPromotions 테이블 참고](/markdown/db/AppProductPromotions.md)

<br /><br /><br /><br />

### 3. post
#### 상품 프로모션 생성

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int | true | 상품 id |
| promotionId | int | true | 프로모션 id |
| presentId | int | false | 증정 상품 id |
| discountRate | int | false | 할인율 |
| discount | int | false | 할인액 |


#### 응답 파라미터

[AppProductPromotions 테이블 참고](/markdown/db/AppProductPromotions.md)

<br /><br /><br /><br />

### 4. put
#### 상품 프로모션 수정

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | int | true | 상품 id |
| promotionId | int | true | 프로모션 id |
| presentId | int | false | 증정 상품 id |
| discountRate | int | false | 할인율 |
| discount | int | false | 할인액 |


#### 응답 파라미터

[AppProductPromotions 테이블 참고](/markdown/db/AppProductPromotions.md)

<br /><br /><br /><br />

### 5. delete
#### 상품 프로모션 제거

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 응답 코드 204

<br /><br /><br /><br />

