# promotions

## /api/pongift/promotions

### 1. get
 #### 단일 얻기

> * get.validate() : 유효성검사

> * get.setParam() : 조건에 맞는 AppPromotion테이블 로우조회 

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | ture | AppPromotionId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPromotion | AppPromotion객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 91,
   "eventId": null,
   "serviceId": null,
   "corporationId": null,
   "type": "discount",
   "serviceType": "total",
   "productType": "exchange",
   "name": "asdfasfdasfd",
   "memo": null,
   "totalPurchaseCount": null,
   "purchaseCountTotal": null,
   "purchaseCountAMonth": null,
   "purchaseCountADay": null,
   "totalPurchaseAmount": null,
   "purchaseAmountTotal": null,
   "purchaseAmountAMonth": null,
   "purchaseAmountADay": null,
   "purchaseAmountATry": null,
   "totalPurchasePrice": null,
   "purchasePriceTotal": null,
   "purchasePriceAMonth": null,
   "purchasePriceADay": null,
   "purchasePriceATry": null,
   "startAt": "2017-11-06T15:00:00.000Z",
   "endAt": "2017-11-11T14:00:00.000Z",
   "sdkBannerImgId1": null,
   "sdkBannerImgId2": null,
   "webBannerImgId1": null,
   "webBannerImgId2": null,
   "visibility": true,
   "createdAt": 1509960784081211,
   "updatedAt": 1510025182347656,
   "deletedAt": null,
   "id": 71,
   "service": null,
   "corporation": null,
   "event": null,
   "sdkBannerImg1": null,
   "sdkBannerImg2": null,
   "webBannerImg1": null,
   "webBannerImg2": null
}

```

### 2. gets
 #### 프로모션 조회

> * gets.validate() : 유효성검사

> * gets.setParam() : 조건에 맞는 AppPromotion테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id, name |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microTimestamp |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| eventId | int |  false | 이벤트 id |
| type | enum |  false | 프로모션 유형 discount, present |
| serviceType | enum |  false | 프로모션 범위 total, supply, distribution, partial |
| productType | enum |  false | 상품 유형 + exchange, cost |
| visibility | boolean |  false | 프로모션 visibility |
| corporationId | int |  false | 법인 id |
| serviceId | int |  false | 서비스 id |
| corporationServiceSearchItem | string |  false | 법인 / 서비스명 검색 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPromotion | AppPromotions [ ] |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "count": 1,
   "list": [
      {
         "event": null,
         "service": null,
         "corporation": null,
         "sdkBannerImg1": null,
         "sdkBannerImg2": null,
         "webBannerImg1": null,
         "webBannerImg2": null,
         "authorId": 1,
         "eventId": null,
         "serviceId": null,
         "corporationId": null,
         "type": "discount",
         "serviceType": "distribution",
         "productType": "exchange",
         "name": "test",
         "memo": null,
         "totalPurchaseCount": null,
         "purchaseCountTotal": null,
         "purchaseCountAMonth": null,
         "purchaseCountADay": null,
         "totalPurchaseAmount": null,
         "purchaseAmountTotal": null,
         "purchaseAmountAMonth": null,
         "purchaseAmountADay": null,
         "purchaseAmountATry": null,
         "totalPurchasePrice": null,
         "purchasePriceTotal": null,
         "purchasePriceAMonth": null,
         "purchasePriceADay": null,
         "purchasePriceATry": null,
         "startAt": "2017-11-08T23:00:00.000Z",
         "endAt": "2017-11-10T14:00:00.000Z",
         "sdkBannerImgId1": null,
         "sdkBannerImgId2": null,
         "webBannerImgId1": null,
         "webBannerImgId2": null,
         "visibility": 0,
         "createdAt": 1510130763446268,
         "updatedAt": 1510130875650656,
         "deletedAt": null,
         "id": 74
      }
      ]
}

```

### 3. post
 #### 프로모션 생성

> * post.validate() : 유효성검사

> * post.validateServicePromotion() : 서비스프로모션 유효성검사

> * post.validateDate() : 날짜 유효성검사

> * post.validateProductType() : 상품유형 유효성검사

> * post.validateType() : 타입 유효성검사

> * post.setParam() : AppPromotion테이블 로우생성

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| eventId | int |  false | 이벤트 id |
| type | enum |  false | 프로모션 유형 discount, present |
| serviceType | enum |  false | 프로모션 범위 total, supply, distribution, partial |
| productType | enum |  false | 상품 유형 exchange, cost |
| name | string |  false | 프로모션명 |
| memo | string |  false | 메모 |
| totalPurchaseCount | int |  false | 프로모션 전체 구매 제한 횟수 |
| purchaseCountTotal | int |  false | 총 구매 제한 횟수 |
| purchaseCountAMonth | int |  false | 월 구매 제한 횟수 |
| purchaseCountADay | int |  false | 일 구매 제한 횟수 |
| totalPurchaseAmount | int |  false | 프로모션 전체 구매 제한 수량 (교환형) |
| purchaseAmountTotal | int |  false | 총 구매 제한 수량 (교환형) |
| purchaseAmountAMonth | int |  false | 월 구매 제한 수량 (교환형) |
| purchaseAmountADay | int |  false | 일 구매 제한 수량 (교환형) |
| purchaseAmountATry | int |  false | 1회 구매 제한 수량 (교환형) |
| totalPurchasePrice | int |  false | 프로모션 전체 구매 제한 금액 (금액형) |
| purchasePriceTotal | int |  false | 총 구매 제한 금액 (금액형) |
| purchasePriceAMonth | int |  false | 월 구매 제한 금액 (금액형) |
| purchasePriceADay | int |  false | 일 구매 제한 금액 (금액형) |
| purchasePriceATry | int |  false | 1회 구매 제한 금액 (금액형) |
| startAt | date |  false | 프로모션 시작일자 |
| endAt | date |  false | 프로모션 종료일자 |
| sdkBannerImgId1 | int |  false | sdk 배너 이미지 id |
| sdkBannerImgId2 | int |  false | sdk 배너 이미지 id |
| webBannerImgId1 | int |  false | web 배너 이미지 id |
| webBannerImgId2 | int |  false | web 배너 이미지 id |
| visibility | boolean |  false | 프로모션 visibility |
| serviceIds | int |  false | 프로모션 전시 할 서비스 ids |
| allProducts | boolean |  false | 전체 상품 적용 여부 |
| discountRate | int |  false | 전체 상품 할인율 |
| discount | int |  false | 전체 상품 할인액 |
| productIds | int |  false | 프로모션 적용 할 상품 ids |
| discountRates | int |  false | 할인율들 |
| discounts | int |  false | 할인액들 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPromotion | AppPromotion객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "eventId": null,
   "serviceId": null,
   "corporationId": null,
   "type": "discount",
   "serviceType": "total",
   "productType": "exchange",
   "name": "promotion",
   "memo": null,
   "totalPurchaseCount": null,
   "purchaseCountTotal": null,
   "purchaseCountAMonth": null,
   "purchaseCountADay": null,
   "totalPurchaseAmount": null,
   "purchaseAmountTotal": null,
   "purchaseAmountAMonth": null,
   "purchaseAmountADay": null,
   "purchaseAmountATry": null,
   "totalPurchasePrice": null,
   "purchasePriceTotal": null,
   "purchasePriceAMonth": null,
   "purchasePriceADay": null,
   "purchasePriceATry": null,
   "startAt": "2017-12-12T15:00:00.000Z",
   "endAt": "2017-12-13T15:00:00.000Z",
   "sdkBannerImgId1": null,
   "sdkBannerImgId2": null,
   "webBannerImgId1": null,
   "webBannerImgId2": null,
   "visibility": false,
   "createdAt": 1512963010439996,
   "updatedAt": 1512963010440040,
   "deletedAt": null,
   "id": 75,
   "event": null,
   "sdkBannerImg1": null,
   "sdkBannerImg2": null,
   "webBannerImg1": null,
   "webBannerImg2": null
}

```

### 4. put
 #### 프로모션 수정

> * put.validate() : 유효성검사

> * put.validateDiscounts() : 상품 유효성검사

> * put.validateProductType() : 상품유형 유효성검사

> * put.findBannerImages() : 프로모션 이미지조회

> * put.validateServicePromotion() : 서비스 프로모션 유효성검사

> * put.validateDate() : 날짜 유효성검사

> * put.checkDate() : 날짜 유효성검사2

> * put.updatePromotion() : 조건에 맞는 AppPromotion테이블 로우업데이느

> * put.updatePromotionImage() : 조건에 맞는 AppPromotion테이블 로우이미지 업데이트

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| eventId | |  false | 이벤트 id |
| type | |  false | 프로모션 유형 discount, present |
| serviceType | |  false | 프로모션 범위 total, supply, distribution, partial |
| productType | |  false | 상품 유형 exchange, cost |
| name | |  false | 프로모션명 |
| memo | |  false | 메모 |
| totalPurchaseCount | |  false | 프로모션 전체 구매 제한 횟수 |
| purchaseCountTotal | |  false | 총 구매 제한 횟수 |
| purchaseCountAMonth | |  false | 월 구매 제한 횟수 |
| purchaseCountADay | |  false | 일 구매 제한 횟수 |
| totalPurchaseAmount | |  false | 프로모션 전체 구매 제한 수량 (교환형) |
| purchaseAmountTotal | |  false | 총 구매 제한 수량 (교환형) |
| purchaseAmountAMonth | |  false | 월 구매 제한 수량 (교환형) |
| purchaseAmountADay | |  false | 일 구매 제한 수량 (교환형) |
| purchaseAmountATry | |  false | 1회 구매 제한 수량 (교환형) |
| totalPurchasePrice | |  false | 프로모션 전체 구매 제한 금액 (금액형) |
| purchasePriceTotal | |  false | 총 구매 제한 금액 (금액형) |
| purchasePriceAMonth | |  false | 월 구매 제한 금액 (금액형) |
| purchasePriceADay | |  false | 일 구매 제한 금액 (금액형) |
| purchasePriceATry | |  false | 1회 구매 제한 금액 (금액형) |
| startAt | |  false | 프로모션 시작일자 |
| endAt | |  false | 프로모션 종료일자 |
| sdkBannerImgId1 | |  false | sdk 배너 이미지 id |
| sdkBannerImgId2 | |  false | sdk 배너 이미지 id |
| webBannerImgId1 | |  false | web 배너 이미지 id |
| webBannerImgId2 | |  false | web 배너 이미지 id |
| visibility | |  false | 프로모션 visibility |
| serviceIds | |  false | 프로모션 전시 할 서비스 ids |
| deleteServiceIds | |  false | 프로모션 전시 해제 할 서비스 ids |
| productIds | |  false | 프로모션 적용 할 상품 ids |
| discounts | |  false | 할인액들 |
| presentProductIds | |  false | 증정 상품 ids |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppPromotion | AppPromotion객체 
 
 
 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "authorId": 20,
   "eventId": null,
   "serviceId": null,
   "corporationId": null,
   "type": "discount",
   "serviceType": "total",
   "productType": "exchange",
   "name": "promotion",
   "memo": null,
   "totalPurchaseCount": null,
   "purchaseCountTotal": null,
   "purchaseCountAMonth": null,
   "purchaseCountADay": null,
   "totalPurchaseAmount": null,
   "purchaseAmountTotal": null,
   "purchaseAmountAMonth": null,
   "purchaseAmountADay": null,
   "purchaseAmountATry": null,
   "totalPurchasePrice": null,
   "purchasePriceTotal": null,
   "purchasePriceAMonth": null,
   "purchasePriceADay": null,
   "purchasePriceATry": null,
   "startAt": "2017-12-12T15:00:00.000Z",
   "endAt": "2017-12-15T00:00:00.000Z",
   "sdkBannerImgId1": null,
   "sdkBannerImgId2": null,
   "webBannerImgId1": null,
   "webBannerImgId2": null,
   "visibility": false,
   "createdAt": 1512963010439996,
   "updatedAt": 1512963239912569,
   "deletedAt": null,
   "id": 75,
   "event": null,
   "sdkBannerImg1": null,
   "sdkBannerImg2": null,
   "webBannerImg1": null,
   "webBannerImg2": null
}

```

### 5. delete
 #### 프로모션 제거

> * del.validate() : 유효성검사

> * del.findBannerImages() : 프로모션 배너이미지 조회

> * del.destroy() : 조건에 맞는 AppPromotion테이블 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppPromotionId |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |


