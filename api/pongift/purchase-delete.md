# purchase-delete

## /api/pongift/purchase-delete

### 1. post
#### 오래된 구매내역 삭제

> * 현재시간으로부터 30분 전까지 결제내역 중 standby 상태인 것들 삭제처리 

> * 삭제하는 결제내역중 프로모션이 있는 결제라면 프로모션 정보도 수정

#### 응답 코드 204

<br /><br /><br /><br />

