# purchase-fail

## /api/pongift/purchase-fail

### 1. post
#### 구매 제거

> * 구매내역을 삭제

> * 프로모션이 적용된 구매내역이라면 프로모션 카운트도 수정 후 삭제된다.

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseCode | int | true | 주문 번호 |


#### 응답 코드 204

<br /><br /><br /><br />

