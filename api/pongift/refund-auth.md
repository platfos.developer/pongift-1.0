# refund-auth

## /api/pongift/refund-auth

### 1. post
 #### 환불 인증번호 전송

> * post.validate() : 유효성검사

> * post.createAuthNum() : 인증번호생성

> * post.sendSMS() : 수신자에게 인증번호전송

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| phoneNum | int |  false | 폰번호 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | authNum | int |

 <br /><br /><br /><br />

#### 응답 예제 (JSON)  

```json
{
   "authNum": "477601"
}

```
