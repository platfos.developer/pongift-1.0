# purchase-success

## /api/pongift/purchase-success

### 1. post
#### kcp 서버로부터 구매 정보 수정 요청

> * KCP 결제가 완료되면 이 api 호출

> * 정산 처리 후 구매내역정보 수정

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseCode | int | true | 주문 신청 번호 |
| purchaseAuthorizationCode | int | true | 카드결제시 주문 승인 번호 |
| tno | int |  false | transaction number: 실질적인 승인번호 |
| cash_no | int |  false | 계좌이체시 현금영수증 거래 번호 |
| cash_authno | int |  false | 계좌이체시 현금영수증 거래 승인 번호 |


#### 응답 파라미터

| parameter | desc |
| --------- | :---- |

<br /><br /><br /><br />

