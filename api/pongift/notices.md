# notices

## /api/pongift/notices

### 1. get
#### 공지사항 단일 조회

#### 응답 파라미터

[AppNotices 테이블 참고](/markdown/db/AppNotices.md)

<br /><br /><br /><br />

### 2. gets
#### 옵션에 따라 공지사항 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id, title, content |
| orderBy | enum | false | 정렬 기준 필드 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| corporationId | int |  false | 법인 id |
| serviceId | int |  false | 서비스 id |
| type | enum |  false | 공지 유형 check, product, disability, security, etc |
| target | enum |  false | 공지 대상 admin, user |


#### 응답파라미터

[AppNotices 테이블 참고](/markdown/db/AppNotices.md)

<br /><br /><br /><br />

### 3. post
#### 관리자 공지 생성

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int |  false | 법인 id |
| serviceId | int |  false | 서비스 id |
| type | enum | false | 공지 유형 check, product, disability, security, etc |
| target | enum |  false | 공지 대상 admin, user |
| title | string |  false | 공지 제목 |
| content | string |  false | 공지 내용 |
| country | enum |  false | 국가코드 kr |

#### 응답 파라미터

[AppNotices 테이블 참고](/markdown/db/AppNotices.md)

<br /><br /><br /><br />

### 4. put
#### 관리자 공지 수정

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| corporationId | int |  false | 법인 id |
| serviceId | int |  false | 서비스 id |
| type | enum |  false | 공지 유형 check, product, disability, security, etc |
| target | enum |  false | 공지 대상 admin, user |
| title | string |  false | 공지 제목 |
| content | string |  false | 공지 내용 |
| country | enum |  false | 국가코드 kr |

#### 응답 파라미터

[AppNotices 테이블 참고](/markdown/db/AppNotices.md)

<br /><br /><br /><br />

### 5. delete
#### 관리자 공지 제거

> * 권한 (roleAdmin 이상 && (법인id || 서비스id 소유))

#### 응답 코드 204

<br /><br /><br /><br />

