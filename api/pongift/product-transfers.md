# product-transfers

## /api/pongift/product-transfers

### 1. get
#### 쿠폰 발송정보 단일 조회

#### 응답 파라미터

[AppProductTransfers 테이블 참고](/markdown/db/AppProductTransfers.md)

<br /><br /><br /><br />

### 2. gets
#### 검색조건에 따른 발송 내역 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | enum |  false | 검색할 필드 id |
| orderBy | enum |  false | 정렬 기준 createdAt, updatedAt |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microtime |  false | 조회 기준 마지막 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |
| purchaseId | int |  false | 구매 id |


#### 응답 파라미터

[AppProductTransfers 테이블 참고](/markdown/db/AppProductTransfers.md)

<br /><br /><br /><br />

### 3. post
#### 발송 내역 생성

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseId | int |  true | 구매 id |
| destination | int |  true | 수신자 번호 |


#### 응답 파라미터

[AppProductTransfers 테이블 참고](/markdown/db/AppProductTransfers.md)

<br /><br /><br /><br />

### 4. put
#### 쿠폰 취소 처리

> * 권한 (roleUltraAdmin)

> * 옴니텔, 스마트인피니에 쿠폰 취소요청

> * 상품공급사, 유통대행사 정산처리

> * 쿠폰 발송정보 업데이트

> * 스토어팜 환불요청

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseId | int |  false | 구매 id |
| destination | int |  false | 수신자 번호 |
| state | enum |  false | 상태 disuse, use, partial, cancel, refundStart, refundFinish |
| refundRate | int |  false | 환불률 |
| refundPrice | int |  false | 환불금액 |
| refundBank | string |  false | 환불계좌은행 |
| refundDepositor | string |  false | 환불계좌예금주 |
| refundAccount | string |  false | 환불계좌번호 |
| refundStartedAt | datetime |  false | 환불승인일자 |
| refundFinishedAt | datetime |  false | 환불완료일자 |


#### 응답 코드 204

<br /><br /><br /><br />

### 5. delete
#### 쿠폰 발송정보 삭제

#### 응답 코드 204


<br /><br /><br /><br />

