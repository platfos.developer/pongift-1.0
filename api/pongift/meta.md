# meta

## /api/pongift/meta

### 1. gets
#### meta 정보 조회

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| last | microtime |  false | 조회 기준 데이터 일자 |
| size | int |  false | 가져올 데이터 갯수 |


#### 응답 파라미터

[AppMeta 테이블 참고](/markdown/db/AppMeta.md)

<br /><br /><br /><br />

### 2. post
#### meta 정보 생성

> * 권한 (roleUltraAdmin)

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| expiryDay | int |  false | 지정 유효기간 일수 |
| extendExpiryDay | int |  false | 지정 유효기간 연장일수 |
| extendExpiryDayCountLimit | int |  false | 지정 유효기간 연장 가능 횟수 |
| transferCountLimit | int |  false | 발송 횟수 제한 |
| refundRateBeforeExpiryDay | int |  false | 유효기간 내 환불률 |
| refundRateAfterExpiryDay | int |  false | 유효기간 이후 환불률 |
| extinctivePrescription | int |  false | 소멸 시효 (년) |
| maxPurchaseAmount | int |  false | 구매 수량 제한 |


#### 응답 파라미터

[AppMeta 테이블 참고](/markdown/db/AppMeta.md)

<br /><br /><br /><br />

### 3. delete
#### meta 정보 생성 내역 삭제

> * 권한 (roleUltraAdmin)

#### 응답 코드 204

<br /><br /><br /><br />

