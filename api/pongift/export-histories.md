# export-histories

## /api/pongift/export-histories

### 1. get
#### 단일 얻기

> * 권한 (roleUltraAdmin)

> * 거래내역, 정산내역 다운로드시 폴링용으로 사용 


#### 응답 파라미터

[AppExportHistories 테이블 참고](/markdown/db/AppExportHistories.md)

 <br /><br /><br /><br />

