# store-images

## /api/pongift/store-images

### 1. get
 #### 단일 얻기

> * get.validate() : 유효성검사

> * get.setParam() : 조건에 맞는 AppStoreImage테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | true | AppStoreImageId |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppStoreImage | AppStoreImages객체 |

 <br /><br /><br /><br />
 
 
 #### 응답 예제 (JSON)  

```json
{
   "storeId": 17,
   "imageId": 683,
   "index": 1,
   "createdAt": 1496036635860996,
   "updatedAt": 1496036635861041,
   "id": 41
}

```

### 2. gets
 #### 매장 이미지 브릿지 조회

> * gets.validate() : 유효성검사

> * gets.setParam () : 조건에 맞는 AppStoreImage테이블 조회

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| searchItem | string |  false | 검색할 내용 |
| searchField | id |  false | 검색할 필드 id |
| orderBy | enum |  false | 정렬 기준 필드 createdAt, updatedAt, index |
| sort | enum |  true  | 정렬 방식 DESC, ASC |
| last | microTimestamp |  false | 조회 기준 데이터 |
| size | int |  false | 가져올 데이터 갯수 |
| storeId | int |  false | 매장 id |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | AppStoreImage | AppStoreImages객체 |
 | store | Appstores객체 |
 | image | images객체 |

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
[
   {
      "storeId": 17,
      "imageId": 683,
      "index": 1,
      "createdAt": 1496036635860996,
      "updatedAt": 1496036635861041,
      "id": 41,
      "store": {
         "authorId": 20,
         "corporationId": 21,
         "name": "ㅂㅈㄷㅂㄷㅈㅂㅈㄷ",
         "address": "대한민국 서울특별시 중구 플랫포스",
         "location": {
            "type": "Point",
            "coordinates": [
               37.560363,
               126.993467
            ]
         },
         "phoneNum": "123123123123",
         "info": "ㅁㄴㅇㄹ",
         "createdAt": 1492505789282218,
         "updatedAt": 1496036635869349,
         "deletedAt": null,
         "id": 17
      },
      "image": {
         "authorId": 20,
         "folder": "store",
         "dateFolder": "2017-05-29",
         "name": "upload_3a8f4f69ce6550e86f17a8a85278e77d.PNG",
         "authorized": true,
         "createdAt": 1496036632823313,
         "updatedAt": 1496036632823359,
         "deletedAt": null,
         "id": 683
      }
   },
   {
      "storeId": 2,
      "imageId": 521,
      "index": 1,
      "createdAt": 1491197125940501,
      "updatedAt": 1491197125940536,
      "id": 40,
      "store": null,
      "image": {
         "authorId": 20,
         "folder": "store",
         "dateFolder": "2017-01-10",
         "name": "upload_6a83907d3c240604ee7fa621dad643fa.jpg",
         "authorized": true,
         "createdAt": 1484024637977334,
         "updatedAt": 1484024637977391,
         "deletedAt": null,
         "id": 521
      }
   },
   {
      "storeId": 11,
      "imageId": 588,
      "index": 1,
      "createdAt": 1488960048210374,
      "updatedAt": 1488960048210408,
      "id": 37,
      "store": null,
      "image": {
         "authorId": 20,
         "folder": "store",
         "dateFolder": "2017-03-08",
         "name": "upload_39bca8b8a8bee9fcfc824fb0e1ad8232.png",
         "authorized": true,
         "createdAt": 1488960044950652,
         "updatedAt": 1488960044950699,
         "deletedAt": null,
         "id": 588
      }
   },
   {
      "storeId": 9,
      "imageId": 557,
      "index": 1,
      "createdAt": 1488421268945301,
      "updatedAt": 1488421268945349,
      "id": 35,
      "store": null,
      "image": null
   }
]

```

### 3. post
 #### 매장 이미지 브릿지 생성

> * post.validate() : 유효성검사

> * post.setParam() : 조건에 맞는 AppStoreImage테이블 로우생성


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| storeId | int |  false | 매장 id |
| imageId | int |  false | 이미지 id |
| index | int |  false | 이미지 순서 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManage |
 | store | AppStore객체 |
 | image | Image객체 |
 
 

 <br /><br /><br /><br />
 
 #### 응답 예제 (JSON)  

```json
{
   "storeId": 11,
   "imageId": 588,
   "index": 1,
   "createdAt": 1488960048210374,
   "updatedAt": 1488960048210408,
   "id": 37
}
```

### 4. put
 #### 매장 이미지 브릿지 수정

> * post.validate() : 유효성검사

> * post.setParam() : 조건에 맞는 AppStoreImage테이블 로우 수정


#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| storeId | int |  false | 매장 id |
| imageId | int |  false | 이미지 id |
| index | int |  false | 이미지 순서 |
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | userManage | AppUserManage |
 | store | AppStore객체 |
 | image | Image객체 |
 
 <br /><br /><br /><br />
 
 ```json
{
   "storeId": 11,
   "imageId": 588,
   "index": 1,
   "createdAt": 1488960048210374,
   "updatedAt": 1488960048210408,
   "id": 37
}
```

### 5. delete
 #### 매장 이미지 브릿지 제거

> * del.validate() : 유효성검사

> * del.checkExistStoreImage() : 조건에 맞는 AppStoreImage테이블 로우조회

> * del.destroy() : 조건에 맞는 AppStoreImage테이블 로우삭제

#### 요청 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+)

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | int | ture | AppStoreImages객체 |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />


