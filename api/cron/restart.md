# restart

## /api/cron/restart

### 1. post
#### 크론 시작

> * 입력받은 키 값에 해당하는 크론을 실행한다.

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| key | varchar |  false | 크론 재시작 key |
 
#### 응답 코드 204



### 2. delete
#### 크론 정지

> * 입력받은 키 값에 해당하는 크론을 정지한다.

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| id | varchar | true | 크론 key |
 

#### 응답 코드 204
