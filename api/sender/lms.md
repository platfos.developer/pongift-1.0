# lms

## /api/sender/lms

### 1. post
 #### lms 발송 - 현재 '컬쳐랜드 상품권' 자체발송을 위한 API로 사용

> * post.findBulkBarcode() : AppBulkBarcode테이블의 조건에 해당하는 row를 가져옴 (배포를 하지 않은 문화상품권)

> * post.findProductTransfer() : 발송정보를 가져옴

> * post.findPurchase() : 구매정보를 가져옴

> * post.sendLms() : LMS로 해당상품을 '수신자'에게 보냄

> * post.insertBulkBarcode() : 해당바코드가 DB에 있다면 update / 업다면 create (직접 API 이용시 바코드값을 보냄)

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| barcode | |  false | 바코드값 |
| productTransferId | |  false | 거래내역 상세 ID |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음 200

 | parameter | desc |
 | --------- | :---- |

 <br /><br /><br /><br />

