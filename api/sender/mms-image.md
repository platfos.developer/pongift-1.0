# mms-image

## /api/sender/mms-image

### 1. post
 #### mms 이미지 생성 (MMS로 발송될 이미지)

> * post.findProduct() : MMS로 상품발송을 할 상품정보를 가져옴

> * post.series() : MMS 발송  <br>
                    createBarcode() : 바코드 템플릿 생성<br>
                    mergeBarcode() : 생성한 바코드 템플릿을 MMS 템플릿과 merge<br>
                    imageMerge() : 상품템플릿과 MMS템플릿을 merge<br>
                    createExpiryAtImg() : 유효기간 템플릿 생성<br>
                    mergeExpiryAtImg() : 유효기간 템플릿과 MMS템플릿 merge<br>
                    removeTextImage() : MMS템플릿을 만들기위해 만들었던 기초 템플릿 삭제 (바코드/유효기간 템플릿)
                   

#### 요청 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| barcode | varchar |  false | 바코드 넘버 |
| productId | Int |  false | 생성 상품 |
| expiryAt | varchar |  false | 비코드 유효 기간 |
 

#### 응답 파라미터 - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) 없음 200

 | parameter | desc |
 | --------- | :---- |
 | barcode | 바코드번호 |
 | productId | 생성 상품 |
 | expiryAt | 바코드 유효 기간 |

 <br /><br /><br /><br />

