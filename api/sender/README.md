## 메시지 전송

### 1. [lms 전송](/markdown/api/sender/lms.md)
### 2. [mms 이미지 (바코드 포함) 생성](/markdown/api/sender/mms-image.md)
### 3. [mms 이미지 (템플릿 적용) 생성](/markdown/api/sender/product-image.md)
### 4. [스토어팜 mms 발송](/markdown/api/sender/storeFarm-mms.md)

