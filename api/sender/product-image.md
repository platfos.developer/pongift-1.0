# product-image

## /api/sender/product-image

### 1. post
 #### MMS 상품 이미지 만들기 (상품 템플릿 만들기) <br> MMS 상품발송 시 더 빠르게 보내기 위해 미리 상품 템플릿을 만듬
 

> * 쿠폰 문자전송시 사용할 쿠폰이미지를 생성
> * productId를 입력하지 않을 경우 모든 상품 쿠폰이미지를 생성
> * 쿠폰이미지가 없는 것만 생성

#### 요청 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| productId | Int | false | 상품 아이디 |

 #### 응답 파라미터 ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200

 | parameter | desc |
 | --------- | :---- |
 | productId | 상품 아이디 |

 <br /><br /><br /><br />

