# kcp-send

## /api/kcp/kcp-send

### 1. post
#### kcp 서버로부터 옴니텔 상품 및 스마트인피니 전송 - 테스트 API로 사용하였으며 / 현재 사용하지 않음

> * findPurchaseByPurchaseCode(): purchaseCode로 거래내역 검색

> * omnitelSendRequest(): 옴니텔 쿠폰 요청

> * smartInfiniSendRequest(): 스마트인피니 쿠폰 요청

> * updatePurchase(): 요청한 쿠폰 정보 데이터 업데이트


#### 파라미터

| name | type | essential | description | 
| ---------- | :--------- | :----------: |  :----------: | 
| purchaseCode | String |  false | 주문 신청 번호 |
| purchaseAuthorizationCode | String |  false | 카드결제시 주문 승인 번호 / 모바일 결제시 전화번호 / 계좌이체시 현금영수증 인증 번호 |
| tno | String |  false | transaction number: 실질적인 승인번호 |
| cash_no | String |  false | 계좌이체시 현금영수증 요청 번호 |
| purchase | String |  false | 사용하지 않음 | 
 

#### 응답 파라미터 - ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) 200
 <br /><br /><br /><br />

