# AppContacts


## DESC
```pre
  * 서비스담당자
   - '법인'/'서비스' 별 담당자를 지정(설정)한다. 
     폰기프트 서비스를 이용하는 사용자(법인/서비스)의 폰기프트 담당자정보이다. 
     '개발','정산' 등 담당부서 별로 정보를 입력할 수 있다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | authorId |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | serviceId |
| type | [enum #3](#enum3) | 'affiliation' | false | | 담당자 유형 |
| part | varchar(255) | NULL | true | | 담당자 부서 |
| position | varchar(255) | NULL | true | | 담당자 직급 |
| name | varchar(255) | NULL | true | | 담당자 유형 |
| telephoneNum | varchar(255) | NULL | true | | 휴대폰 번호 |
| cellphoneNum | varchar(255) | NULL | true | | 전화번호 |
| email | varchar(255) | NULL | true | | 이메일 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| id | int(11) | | false | | id |



### ENUM
#### enum3
* 'affiliation','calculation','development'


---
### ClassMethods


#### createContact 
 * AppContact의 row 생성 (각 서비스별 담당자)
 
#### updateContact 
 * 생성된 AppContact row 업데이트
 
#### deleteContact
 * 해당되는 AppContact row 삭제 
 
#### findContactsByOptions 
 * 조건에 해당되는 개수만큼 AppContact row 조회
 
#### findContactsByServiceId
 * 서비스에 해당하는 AppContact row 3개 조회 ( limit : 3 where : serviceId )
