# AppAuctionOrderInfos


## DESC
```pre
  * 옥션 주문 정보
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| sendNo | varchar(255) | NULL | true |  | 전송 no |
| state | varchar(255) | NULL | true |  | 주문 상태 |
| itemNo | varchar(255) | | true | | 주문 상품 번호 |
| itemName | varchar(255) |  | true | | 상품 이름 |
| orderNo | varchar(255) |  | true | | 주문 번호 |
| paymentDate | varchar(255) |  | true | | 결제일 |
| sendRequestDate | varchar(255) |  | true | | 주문 요청일 |
| buyerID | varchar(255) |  | true | | 고객 id |
| buyerName | varchar(255) |  | true | | 고객 이름 |
| buyerMobileTel | varchar(255) |  | true | | 고객 전화번호 |
| receiverName | varchar(255) |  | true | | 받는사람 이름 |
| receiverMobileTel | varchar(255) |  | true | | 받는사람 전화번호 |
| firstSendYN | varchar(255) |  | true | | 전송 여부 |
| awardQty | varchar(255) |  | true | | 상품갯수 |
| xmlns | varchar(255) |  | true | | 전송url |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |
