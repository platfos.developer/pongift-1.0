# AppPushes


## DESC


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자 ID |
| themeId | int(11) | NULL | true | [AppThemes](/markdown/db/AppThemes.md).id | 테마 ID |
| name | varchar(255) | | false | | 푸시명 |
| visibility | tinyint(1) | '0' | false | | 푸시 사용 여부 |
| pushAtType | [enum #5](#enum5) | 'designation' | false | | 푸시 날짜 유형 |
| repeat | [enum #6](#enum6) | 'once' | false | | 반복여부 |
| pushAt | datetime | NULL | true | | 푸시 날짜 |
| message | varchar(255) | NULL | true | | 푸시 메세지 |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| deletedAt | datetime | NULL | true | | 삭제날짜 |
| id | int(11) | | false | | row ID |



### ENUM
#### enum5
* 'birth','wedding','designation'

#### enum6
* 'years','once'

---
### ClassMethods

#### createPush 
 * 푸시 생성
 
#### updatePush 
 * 푸시 수정
 
#### findPushesByOptions 
 * 푸시 검색(옵션)

 
