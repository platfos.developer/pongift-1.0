# AppStoreImages


## DESC


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| storeId | int(11) | NULL | true | [AppStores](/markdown/db/AppStores.md).id | 매장 ID |
| imageId | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 이미지 ID |
| index | int(11) | | false | | 이미지 순서 |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| id | int(11) | | false | | row ID |


---
### ClassMethods

#### createStoreImage 
 * 매장 이미지 생성
 
#### updateStoreImage 
 * 매장 이미지 수정
 
#### deleteStoreImage 
 * 매장 이미지 삭제
 
#### findStoreImagesByStoreId 
 * 매장 이미지 검색(매장ID 기준)
 
#### findStoreImagesByOptions 
 * 매장 이미지 검색(매장/이미지 include)
 
 
