# AppThemes


## DESC


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자 ID |
| iconImgId | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 아이콘 이미지 ID |
| type | [enum #3](#enum3) | 'public' | true | | 테마 유형 |
| name | varchar(255) | | false | | 테마명 |
| visibility | tinyint(1) | '0' | false | | 테마 표시 유무 |
| order | tinyint(1) | '0' | false | | 상품 정렬구분 |
| weight | tinyint(1) | '0' | false | | 정렬순서 |
| createdAt | bigint(20) | NULL | true | | 생성 날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정 날짜 |
| deletedAt | datetime | NULL | true | | 삭제 날짜 |
| id | int(11) | | false | | row ID |



### ENUM
#### enum3
* 'personal','public'


### ClassMethod

#### createTheme
* 테마를 생성한다

#### updateThemeWithIconImage
* 해당 테마와 관계가 있는 테이블(상품, 이미지)를 삭제 후 다시 생성하고 테마정보를 업데이트한 후 테마정보를 리턴한다.

#### deleteThemeWithIconImage
* 해당 테마와 관계가 있는 테이블(상품, 이미지)를 삭제한다.

#### findThemesByOptions
* 옵션에 따라 테마를 검색한다.
  * 옵션 : 검색할 필드, 검색어, 정렬방식 등
  
#### findThemesByUser
* 옵션에 따라 테마를 검색한다.
  * 옵션 : 서비스아이디, 테마아이디



### 주의사항

```pre
- type
  - personal일 경우 SDK에서 해당 유저의 생일, 결혼기념일등 특별한 날짜에 노출
  - public일 경우 무조건 노출
```
