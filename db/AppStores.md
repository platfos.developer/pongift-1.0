# AppStores


## DESC


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자 ID |
| corporationId | int(11) | NULL | true | [AppCorporations](/markdown/db/AppCorporations.md).id | 법인 ID |
| name | varchar(255) | NULL | true | | 매장명 |
| address | varchar(255) | NULL | true | | 매장 주소 |
| location | point | NULL | true | | 위도 / 경도 |
| phoneNum | varchar(255) | NULL | true | | 매장 연락처 |
| info | varchar(1024) | NULL | true | | 매장 정보 |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| deletedAt | datetime | NULL | true | | 삭제날짜 |
| id | int(11) | | false | | row ID |


---
### ClassMethods

#### createStoreWithStoreImage 
 * 매장 생성
 
#### updateStoreWithStoreImages 
 * 매장 수정
 
#### deleteStoreWithStoreImages 
 * 매장 삭제
 
#### findStoresByOptions 
 * 매장 
 
