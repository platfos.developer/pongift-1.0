# AppCorporations


## DESC
```pre
  * 법인관리
   - 사용자 '법인'에 대한 정보이다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | authorId |
| corporationId | int(11) | NULL | true | [AppCorporations](/markdown/db/AppCorporations.md).id | corporationId |
| type | [enum #3](#enum3) | 'corporation' | false | | 법인 유형 |
| number | varchar(255) | | false | | 사업자번호 |
| name | varchar(255) | | false | | 법인명 |
| address | varchar(255) | NULL | true | | 주소 |
| leader | varchar(255) | NULL | true | | 대표 |
| condition | varchar(255) | NULL | true | | 업태 |
| category | varchar(255) | NULL | true | | 업종 |
| logoImgId | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 로고 이미지ID |
| state | [enum #11](#enum11) | 'standby' | false | | 계약상태 |
| contractedAt | datetime | NULL | true | | 계약일 |
| canceledAt | datetime | NULL | true | | 취소일 |
| country | varchar(255) | 'kr' | false | | 국가 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |



### ENUM
#### enum3
* 'corporation','personal'

#### enum11
* 'standby','contract','cancel'

 
 
### ClassMethods


#### createCorporation 
 * 법인 생성 
    1. Create : AppCorporation / AppUserManage 
    2. Build-save (Create) : Profile / User 
    3. upsert(update or create) : / Auth / Device

#### updateCorporationWithLogoImage
 * 법인 이미지로고를 업데이트한다. (기존로고이미지는 삭제)
 
#### deleteCorporationWithData
 * 해당하는 법인을 삭제한다. ( update - destory) 
 
#### findCorporationsByOptions 
 * 조건에 해당하는 법인을 조회


### 주의사항
```pre
- corporationsId 법인 아이디
  - 자기 참조ID (ex: 네이버 -> 라인 같이 법인끼리 부모자식관계가 형성될 때 사용)
- canceledAt
  - 관리자페이지 -> 법인관리 -> 상세보기 -> 수정 -> 왼쪽 상단 취소 선택 시 취소날짜 입력
```
