# AppBulkBarcodes


## DESC
```pre
  * 벌크바코드정보
   - 벌크로 가져오는 핀(바코드)번호에 대한 정보로 구매자에게 상품발송(문자)를 해주었는지, 권종별 재고수량 등을 파악할 수 있다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| productId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | productId |
| barcode | varchar(255) | | false | | 핀(바코드)번호 |
| expiryAt | datetime | NULL | true | | 유효기간 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |
| status | [enum #8](#enum8) | 'ready' | false | | 발급여부 |



### ENUM
#### enum8
* 'ready','pending','finish' 대기 / 처리 중 / 완료

---
### ClassMethods

#### createBulkBarcode 
 * 벌크 데이터 생성 
 
#### sendCultureland 
 * 벌크 주문시 발송 
 
#### updateBulkBarcodeFinish 
 * 벌크 완료 처리
 
#### findUpdateBulkBarcodeLimitOne 
 * 사용하지 않은 벌크 데이터 하나만 검색 후 진행중으로 업데이트 (테이블 락 사용)
 
#### countBulkBarcode 
 * 현재 남은 벌크 데이터 카운트 
 
 
