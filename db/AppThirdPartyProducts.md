# AppThirdPartyProducts


## DESC
```pre
  * 연동상품정보
   - 상품이 연동된 업체들의 상품정보이다.
     업체에 따라 판매중인 '상품','가격','이미지' 등이 다를 수 있다.
```


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | id |
| productId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | productId |
| thirdPartyId | varchar(255) | NULL | true | |  |
| name | varchar(255) | NULL | true | | 상품명 |
| type | varchar(255) | NULL | true | | 연동업체 |
| categoryId | int(11) | NULL | true | | categoryId |
| status | varchar(255) | NULL | true | | 사용여부 |
| image | varchar(255) | NULL | true | | 이미지URL |
| salePrice | int(11) | NULL | true | | 판매가격 |
| productOrderMax | int(11) | NULL | true | | 1회 최대구매수량 |
| isUse | tinyint(1) | '0' | true | | |
| createdAt | bigint(22) | | false | | 생성일 |
| updatedAt | bigint(22) | | false | | 수정일 |


### ClassMethod

#### findDataByThirdPartyId
* 타입과 써드파티 id로 검색한다.

#### findByThirdPartyProductId
* 타입과 써드파티 상품id로 검색한다.

#### createThirdPartyProduct
* 써드파티 상품을 생성한다.

#### deleteByThirdPartyId
* 써드파티 상품을 삭제한다.

#### updateThirdPartyProduct
* 써드파티 상품을 업데이트한다.
