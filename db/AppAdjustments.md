# AppAdjustments


## DESC
```pre
  *정산정보
   - 서비스 별로 '정산정보'를 확인할 수 있다. 
     법인계정 시 해당하는 '서비스 별'로 정산정보를 확인할 수 있다.
     서비스의 정산기준이 '사용일','판매일'에 따라 정산방법이 달라진다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | 서비스 ID |
| calculationId | int(11) | NULL | true | [AppCalculations](/markdown/db/AppCalculations.md).id | 정산정보 ID |
| transferAt | datetime | | false | | 입금일 |
| vatType | [enum #4](#enum4) | 'separate' | false | | VAT 유형 |
| state | [enum #5](#enum5) | 'beforeAuthorized' | false | | 정산여부 |
| count | int(11) | '0' | false | | 판매(사용)횟수 |
| totalMoney | int(11) | '0' | false | | 총 판매(사용)금액 |
| cancelMoney | int(11) | '0' | false | | 취소금액 |
| refundMoney | int(11) | '0' | false | | 환불금액 |
| chargeMoney | int(11) | '0' | false | | 수수료 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |



### ENUM
#### enum4
* 'separate (별도)', 'include (포함)'

#### enum5
* 'beforeAuthorized (정산대상)', 'beforeTransfer (정산승인)', 'complete (정산완료)'

---
### ClassMethods

#### initSupplyAdjustment 
 * 정산해야 될 구매상품의 서비스가 상품공급사인 경우 정산초기화작업을 한다. 
 * 정산하기 위한 row가 생성되어있다면 해당하는 row를 가져오고 없는 경우 생성한다. (findOne / create)

#### initDistributionAdjustment 
 * 정산해야 될 구매상품의 서비스가 유통대행사인 경우 정산초기화작업을 한다. 
 * 정산하기 위한 row가 생성되어있다면 해당하는 row를 가져오고 없는 경우 생성한다. (findOne / create
 
 
#### updateAdjustments 
 * [폰기프트]-[정산관리]페이지에서 사용한다. 
 * findAdjustments 클래스메소드를 사용하여 가져온 데이터들을 가지고 '정산대상'/'정산승인'의 상태 값을가지고 업데이트를 한다.
 
 
#### findAdjustmentsByOptions
 * 조건에 맞는 정산데이터들의 개수만큼 데이터를 가져온다.(count/limit/findAll)
 
 
#### countAllAdjustmentsByOptions 
 * 조건에 맞는 정산데이터들의 개수를 리턴한다.(count)
 
### 주의사항
```pre
- transferAt
  - 입금일 실제로 업체에 (지불/입금) 받는 날짜
  - 관리자페이지 -> 서비스 (등록/수정) 시 입력된다. 
- state
  - 관리자페이지 -> 정산 : 정산대상/정산처리/정산승인 구분 값에 사용된다.  
- cancelMoney
  - 1. 폰기프트 : 구입유저 취소 = 취소, 쿠폰을 받은유저가 취소 = 환불
  - 2. 스토어팜 : 구입유저가 상품을 받기 전 취소 = 취소, 구입유저가 상품을 받은 후 취소 = 환불
  - 3. 판매일기준 상품일 경우 취소 = 취소 
```
