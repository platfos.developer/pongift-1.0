# AppBookProductAuthorizations


## DESC
```pre
  * 상품예약 (상품관리 - 승인예약버튼)
   - 벌크로 상품의 상태값을 변경할 때 사용한다.
```


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| productId | int(11) | | false | [AppProducts](/markdown/db/AppProducts.md).id | productId |
| authorized | tinyint(1) | '0' | false | | 승인여부 |
| bookAt | datetime | | false | | 예약일 |
| processed | tinyint(1) | '0' | false | | 진행사항 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| id | int(11) | | false | | id |

---
### ClassMethods


#### createBookProductAuthorizationByCsv 
 * 승인(미승인)예약 할 상품들을 생성


#### deleteBookProductAuthorization
 * 생성된 승인(미승인)된 상품들을 삭제


#### findBookProductAuthorizationsByOptions
 * 조건에 해당하는 개수 만큼의 데이터를 조회 
 

#### applyProductAuthorizations
 * 조건에 해당하는 AppProducts 와 AppBookProductAuthorization을 찾아 업데이트


### 주의사항
```pre
- authorization 승인여부
  - 승인 = 판매, 미승인 = 판매중지
- processed 진행사항 
  - 승인 상태에서 승인예약일이 지나면 1로 업데이트 
```
