# AppPromotions


## DESC
- 상품에 대한 할인 지정 프로모션 저장 

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | |
| eventId | int(11) | NULL | true | [AppEvents](/markdown/db/AppEvents.md).id | 이벤트 ID (사용안함) |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | 서비스 ID |
| corporationId | int(11) | NULL | true | [AppCorporations](/markdown/db/AppCorporations.md).id | 법인 ID |
| type | [enum #5](#enum5) | 'discount' | false | | 유형 |
| serviceType | [enum #6](#enum6) | 'total' | false | | 프로모션 전시 서비스 유형 |
| productType | [enum #7](#enum7) | 'exchange' | false | | 상품 유형 |
| name | varchar(255) | | false | | 프로모션명 |
| memo | varchar(1024) | NULL | true | | 메모 |
| totalPurchaseCount | int(11) | NULL | true | | 전체 구매 횟수 한도 |
| purchaseCountTotal | int(11) | NULL | true | | 개인 전체 구매 횟수 한도 |
| purchaseCountAMonth | int(11) | NULL | true | | 개인 월 구매 횟수 한도 |
| purchaseCountADay | int(11) | NULL | true | | 개인 일 구매 횟수 한도 |
| totalPurchaseAmount | int(11) | NULL | true | | 전체 구매 수량 |
| purchaseAmountTotal | int(11) | NULL | true | | 개인 전체 구매 수량 한도 |
| purchaseAmountAMonth | int(11) | NULL | true | | 개인 월 구매 수량 한도 |
| purchaseAmountADay | int(11) | NULL | true | | 개인 일 구매 수량 한도 |
| purchaseAmountATry | int(11) | NULL | true | | 개인 1시도당 구매 수량 한도 |
| totalPurchasePrice | int(11) | NULL | true | | 전체 구매 금액 한도 |
| purchasePriceTotal | int(11) | NULL | true | | 개인 전체 구매 금액 한도 |
| purchasePriceAMonth | int(11) | NULL | true | | 개인 월 구매 금액 한도 |
| purchasePriceADay | int(11) | NULL | true | | 개인 일 구매 금액 한도 |
| purchasePriceATry | int(11) | NULL | true | | 개인 1시도당 구매 수량 한도 |
| startAt | datetime | | false | | 프로모션 시작 날짜 |
| endAt | datetime | | false | | 프로모션 끝 날짜 |
| sdkBannerImgId1 | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 프로모션 배너 이미지 |
| sdkBannerImgId2 | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 사용안함 |
| webBannerImgId1 | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 사용안함 |
| webBannerImgId2 | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 사용안함 |
| visibility | tinyint(1) | '0' | false | |프로모션 표시 유무 |
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| deletedAt | datetime | NULL | true | | |
| id | int(11) | | false | | |



### ENUM
#### enum5
* 'discount','present'
     할인    /   증정
#### enum6
* 'total','supply','distribution','partial'
   전체 / 서비스 공급사만 / 유통대행사만 / 서비스 공급사 + 유통대행사만
#### enum7
* 'exchange','cost'
    교환     / 금액
    
---
### ClassMethods

#### findPromotionsByEventId 
 * 프로모션 eventID로 검색
 
#### createPromotion 
 * 프로모션 생성
 
#### updatePromotion 
 * 프로모션 수정
 
#### updatePromotionVisibility 
 * 프로모션 표기 여부 업데이트
 
#### deletePromotion 
 * 프로모션 삭제
 
#### findPromotionsByOptions 
 * 프로모션 검색(옵션)
 
#### findPromotionsByUser 
 * 프로모션 유저기준 검색
