# AppMessage


## DESC
- 발송메시지 큐 


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| productTransferId | int(11) | NULL | true | [productTransfer](/markdown/db/AppProductTransfers.md).id | |
| state | ENUM | 'notSend' | false | | "notSend", "send", "ing", "error" |
| from | varchar(255) |  | false | | 보내는 사람 전화번호 |
| to | varchar(255) |  | false | | 받는 사람 전화번호 |
| title | varchar(255) | | true | | 메시지 타이틀 |
| content | text | '100' | true | | 메시지 텍스트 |
| errorCount | int(11) |  | false | | 에러 횟수 |
| filePath | varchar(255) |  | false | | 파일 경로 |
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| deletedAt | datetime | NULL | true | | |
| id | int(11) | | false | | |

---
### ClassMethods


#### push
##### AppMessage에 데이터 추가

#### excute
##### AppMessage의 발송되지 않은 메시지 발송