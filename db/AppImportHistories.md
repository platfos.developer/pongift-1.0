# AppImportHistories


## DESC
```pre
  * CSV import
   - CSV 파일 업로드 시 사용한다. (상품등록 시 파일업로드)
     파일 업로드 시 '에러' 및 '진행률'등을 알 수있다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users]().id | authorId |
| fileName | varchar(255) | | false | | 파일명 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| progress | int(11) | NULL | true | | CSV import 진행률 |
| errorCode | varchar(255) | NULL | true | | 에러코드 |
| failIndex | longtext, | | true | | 업로드 실패 index번호 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |
