# AppThirdPartyProductSync


## DESC
```pre
  * 연동상품상태확인
   - 옴니텔 윈큐브의 상품 확인 ( 가격, 판매중 ), 현재 게시된 상품 정보와 다를 경우 해당 테이블에 등록 admin->상품싱크 화면에 보여줌
   - 해당 정보는 15분 마다 갱신됨
```


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | id |
| productId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | productId |
| price | int(11) | NULL | true | | 상품가격 |
| name | varchar(255) | NULL | true | | 상품명 |
| externalCode | varchar(255) | NULL | true | | 연동코드 |
| serviceName | varchar(255) | NULL | true | | 서비스이 |
| type | varchar(255) | NULL | true | | omnitel/wincube |
| service | varchar(255) | NULL | true | | storefarm/auction |
| createdAt | bigint(22) | | false | | 생성일 |
| updatedAt | bigint(22) | | false | | 수정일 |


### ClassMethod

#### findDataByThirdPartyId
* 타입과 써드파티 id로 검색한다.

#### findByThirdPartyProductId
* 타입과 써드파티 상품id로 검색한다.

#### createThirdPartyProduct
* 써드파티 상품을 생성한다.

#### deleteByThirdPartyId
* 써드파티 상품을 삭제한다.

#### updateThirdPartyProduct
* 써드파티 상품을 업데이트한다.
