# AppCalculations


## DESC
```pre
  * 서비스 정산정보
   - 각 서비스 별 정산정보이다. 
     서비스 별로 '수수료','정산일','수수료 상태'등이 다르다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | aythorId |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | serviceId |
| chargeState | [enum #3](#enum3) | 'supply' | false | | 수수료 상태 |
| chargeType | [enum #4](#enum4) | 'chargeTypeTotal' | false | | 수수료 유형 |
| charge | float | NULL | true | | 수수료 |
| period | [enum #6](#enum6) | 'onceAMonth' | false | | 정산주기 |
| startDay | int(11) | '1' | false | | 정산 시작일 |
| transferDay | int(11) | '1' | false | | 입금일 |
| bookAt | datetime | NULL | true | | 예약일 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |



### ENUM
#### enum3
* 'supply','distribution'

#### enum4
* 'chargeTypeTotal','chargeTypeSeparate'

#### enum6
* 'onceAMonth','twiceAMonth'

---
### ClassMethods


#### createCalculation 
 * user의 권한/userManage 정보를 가지고 정산정보 생성
 
 
#### updateCalculation 
 * user의 권한/userManage 정보를 가지고 정산정보 업데이트
 
 
#### deleteCalculation
 * 해당하는 정산정보 삭제 (삭제를 바로해도되지만 업데이트 후 삭제를 한다.)
   1) bookAt은 serviceId와 복합키로 걸려있다. destory를 써서 삭제를 해도 Row가 남아있기 때문에 해당 값을 Null로 만든 후 삭제하는 것 ( deleteAt값이 있으면 삭제로 간주함 'paranoid': true )
   
   
#### findCalculationsByOptions
 * 조건에 해당하는 개수만큼 정산정보를 가져온다.



### 주의사항
```pre
- chargeState 수수료 상태
  - 확장성 고려를 위해 만들어진 필드 (확장성: 상품공급사가 유통대행사로 변경될 경우 서비스는 나누고 cal의 데이터만 수정하여 적용)
```
