# AppProductPromotions


## DESC
- 프로모션 등록 상품 관리
  promotion의 상세 데이터 이며 상품을 fk로 연결된다.
  promotion에서 지정한 전체 수량을 구분하기 위해 상품별로 판매 갯수 기록 

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | |
| productId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | |
| presentId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | 사용 안함 |
| promotionId | int(11) | NULL | true | [AppPromotions](/markdown/db/AppPromotions.md).id | |
| discount | int(11) | NULL | true | | 할인액 |
| totalAmount | int(11) | '0' | false | | 전체수량 |
| totalCount | int(11) | '0' | false | | 전체횟수 |
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| id | int(11) | | false | | |


---
### ClassMethods

#### createProductPromotion 
 * 상품 프로모션 생성
 
#### updateProductPromotion 
 * 상품 프로모션 수정
 
#### deleteProductPromotion 
 * 상품 프로모션 삭제
 
#### findProductPromotionsByOptions 
 * 상품 프로모션 검색(옵션)
