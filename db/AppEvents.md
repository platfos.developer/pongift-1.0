# AppEvents


## DESC
```pre
  * 이벤트 (현재 사용안함)
   - 이벤트에 대한 정보이다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [users]().id | authorId |
| name | varchar(255) | | false | | 이벤트명 |
| visibility | tinyint(1) | '0' | false | | 이벤트 표시 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |


---
### ClassMethods


#### createEvent 
 * 이벤트 생성
 
#### updateEvent
 * 이벤트 업데이트
 
#### deleteEvent
 * 해당되는 이벤트 삭제 (update - destroy)
 
#### findEventsByOptions
 * 조건에 해당하는 개수만큼 이벤트 데이터 조회 ( limit)
