# Profiles


## DESC
```pre
 * 유저프로필
  - 사용자가 현재 서비스를 이용하고 있는지의 여부(state)와 기념일 알림을(wedding) 위한 정보들이다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| state | [enum #1](#enum1) | 'normal' | false | | 유저상태 |
| wedding | datetime | NULL | true | | 결혼여부 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |



### ENUM
#### enum1
* 'normal','active','sleep','leave'

