# AppNonmembers


## DESC
- 비회원 / 서드파티 ( 스토어팜 ) 구매일경우 구입자의 정보를 저장
- purchase에 구매자로 fk 연결됨

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| type | varchar(255) | NULL | true | | 구매 타입(서비스 명 ex: storefarm) |
| serviceId | int(11) | | false | [AppServices](/markdown/db/AppServices.md).id | 구매 서비스|
| phoneNum | varchar(255) | NULL | true | | 구매자 전화번호 |
| serviceUid | varchar(255) | | false | | 구매자 ID |
| name | varchar(255) | NULL | true | | 구매자 이름 |
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| deletedAt | datetime | NULL | true | | |
| id | int(11) | | false | | |

---
### ClassMethods

#### upsertNonmember
 * client에서 비회원 가입시에 사용 (/api/client/nonmember)
 

#### findDataByPhoneNumUidName
 * Phone / Uid(서비스의 고유 번호) / Name 으로 nonmember 검색 
 
#### createNonmember
 * 고유 nonmember 생성 
