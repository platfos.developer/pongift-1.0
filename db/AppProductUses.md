# AppProductUses


## DESC
```pre
  * 금액권 관리 (현재 금액권 상품이 없어 사용하지 않는다.)
   - 금액권 상품에 대한 관리를 한다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| purchaseId | int(11) | NULL | true | [AppPurchases](/markdown/db/AppPurchases.md).id | purchaseId |
| location | varchar(255) | | false | | 사용장소 |
| usedAt | datetime | | false | | 사용일자 |
| usePrice | int(11) | NULL | true | | 사용금액 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |
