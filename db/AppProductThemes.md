# AppProductThemes


## DESC
- 테마 적용 상품 관리 (sdk에서 사용)

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자 ID |
| productId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | 상품 ID |
| themeId | int(11) | NULL | true | [AppThemes](/markdown/db/AppThemes.md).id | 테마 ID |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| id | int(11) | | false | | row ID |

---
### ClassMethods

#### createProductTheme
 * 상품 테마 생성
  
#### updateProductTheme
 * 상품 테마 수정
  
#### deleteProductTheme
 * 상품 테마 삭제
  
#### findProductThemesByOptions
 * 상품 테마 검색(옵션)
