# AppServiceSessions


## DESC
```pre
  * 서비스세션
   - 서비스 세션정보로 '쿠키' 및 '세선아이디' 등의 정보가 있다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| cookie | varchar(255) | NULL | true | | 쿠키값 |
| expiration | bigint(20) | NULL | true | | 만료정보 |
| serviceId | varchar(255) | NULL | true | | serviceId |
| uid | varchar(255) | NULL | true | | 세션아이디 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |


---
### ClassMethods

#### setServiceSession 
 * 클라이언트 비회원 일 경우 해당 세션 저장(sdk로 들어온 다른 회원일 경우 세션 저장)
 
 
