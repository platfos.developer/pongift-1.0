# AppProductTransfers


## DESC
```pre
  * 상품(쿠폰)정보
   - 상품(쿠폰)에 대한 정보이다.
     상품(쿠폰)의 이력(사용/취소/환불)을 볼 수 있으며, 
     사용 시 사용정보 
     환불 시 환불정보
     재발송 및 연장 시 정보 등이 있다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | id |
| purchaseId | int(11) | NULL | true | [apppurchases]().id | purchaseId |
| state | [enum #3](#enum3) | 'disuse' | false | | 쿠폰상태 |
| sendState | [enum #4](#enum4) | 'notSend' | false | | 전송여부 |
| destination | varchar(255) | | false | | 수신인(휴대폰번호) |
| externalCode | varchar(255) | NULL | true | | 외부코드(쿠폰번호) |
| externalTransactionCode | varchar(255) | NULL | true | | 외부 transactionCode |
| exchangeTransactionCode | bigint(20) | NULL | true | | 외부 상품교환코드 |
| charge | float | NULL | true | | 수수료 |
| distributionCharge | float | NULL | true | | 유통대행수수료 |
| amount | int(11) | '1' | false | | 상품 수량 |
| usedAt | datetime | NULL | true | | 사용일 |
| useCanceledAt | datetime | NULL | true | | 사용취소날짜 |
| canceledAt | datetime | NULL | true | | 취소가능날짜 |
| usedLocationCode | varchar(255) | NULL | true | | 사용장소 코드 |
| usedLocation | varchar(255) | NULL | true | | 사용장소 |
| refundStartedAt | datetime | NULL | true | | 환불처리 시작날짜 |
| refundFinishedAt | datetime | NULL | true | | 환불처리 완료날짜 |
| refundRate | float | NULL | true | | 환불률 |
| refundPrice | int(11) | NULL | true | | 환불금액 |
| refundBank | [enum #21](#enum21) | NULL | true | | 환불계좌은행 |
| refundDepositor | varchar(255) | NULL | true | | 환불 예금주 |
| refundAccount | varchar(255) | NULL | true | | 환불 계좌번호 |
| expiryAt | datetime | NULL | true | | 유효기간 |
| extendExpiryDayCount | int(11) | '0' | false | | 유효기간 연장횟수 |
| recentExtendExpiryDayAt | datetime | NULL | true | | 최근 유효기간 연장일 |
| reTransferCount | int(11) | '0' | false | | 재전송 횟수 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |



### ENUM
#### enum3
* 'disuse','use','cancel','refundStart','refundFinish','partial'
    미사용 /  사용 /  취소   / 환불처리중     /    환불완료    / 부분 사용

#### enum4
* 'send','notSend'
    발송 / 미발송 
    
#### enum21
* 'KEB하나','외환(하나','국민','농협','우리','신한','기업','경남','광주','대구','도이치','부산','산업','상호저축','새마을금고','수협중앙회','신용협동조합','우체국','전북','제주','한국씨티','BOA','HSBC','JP모간','SC','교보증권','대신증권','대우증권','동부증권','유안타증권','메리츠증권','미래에셋','부국증권','삼성증권','신영증권','신한금융투자','NH투자증권','유진증권','키움증권','하나금융투자','하이투자증권','한국투자','한화투자증권','현대증권','이베스트투자증권','HMC증권','LIG증권','SK증권','산림조합','중국공상은행','BNP파리바은행','KB투자증권','펀드온라인코리아'


---
### ClassMethods

#### createProductTransfer 
 * 상품 발송 생성 
  
#### updateProductTransfer 
 * 상품 발송 생성 
  
#### updateProductTransferRetransferByNonmember 
 * nonmember 주문 재전송시 업데이트 
  
#### updateProductTransferRetransfer 
 * 일반 회원 주문 재전송시 업데이트 
  
#### updateProductTransferExchange 
 * 주문 정산 
  
#### deleteProductTransfer 
 * 상품 발송 데이터 삭제 
  
#### findProductTransfersByOptions 
 * 상품 발송 검색 (옵션)
  
#### getUserDashboard 
 * 대시보드 유저통계
 
#### getDistributionDashboard 
 * 대시보드 유통대행 통계
  
#### getCategoryDashboard 
 * 대시보드 카테고리 통계
 
#### getProductDashboard 
 * 대시보드 상품 통계
 
#### getDateDashboard 
 * 대시보드 기간별 통계
  
#### findProductTransferByPurchaseId 
 * purchaseId 로 검색 
  
#### findDataByOmnitelInfo 
 * 어드민 사용자 옴니텔 주문 내역 
 
#### updateOptionProductTransfer 
 * 상품 발송 업데이트(옵션)
 
#### updateSmartInfiniProductTransferExchange 
 * 스마트인피니 정산
 
#### GETupdateProductTransferExchange 
 * 주문정산(GET호출로 강제 정산시 사용)
 
