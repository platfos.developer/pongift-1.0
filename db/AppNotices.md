# AppNotices


## DESC
- 공지사항 관리

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | |
| corporationId | int(11) | NULL | true | [AppCorporations](/markdown/db/AppCorporations.md).id | |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | |
| type | [enum #4](#enum4) | 'etc' | false | | 공지 유형 |
| target | [enum #5](#enum5) | 'user' | false | | 공지 대상 |
| title | varchar(255) | NULL | true | | 공지 제목 |
| content | varchar(1024) | NULL | true | | 공지 내용|
| country | varchar(255) | | false | | 국가 |
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| deletedAt | datetime | NULL | true | | |
| id | int(11) | | false | | |



### ENUM
#### enum4
* 'check','product','disability','security','etc'
   서비스점검 / 상품   / 서비스 장애   / 보안      / 기타

#### enum5
* 'admin','user'

---
### ClassMethods

#### createNotice 
 * 새로운 알림 추가 
  
#### updateNotice 
 * 알림 수정
  
#### deleteNotice 
 * 알림 삭제
 
#### findNoticesByOptions 
 * 알림 조건 검색
 
