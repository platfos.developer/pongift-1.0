# AppProducts


## DESC
```pre
  * 상품관리
   - 상품에 대한 기본정보이다. 
     (업체에 따라 상품정보가 다르게 나타난다. AppThirdPartyProducts : 현재 네이버스토어팜만 사용)
     유효기간 방식(구매일로 부터 / 기간(~일 까지))에 따라 유효기간 정보가 다를 수 있다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | id |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | authorId |
| serviceId | int(11) | NULL | true | | serviceId |
| categoryId | int(11) | NULL | true | [AppCategories](/markdown/db/AppCategories.md).id | categoryId |
| state | [enum #5](#enum5) | 'unauthorized' | false | | 상품 상태 |
| type | [enum #6](#enum6) | 'exchange' | false | | 상품 유형 |
| weight | int(11) | '0' | false | | 노출빈도 |
| externalCode | varchar(255) | NULL | true | | 외부코드(상품공급사의 연동코드) |
| name | varchar(255) | | false | | 상품명 |
| tax | [enum #10](#enum10) | NULL | true | | 과세여부 |
| optionName | varchar(255) | NULL | true | | 옵션명 |
| info1 | varchar(1024) | NULL | true | | 교환처 |
| info2 | varchar(1024) | NULL | true | | 상품정보 |
| info3 | varchar(1024) | NULL | true | | 제한사항 |
| info4 | varchar(1024) | NULL | true | | 주의사항 |
| info5 | varchar(1024) | NULL | true | | 안내사항 |
| memo | varchar(1024) | NULL | true | | 안내사항 |
| expiryDay | int(11) | | false | | 유효기간(옴니텔_일수) |
| endDay | varchar(45) | NULL | true | | 유효기간(스마트인피니_날짜) |
| authorizedAt | datetime | NULL | true | | 승인일 |
| storeFarmType | [enum #20](#enum20) | 'del' | true | | 스토어팜 연동여부 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |



### ENUM
#### enum5
* 'authorized','unauthorized'

#### enum6
* 'exchange','cost'

#### enum10
* 'taxFree','tax'

#### enum20
* 'use','hidden','del'


---
### ClassMethods

#### createProductSimple 
 * 상품 생성
 
#### createProductWithData 
 * 상품 생성(이미지 포함)
 
#### updateProductWithData 
 * 상품 수정
 
#### deleteProductWithData 
 * 상품 삭제
 
#### findProductsByOptions 
 * 상품 검색(옵션)

#### searchProductsByOptions 
 * 클라이언트 상품 검색(옵션)

#### findProductsByPromotion 
 * 상품 프로모션 검색(옵션)
 
#### findProductsByTheme 
 * 상품 테마 검색(옵션)


### 궁금
```pre
- weight 노출빈도
  - SDK에서 상품노출 정렬에 사용된다.
- externalCode 외부코드(상품공급사의 연동코드)
  - 외부연동 시 상품고유번호 (ex: 옴니텔에서 제공한 제품 ID)
- endDay 유효기간
  - 스마트인피니에서 제공한 쿠폰 사용 날짜 ( 2017.12.01 )
- expiryAt
  - 쿠폰 제공처에서 제공한 사용 가능 날짜 ( 발매일 기준 100일 후 )
```
