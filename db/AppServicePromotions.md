# AppServicePromotions


## DESC


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자 ID |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | 서비스 ID |
| promotionId | int(11) | NULL | true | [AppPromotions](/markdown/db/AppPromotions.md).id | 프로모션 ID |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| id | int(11) | | false | | row ID |

---
### ClassMethods

#### createServicePromotion 
 * 서비스프로모션 생성
 
#### updateServicePromotion 
 * 서비스프로모션 수정
 
#### deleteServicePromotion 
 * 서비스프로모션 삭제
 
#### findServicePromotionsByOptions 
 * 서비스프로모션 검색 (옵션)
 
