# AppServiceStores
 * 매장과 서비스 연결 브릿지 테이블

## DESC


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자 ID |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | 서비스 ID |
| storeId | int(11) | NULL | true | [AppStores](/markdown/db/AppStores.md).id | 매장 ID |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| id | int(11) | | false | | row ID |


---
### ClassMethods

#### createServiceStore 
 * 서비스 스토어 브릿지 생성
 
#### updateServiceStore 
 * 서비스 스토어 브릿지  수정
 
#### deleteServiceStore 
 * 서비스 스토어 브릿지  삭제
 
#### findServiceStoresByOptions 
 * 서비스 스토어 브릿지  검색 (옵션)
 
