# AppUserManages


## DESC
```pre
  * 권한정보
   - 관리자 별 권한을 설정하기위한 테이블이다. 
     관리자 별(일반유저/법인ID/서비스ID)에 따라 접근할 수 있는 페이지,권한 등이 다르다.
```


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자ID |
| userId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 유저ID (대상) |
| corporationId | int(11) | NULL | true | [AppCorporations](/markdown/db/AppCorporations.md).id | 법인ID (법인 관리자인 경우) |
| serviceId | int(11) | NULL | true | [AppServices](/markdown/db/AppServices.md).id | 서비스ID (서비스 관리자인 경우) |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| id | int(11) | | false | | id |


---
### ClassMethod

#### findUserCountByOptions
* 옵션에 따라 유저 카운트를 검색한다.
    * 옵션: 검색어, 권한(서비스유형 or 권한 Enum)

#### findUserByOptions
* 옵션에 따라 유저들을 검색한다.
    * 옵션: 검색어, 권한(서비스유형 or 권한 Enum), 생성일

#### findUserManagesByOptions
* 옵션에 따라 유저들을 검색한다.
    * 옵션: 유저id, 법인id, 서비스id

#### findUserManageByUserId
* 유저id로 유저를 검색한다.

#### upsertUserManage
* 유저 권한을 업데이트/생성 한다.

#### deleteUserManage
* 유저권한을 일반유저로 업데이트 후 유저매니저를 삭제한다.
