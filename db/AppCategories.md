# AppCategories


## DESC
```pre
  * 서비스/상품 카테고리
   - 서비스/상품 별 카테고리를 설정한다.
     카테고리 별로 상품정렬 및 조회 등을 할 수 있게 해주며,
     특정업체(네이버스토어팜)에 해당하는 카테고리(code/depth)를 설정하기도 한다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | authorId |
| type | [enum #2](#enum2) | 'service' | false | | 카테고리 유형 |
| name | varchar(255) | | false | | 카테고리명 |
| visibility | tinyint(1) | '0' | false | | 카테고리 표시 |
| code | varchar(255) | NULL | true | | 카테고리 코드 |
| depth | varchar(255) | NULL | true | | 카테고리 범주 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |



### ENUM
#### enum2
* 'service','product','storeFarm'


---
### ClassMethods


#### createCategory 
 * 카테고리 생성
 
#### updateCategory
 * 조건에 해당하는 카테고리를 업데이트
 
#### updateCategoryVisibility
 * 해당하는 카테고리의 업데이트 (플래그 : 화면에 보일지 / 안보일지)
 
#### deleteCategory
 * 해당하는 카테고리 삭제 (업데이트 name : 'delete_id'~ / 삭제 : deleteAt)
 
#### findCategoriesByOptions
 * 조건에 해당하는 카테고리 조회 (count / findAll)

#### findProductCategoriesByUser
 * 조건에 해당하는 상품카테고리 조회
 
#### findServiceCategoriesByUser
 * 조건에 해당하는 서비스카테고리 조회

