# AppAffiliations


## DESC
```pre
  * 제휴사
   - 제휴업체정보로 '생성일','제휴사명'을 알려준다.
```


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [users]().id | 작성자ID |
| name | varchar(255) | | false | | 제휴사명 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |



---
### ClassMethods

#### createAffiliation 
 * 유저의 권한에 따라 제휴사 생성 ( 권한 => roleF)


#### updateAffiliation
 * 조건(id)에 해당하는 제휴사정보를 수정한다.
 
 
#### deleteAffiliation
 * 조건(id)에 해당하는 제휴정보를 수정한다. (삭제표시로 수정 : name = 'delete_id ~')
 ```pre
 function deleteAffiliation (t) {
     return sequelize.models.AppAffiliation.update({
         name: STD.common.deletedRowPrefix + affiliation.id + "_" + affiliation.name
     }, {
         where: {
             id: reqId
         },
         transaction: t
     }).then(function () {
         return sequelize.models.AppAffiliation.destroy({
             where: {
                 id: reqId
             },
             cascade: cascade,
             transaction: t
         }).then(function () {
             transaction = true;
         });
     });
 }
 ```
 * 업데이트하고 삭제하는 이유는 paranoid로 인해 실제 데이터는 삭제되지 않고 남아있고 다음 데이터 입력 유니크 에러가 날 수 있기 떄문에 name을 업데이트 후 삭제처리 만일 업데이트가 아니라면 복합유니크키로 deletedAt과 name을 걸어주면 된다. (deletedAt은 삭제에만 일어나기때문에 )
 
 
 
 
#### findAffiliationsByOptions
 * 조건에 해당되는 개수만큼 데이터를 조회
