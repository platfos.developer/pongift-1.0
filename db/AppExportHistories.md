# AppExportHistories


## DESC
```pre
  * CSV Export
   - CSV 파일로 다운로드 할 때 사용한다.(거래내역조회 시 파일받기 등...)
     CSV 파일 다운로드 시 '진행률' 및 '에러 내용'등을 알려준다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| fileName | varchar(255) | NULL | true | | 파일명 |
| progress | int(11) | '0' | false | | csv export 진행률 |
| errorCode | varchar(255) | NULL | true | | 에러 코드 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | |  삭제일 |
| id | int(11) | | false | | id |
