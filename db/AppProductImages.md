# AppProductImages


## DESC
- 상품 이미지 

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| productId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | 상품 ID |
| imageId | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 이미지 ID |
| index | int(11) | | false | | |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| id | int(11) | | false | | row ID |


---
### ClassMethods

#### createProductImage 
 * 상품 이미지 생성
 
#### updateProductImage 
 * 상품 이미지 수정
 
#### findProductImagesByProductId 
 * 상품 이미지 검색(productID 기준)
 
#### findProductImagesByOptions 
 * 상품 이미지 검색(옵션)


### 주의사항

```pre
- index
  - z-index와 같은 개념으로 사용
```
