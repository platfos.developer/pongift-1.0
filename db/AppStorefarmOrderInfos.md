# AppStorefarmOrderInfos


## DESC
- 스토어팜의 주문 정보 

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | |
| orderId | varchar(255) | NULL | true | |  주문ID |
| productOrderId | varchar(255) | NULL | true | | 상품 주문 ID|
| productOrderStatus | varchar(255) | NULL | true | | 상품 주문 상태  |
| state | varchar(255) | NULL | true | | 처리 상황 |
| orderType | varchar(255) | NULL | true | | 주문 타입 (normal / gift) |
| giftReceivingStatus | varchar(255) | NULL | true | | 선물하기 상태 |
| paymentDate | varchar(255) | NULL | true | | 결제일 |
| claimStatus | varchar(255) | NULL | true | | 클래임상태 |
| refundStandbyStatus | varchar(255) | NULL | true | | 환불 상태 |
| orderName | varchar(255) | NULL | true | | 주문자 이름 |
| ordererId | varchar(255) | NULL | true | | 주문자 ID|
| productID | varchar(255) | NULL | true | | 상품 ID |
| tel | varchar(255) | NULL | true | | 주문자 전화번호 |
| externalCode | varchar(255) | NULL | true | | 주문 바코드 |
| quantity | int(11) | '1' | false | | 주문 갯수 |
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| deletedAt | datetime | NULL | true | | |


---
### ClassMethods

#### findByOrderId / findByProductOrderId / findByExternalCode / findByOrderIdCount
 * 각 조건별 검색 
 
#### findByProductOrderIdCount
 * ProductOrderId 카운트 리턴
 
#### findByProductOrderPayedToWait
 * giftReceivingStatus과 tel 이 null인 데이터 하나만 리턴 (아직 주문 진행이 되지 않은 데이터) - 이전 api 에서 사용(하나씩 처리)
 
#### findByProductOrderPayedToWaitAll
 * giftReceivingStatus과 tel과 state이 null인 데이터들을 state를 ing로 업데이트 하고 리턴  (최신 버전에서 사용)

#### findByProductOrderPayedToGift
 * giftReceivingStatus과 tel과 giftReceivingStatus이 null인 데이터들 리턴  (스토어팜 선물하기 데이터)
 
#### createStoreFarmOrderInfo
 * 스토어팜 주문 데이터 생성 
 
#### updateStoreFarmOrder
 * 스토어팜 주문 데이터 업데이트 (id기준)
 
#### updateStoreFarmOrderByOrderId
 * 스토어팜 주문 데이터 업데이트 (orderID 기준) - 환불/ 취소시 사용
 
#### findDuplicateTno
 * purchase의 tno 중복 탐색 (주문 생성시 중복 체크)

#### storeFarmOrderRequest
 * 스토어팜 주문 저장 크론 실행 함수
  
#### findNullTnoBefore30Min
 * 10분 동안 주문이 완료되지 않은 주문 초기화

#### storeFarmOrderDelivery
 * 스토어팜 주문 크론 실행 함수
 
 
#### storeFarmGiftOrder
 * 스토어팜 선물하기 주문 크론 실행 함수
 
 
#### updateStoreFarmOrderList
 * 내장 함수 - 주문 업데이트  
  
#### findThirdPartyProduct
 * 내장 함수 - 스토어팜 상품 검색   
  
#### findThirdPartyProduct
 * 내장 함수 - 스토어팜 상품 검색 

#### findProduct
 * 내장 함수 - 상품 검색 

#### uniqueCheckPurchaseTno
 * 내장 함수 - purchase tno 검색

#### reqPurchase
 * 내장 함수 - purchase 생성 (쿠폰 발급)

#### requestProductOrderConfirm
 * 내장 함수 - 스토어팜 발송완료 처리 

#### modifyECoupon
 * 내장 함수 - 스토어팜 선물하기 완료 처리 
