# AppPurchases


## DESC
```pre
  * 구매관리
   - 상품 구매 시 구매정보이다. 
     현재 '소비자 가격', '공급 가격' , '실 거래 가격'은 구매 당시 가격이다.
     (폰기프트페이지 가격을 기준으로 한다. 즉, 네이버 스토어팜에서 할인 판매하였어도 '폰기프트페이지' 기준으로 구매가격이 표시된다.)
     
     AppPurchases테이블의 externalCode는 '외부연동 주문번호'이다. 
     AppProductTransfers의 externalCode는 '쿠폰(바코드)번호'이다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | id |
| userId | int(11) | NULL | true | [users]().id | userId |
| nonmemberId | int(11) | NULL | true | [appnonmembers]().id | nonmemberId |
| serviceId | int(11) | NULL | true | [appservices]().id | serviceId |
| productId | int(11) | NULL | true | [appproducts]().id | productId |
| promotionId | int(11) | NULL | true | [apppromotions]().id | promotionid |
| productPromotionId | int(11) | NULL | true | [appproductpromotions]().id | productPromotionId |
| themeId | int(11) | NULL | true | [appthemes]().id | themeId |
| state | [enum #9](#enum9) | 'standby' | false | | 구매상태 |
| type | [enum #10](#enum10) | NULL | true | | 구매유형 |
| sendMethod | [enum #11](#enum11) | 'sms' | false | | 전송방법 |
| charge | float | '0' | false | | 구매 당시 상품공급사 수수료 |
| distributionCharge | float | NULL | true | | 구매 당시 유통대행사 수수료 |
| customerPrice | int(11) | | false | | 구매 당시 소비자 가격 |
| supplyPrice | int(11) | | false | | 구매 당시 공급 가격 |
| realPrice | int(11) | | false | | 구매 당시 실 거래 가격 |
| vatType | [enum #17](#enum17) | 'separate' | false | | 구매 당시 VAT |
| message | varchar(255) | NULL | true | | 메세지 |
| purchasedAt | datetime | NULL | true | | 구매일 |
| purchaseCode | varchar(255) | | false | | 주문번호 |
| purchaseAuthorizationCode | varchar(255) | NULL | true | | 주문 승인 번호(KCP) |
| tno | varchar(255) | NULL | true | | 결제 번호(KCP) |
| cash_no | varchar(255) | NULL | true | | cash_no |
| cash_authno | varchar(255) | NULL | true | | cash_authno |
| externalCode | varchar(255) | NULL | true | | 외부 연동 주문 번호(쿠폰번호X) |
| canceledAt | datetime | NULL | true | | 구매취소 가능 일 |
| cancelCode | varchar(255) | NULL | true | | 구매취소코드 |
| cancelAuthorizationCode | varchar(255) | NULL | true | | 구매 취소 승인번호 |
| canCancelAt | datetime | NULL | true | | 구매취소일 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |



### ENUM
#### enum9
* 'standby','purchase','cancel' 대기 / 구매 / 취소

#### enum10
* 'card','phone','account','withoutPassbook','point','phoneEasyPayment','cardEasyPayment' 
카드 / 핸드폰 / 계좌 / 무통장입금 / 포인트 / 핸드폰간편결제 / 카드간편결제

#### enum11
* 'sms','lms','mms'

#### enum17
* 'separate','include' 수수료 별도 / 수수료 포함

---
### ClassMethods

#### createPurchase 
 * 주문 생성
 
#### updatePurchase 
 * 주문 수정
 
#### updateOmnitelData 
 * 옴니텔 주문 수정(상태/상품기간)
 
#### deletePurchase 
 * 주문 삭제
 
#### deletePurchaseById 
 * 주문 번호로 삭제 
 
#### deleteOldPurchases 
 * 주문 삭제 (크론) - 10시간 이상 주문대기 삭제 

#### cancelPurchase 
 * 주문 취소
 
#### findPurchasesByOptions 
 * 주문 검색(옵션)
 
#### findPurchasesByOptionsInNonmember 
 * 주문 검색(옵션) - 비회원
 
#### countAllPurchasesByOptions 
 * 주문 개수 
 
#### findPurchaseByPurchaseAuthorizationCode 
 * 주문 검색 (PurchaseAuthorizationCode  기준)
 
#### updateOmnitelCouponNumById 
 * 쿠폰번호 업데이트(purchaseId 기준)

#### createSinglePurchase 
 * 스마트인피니 주문 생성
 
#### SmartInfiniUpdatePurchase 
 * 스마트인피니 kcp 주문 생성 

 
 
