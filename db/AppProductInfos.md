# AppProductInfos


## DESC
- 상품 상세 정보
  예약일에 따라 해당 예약일 부터 가격이 변동된다.

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | |
| productId | int(11) | NULL | true | [AppProducts](/markdown/db/AppProducts.md).id | |
| charge | float | '0' | false | | 수수료 |
| customerPrice | int(11) | | false | | 소비자가격 |
| supplyPrice | int(11) | | false | | 공급가격 |
| realPrice | int(11) | | false | | 실거래가격 |
| vatType | [enum #7](#enum7) | 'separate' | false | | VAT 유형|
| bookAt | datetime | NULL | true | | 예약일 |
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| deletedAt | datetime | NULL | true | | |
| id | int(11) | | false | | |



### ENUM
#### enum7
* 'separate','include'
  별도       /   포함

---
### ClassMethods

#### createProductInfo 
 * 상품 정보 생성

#### updateProductInfo 
 * 상품 정보 수정
 
#### deleteProductInfo 
 * 상품 정보 삭제
 
#### findProductInfosByOptions 
 * 상품 정보 검색(옵션)
 
#### findProductInfosByProductIdInclude 
 * 상품 정보 검색(상품 객체 추가)
 
