# Images


## DESC
```pre
  *이미지 관리
   - 이미지파일 관리를 위한 이미지정보(폴더/파일)이다.
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [users]().id | 작성자ID |
| folder | varchar(255) | | false | | 폴더명 |
| dateFolder | varchar(255) | '' | false | | 날짜 폴더명 |
| name | varchar(255) | | false | | 파일명 |
| authorized | tinyint(1) | '1' | false | | 승인여부 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |


### ClassMethod

#### findImageIncludingById
 * 이미지 폴더명에 따라 해당하는 테이블(법인, 서비스, 상품 등)을 include 하여 검색한다.
