# AppMeta


## DESC
- 폰기프트 디폴트 데이터 


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | |
| expiryDay | int(11) | '60' | false | | 상품 디폴트 유효기간(일) |
| extendExpiryDay | int(11) | '60' | false | | 상품 유효기간 연장 가능일 |
| extendExpiryDayCountLimit | int(11) | '0' | false | | 상품 유효기간 연장 가능 횟수 |
| transferCountLimit | int(11) | '2' | false | | 재전송 가능 횟수 |
| refundRateBeforeExpiryDay | float | '100' | false | | 유효기간 이전 환불률 |
| refundRateAfterExpiryDay | float | '100' | false | | 유효기간 이후 환불률|
| extinctivePrescription | int(11) | '5' | false | | 소멸 시효(년) |
| maxPurchaseAmount | int(11) | '100' | false | | 최대 구매 가능 수량 |
| storeFarmServiceId | int(11) | NULL | true | | 스토어팜 ID |
| excludeProductId | varchar(255) | NULL | true | | 옴니텔 전송 거절 상품Id 리스트 (, 구분) |
| exceptionProductId | varchar(255) | NULL | true | |  sdk 예외 상품 리스트|
| pinMaxAmountADay | int(11) | NULL | true | | 핀발급 제한 횟수 - 하루 |
| pinMaxAmountAHour | int(11) | NULL | true | | 핀발급 제한 횟수  - 시간 |
| pinMaxAmountAMinute | int(11) | NULL | true | | 핀발급 제한 횟수  - 분 |
| pinMaxAmountADayWithIp | int(11) | NULL | true | | 핀발급 제한 횟수  - ip/하루 |
| pinMaxAmountAHourWithIp | int(11) | NULL | true | | 핀발급 제한 횟수  - ip/시간|
| pinMaxAmountAMinuteWithIp | int(11) | NULL | true | | 핀발급 제한 횟수  - ip/분|
| createdAt | bigint(20) | NULL | true | | |
| updatedAt | bigint(20) | NULL | true | | |
| deletedAt | datetime | NULL | true | | |
| id | int(11) | | false | | |

---
### ClassMethods

#### createMeta 
 * 새로운 메타 생성
  
#### findMetaData 
 * 메타 데이터 검색
  
#### deleteMeta 
 * 메타 데이터 
