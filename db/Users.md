# Users


## DESC
```pre
 * 회원관리
 - 회원가입을 위해 사용자가 입력한 회원 정보이다.
```


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | id |
| aid | varchar(255) | NULL | true | | 아이디(이메일/이름/핸드폰)  |
| email | varchar(255) | NULL | true | | 이메일 |
| secret | varchar(255) | NULL | true | | secret |
| salt | varchar(255) | | false | | salt |
| phoneNum | varchar(255) | NULL | true | | 핸드폰번호 |
| name | varchar(255) | NULL | true | | 이름 |
| nick | varchar(255) | NULL | true | | 닉네임 |
| role | [enum #9](#enum9) | 'roleB' | false | | 사용자권한 |
| gender | [enum #10](#enum10) | NULL | true | | 성별 |
| birth | varchar(255) | NULL | true | | 생일 |
| isVerifiedEmail | tinyint(1) | '0' | false | | 이메일 인증 여부 |
| country | varchar(255) | | false | | 국가 |
| language | varchar(255) | | false | | 언어 |
| isReviewed | tinyint(1) | '0' | false | | 리뷰 남김 여부 |
| agreedEmail | tinyint(1) | NULL | true | | 이메일 수신 동의 |
| agreedPhoneNum | tinyint(1) | NULL | true | | 폰번호 수신 동의 |
| profileId | int(11) | NULL | true | [profiles]().id | 프로파일 ID |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| passUpdatedAt | bigint(20) | '1475995080970615' | true | | 비밀번호 변경 날짜 |
| agreedTermsAt | bigint(20) | NULL | true | | 약관 동의 날짜 |
| di | varchar(255) | NULL | true | | di |
| ci | varchar(255) | NULL | true | | ci |



### ENUM
#### enum9
* 'roleA','roleB','roleC','roleD','roleE','roleF','roleG','roleS'

#### enum10
* 'm','f'


---
### ClassMethods

#### createUserWithType
 * 이메일, 핸드폰, 일반아이디 등 타입에 따라 회원가입을 처리한다.
