# AppDistributionProductInfos


## DESC
```pre
  * 유통대행사 상품관리 테이블
```

### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| productInfoId | int(11) | NULL | false | [productInfo]().id | productInfo fk |
| distributionId | int(11) | NULL | false | [service]().id | service fk |
| state | ENUM | | false | | 'authorized', 'unauthorized' |
| realPrice | int(11) |  | false | | 실제금액 |
| thirdPartyPriceSyncState | ENUM | NULL | true | | 'standby', 'done', 'error' |
| thirdPartyVisibilitySyncState | ENUM | NULL | true | | 'standby', 'done', 'error' |
| thirdPartySyncdAt | date | | false | | 서드파티동기화날짜 |
| createdAt | bigint(20) | NULL | true | | 생성일 |
| updatedAt | bigint(20) | NULL | true | | 수정일 |
| deletedAt | datetime | NULL | true | | 삭제일 |
| id | int(11) | | false | | id |


---
### ClassMethods


#### createWithCallback 
 * AppDistributionProductInfo 신규 생성 
 
#### bulkCreateWithCallback
 * AppDistributionProductInfo 벌크 생성
 