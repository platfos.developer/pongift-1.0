# AppServices


## DESC


### TABLE
| name | type | default | allow null | foreign key | description |
| ----- | ----- | ----- | ----- | ----- | ----- |
| id | int(11) | | false | | row ID |
| accessKey | varchar(255) | NULL | true | | access key |
| authorId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | 작성자 ID |
| affiliationId | int(11) | NULL | true | [AppAffiliations](/markdown/db/AppAffiliations.md).id | 제휴사 ID |
| corporationId | int(11) | NULL | true | [AppCorporations](/markdown/db/AppCorporations.md).id | 법인 ID |
| categoryId | int(11) | NULL | true | [AppCategories](/markdown/db/AppCategories.md).id | 서비스 카테고리 ID |
| externalEnterprise | [enum #7](#enum7) | NULL | true | | 외부 연동 모듈 |
| externalCode | varchar(255) | NULL | true | | |
| state | [enum #9](#enum9) | 'standby' | false | | 서비스 상태 |
| type | [enum #10](#enum10) | 'supply' | false | | 서비스 유형 |
| calculationType | [enum #11](#enum11) | 'used' | false | | 정산 기준 |
| name | varchar(255) | | false | | 서비스명 |
| pongiftContactId | int(11) | NULL | true | [Users](/markdown/db/Users.md).id | |
| local | tinyint(1) | '0' | false | | 로컬 유무 |
| pgUsage | tinyint(1) | '0' | false | | pg 사용 유무 |
| pgCardUsage | tinyint(1) | '0' | false | | pg card 사용 유무 |
| pgPhoneUsage | tinyint(1) | '0' | false | | pg phone 사용 유무 |
| pgAccountUsage | tinyint(1) | '0' | false | | pg account 사용 유무 |
| paymentBank | [enum #19](#enum19) | NULL | true | | 계좌 은행 |
| paymentDepositor | varchar(255) | NULL | true | | 계좌 예금주 |
| paymentAccount | varchar(255) | NULL | true | | 계좌 번호 |
| customerCenterPhoneNum | varchar(255) | NULL | true | | 소비자 상담 센터 |
| logoImgId | int(11) | NULL | true | [Images](/markdown/db/Images.md).id | 로고 이미지 ID |
| distribution | [enum #24](#enum24) | NULL | true | | 유통대행 |
| businessRange | [enum #25](#enum25) | NULL | true | | 영업범위 |
| info | varchar(1024) | NULL | true | | 상품 이용 안내 템플릿 |
| headerColor | varchar(255) | NULL | true | | |
| templateImageKey | varchar(255) | | false | | 서비스템플릿id |
| info1 | varchar(255) | | false | | info1 |
| info2 | varchar(255) | | false | | info2 |
| info3 | varchar(255) | | false | | info3 |
| info4 | varchar(255) | | false | | info4 |
| info5 | varchar(255) | | false | | info5 |
| createdAt | bigint(20) | NULL | true | | 생성날짜 |
| updatedAt | bigint(20) | NULL | true | | 수정날짜 |
| deletedAt | datetime | NULL | true | | 삭제날짜 |



### ENUM
#### enum7
* 'omnitel (옴니텔)','smartInfini (스마트인피니)','cultureland (컬쳐랜드)'

#### enum9
* 'standby (대기)','use (사용)','close (종료)'

#### enum10
* 'supply (상품공급사)','distribution (유통대행사)','etc (기타)'

#### enum11
* 'used (사용일)','purchased (판매일)'

#### enum19
* 'KEB하나','외환(하나','국민','농협','우리','신한','기업','경남','광주','대구','도이치','부산','산업','상호저축','새마을금고','수협중앙회','신용협동조합','우체국','전북','제주','한국씨티','BOA','HSBC','JP모간','SC','교보증권','대신증권','대우증권','동부증권','유안타증권','메리츠증권','미래에셋','부국증권','삼성증권','신영증권','신한금융투자','NH투자증권','유진증권','키움증권','하나금융투자','하이투자증권','한국투자','한화투자증권','현대증권','이베스트투자증권','HMC증권','LIG증권','SK증권','산림조합','중국공상은행','BNP파리바은행','KB투자증권','펀드온라인코리아'

#### enum24
* 'online','offline'

#### enum25
* 'corporation','channel'


---
### ClassMethods


#### createServiceWithCalculation 
 * 서비스 생성 및 업데이트 ( build-save(Create) : AppService/User/Profile | Create : AppCalculation/AppContact)
    1) 생성 후 암호화(Accesskey) 업데이트
    
#### updateServiceWithData
  * 서비스 업데이트 ( update-destroy 후 create )
    1) Image / AppCalculation / AppContact / AppService

#### deleteServiceWithData
  * 서비스 업데이트 ( update-destroy 후 create )
    1) update : User(권한수정) / destroy : AppUserManage , Image , AppServiceStore , AppContact , AppCalculation , AppService 
        
#### findServicesByOptions
  * 조건에 해당하는 개수만큼 서비스 조회


### 주의사항

```pre
- accessKey
  - 서비스 업체에 SDK 전달 시 랜덤 값으로 생성하여 해당 서비스의 고유값으로 사용 (accessKey를 이용해 접속경로 확인)
  - ex) Dalvik/2.1.0 (Linux; U; Android 7.0; SM-G920S Build/NRD90M) X  ::ffff:223.39.140.8  Wed, 08 Nov 2017 07:05:35 GMT POST /api/pongift/sdk-initial {"accessKey":"ymlH4/Y3PaaYy0ajPvEoVw=="} 200 4.856 ms
- externalEnterprise 외부연동모듈
  - 해당 서비스를 제공하는 외부업체 값 (ex: 옴니텔/스마트인피니/컬쳐랜드)
- externalCode 외부 코드
  - 외부업체(ex: 옴니텔)에서 부여받은 고유값으로 해당 값을 통해 외부업체에서 폰기프트를 구분 (accessKey와 비슷한 역할)
  - POC0000090 -> 사용일 기준 / POC0000094 -> 판매일 기준 (옴니텔의 정산으로 인해 두개로 나뉨)
- pongiftContactId
  - 플랫포스 담당자 저장 필드
- distribution
  - 서비스등록시 온라인/오프라인 구분 
  - 온라인 : 기존 온라인 판매
  - 오프라인 : 핀번호 발급을 통한 오프라인 판매  
- businessRange
  - 유통채널 구분(사용하지 않음)
```
